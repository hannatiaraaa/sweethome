import {PermissionsAndroid, ToastAndroid} from 'react-native';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';

export const RequestPermission = () => {
  return new Promise(async (resolve, reject) => {
    const granted = await PermissionsAndroid.check('android.permission.CAMERA');
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      resolve(true);
    } else {
      const request = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.CAMERA,
      );
      if (request === PermissionsAndroid.RESULTS.GRANTED) {
        resolve(true);
      } else {
        resolve(false);
        ToastAndroid.showWithGravity(
          'Please grant camera permission',
          ToastAndroid.LONG,
          ToastAndroid.CENTER,
        );
      }
    }
  });
};

export const UploadedImage = (type) => {
  const config = {
    title: 'Select Image',
    storageOptions: {skipBackup: true, path: 'images'},
    allowsEditing: true,
  };

  if (type === 'camera') {
    return new Promise((resolve, reject) => {
      RequestPermission();
      launchCamera(config, (res) => {
        if (res.uri) {
          resolve({
            response: true,
            loading: true,
            ...res,
          });
        } else if (res.didCancel) {
          resolve({
            response: false,
            loading: true,
          });
        } else {
          reject({
            response: false,
            loading: true,
          });
        }
      });
    });
  } else {
    return new Promise((resolve, reject) => {
      launchImageLibrary(config, (res) => {
        if (res.fileSize < 4194304) {
          resolve({
            response: true,
            loading: true,
            ...res,
          });
        } else if (res.didCancel) {
          resolve({
            response: false,
            loading: true,
          });
        } else {
          reject({
            response: false,
            loading: true,
          });
        }
      });
    });
  }
};
