import axios from 'axios';
import {LogBox} from 'react-native';

// store
import {Store} from '../../store/Store';

// url
export const BASE_URL = 'https://sweethome-api-v1.herokuapp.com/api/v1';

LogBox.ignoreLogs([
  'Require cycle: src/shared/utils/api.js -> src/store/Store.js -> src/store/SagaWatcher.js -> src/features/Profile/Authentication/redux/saga.js -> src/shared/utils/api.js',
]);

function* doRequest(endpoint, options) {
  try {
    const Blacklist = ['/login', '/register', '/google', '/facebook'];
    let response;
    const {method, body, headers} = options;
    const Token = Store.getState().AuthReducer.token;
    const result = yield axios({
      url: `${BASE_URL}${endpoint}`,
      method,
      data: body,
      headers: headers
        ? Token && !Blacklist.includes(endpoint)
          ? {Authorization: `${Token}`}
          : {...headers}
        : Token && !Blacklist.includes(endpoint)
        ? {Authorization: `${Token}`}
        : null,
      validateStatus: (status) => status < 500,
    });

    if (result.status === 200 || result.status === 201) {
      response = {
        ...result,
        status: true,
      };
    } else {
      response = {
        ...result,
        status: false,
      };
    }
    return response;
  } catch (error) {
    console.log(error);
    return {
      messages: "There's an error in our server, try again later",
      status: false,
    };
  }
}

export function* request(endpoint, method, config) {
  return yield doRequest(endpoint, {method, ...config});
}
