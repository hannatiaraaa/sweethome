import {StyleSheet} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {Size} from './config';

export const Layouting = (type = 'center', num = 1) => {
  return StyleSheet.create({
    flex: {
      flex: num,
    },
    flexRow: {
      flexDirection: 'row',
    },
    centered: {
      justifyContent: 'center',
      alignItems: type,
    },
    spaceBetween: {
      justifyContent: 'space-between',
      alignItems: type,
    },
    spaceAround: {
      justifyContent: 'space-around',
      alignItems: type,
    },
    spaceEvenly: {
      justifyContent: 'space-evenly',
      alignItems: type,
    },
    flexStart: {
      justifyContent: 'flex-start',
      alignItems: type,
    },
    flexEnd: {
      justifyContent: 'flex-end',
      alignItems: type,
    },
    align: {
      alignItems: type,
    },
    shadow: {
      shadowColor: '#214457',
      shadowOffset: {
        width: 0,
        height: Size.ms4,
      },
      shadowOpacity: moderateScale(0.2),
      shadowRadius: moderateScale(6.27),
      elevation: Size.ms10,
    },
    linearGradient: {
      backgroundColor: 'transparent',
      position: 'absolute',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
    },
  });
};
