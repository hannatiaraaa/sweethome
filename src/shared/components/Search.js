import React from 'react';
import {StyleSheet} from 'react-native';
import {SearchBar} from 'react-native-elements';
import {widthPercentageToDP} from 'react-native-responsive-screen';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

export const Search = ({onChangeText, value}) => {
  return (
    <SearchBar
      containerStyle={[Layouting().flexEnd, styles.searchBarContainer]}
      inputContainerStyle={[Layouting().centered, styles.searchBarBox]}
      inputStyle={styles.searchBarText}
      placeholder="Search projects"
      placeholderTextColor={Color.ashGray}
      onChangeText={onChangeText}
      value={value}
    />
  );
};

const styles = StyleSheet.create({
  searchBarContainer: {
    backgroundColor: Color.primaryBlue,
    borderBottomColor: 'transparent',
    borderTopColor: 'transparent',
    paddingHorizontal: widthPercentageToDP(6),
    height: Size.ms84,
  },
  searchBarBox: {
    height: Size.ms32,
    backgroundColor: 'white',
    borderRadius: Size.ms4,
  },
  searchBarText: {
    paddingBottom: Size.hp1,
    fontSize: Size.ms14,
    fontFamily: 'NotoSans-Regular',
  },
});
