import React from 'react';
import {StyleSheet} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

export const Dropdown = ({
  placeholder,
  colorText,
  items,
  defaultValue,
  onChangeItem,
}) => {
  return (
    <DropDownPicker
      items={items}
      placeholder={placeholder}
      placeholderStyle={styles.placeholder}
      defaultValue={defaultValue}
      onChangeItem={onChangeItem}
      containerStyle={styles.container}
      style={[
        styles.box,
        {borderColor: placeholder ? Color.ashGray : Color.primaryBlue},
      ]}
      itemStyle={Layouting().flexStart}
      dropDownStyle={styles.dropDown}
      labelStyle={[
        styles.label,
        {color: colorText ? colorText : Color.primaryBlue},
      ]}
      activeLabelStyle={styles.active}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    width: Size.wp92,
    height: Size.ms48,
  },
  placeholder: {
    color: Color.ashGray,
    fontFamily: 'NotoSans-Regular',
    fontSize: Size.ms14,
  },
  box: {
    borderWidth: 1,
    backgroundColor: 'transparent',
    borderRadius: Size.ms4,
  },
  dropDown: {
    borderWidth: 1,
    borderColor: Color.primaryBlue,
    backgroundColor: Color.wheat,
  },
  label: {
    fontSize: Size.ms14,
    fontFamily: 'NotoSans-Regular',
  },
  active: {
    fontFamily: 'NotoSans-Bold',
  },
});
