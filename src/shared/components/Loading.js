import React from 'react';
import {View, StyleSheet} from 'react-native';
import FastImage from 'react-native-fast-image';
import {SkypeIndicator} from 'react-native-indicators';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

export default function Loading() {
  return (
    <View style={[Layouting().centered, Layouting().flex]}>
      <SkypeIndicator
        size={Size.ms32}
        color={Color.successGreen}
        style={[Layouting().flex, Layouting().flexEnd, styles.loading]}
      />
      <View style={[Layouting(undefined, 1.7).flex, Layouting().flexStart]}>
        <FastImage
          source={require('../../assets/images/loadingLogo.png')}
          resizeMode="contain"
          style={styles.logo}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  loading: {
    marginBottom: Size.hp10,
  },
  logo: {
    width: Size.wp84,
    height: Size.hp10,
  },
});
