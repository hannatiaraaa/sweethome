import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';

// global
import {Layouting} from '../global/Layouting';
import {Color, Size} from '../global/config';

// icon
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export const SavedButton = ({onPress, pressedSaved = false}) => {
  return (
    <View style={[Layouting().flex, Layouting('flex-end').flexStart]}>
      <TouchableOpacity
        activeOpacity={0.9}
        onPress={onPress}
        style={[
          Layouting().centered,
          Layouting().shadow,
          styles.savedContainer,
        ]}>
        <MaterialCommunityIcons
          name={pressedSaved ? 'heart' : 'heart-outline'}
          size={Size.ms20}
          color={pressedSaved ? Color.errorPink : Color.primaryBlue}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  savedContainer: {
    marginTop: Size.hp2,
    marginRight: Size.wp4,
    width: Size.ms40,
    height: Size.ms40,
    borderRadius: Size.ms80,
    backgroundColor: 'white',
  },
});
