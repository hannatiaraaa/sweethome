import React from 'react';
import {StyleSheet, View} from 'react-native';

// component
import NotoSans from './NotoSans';
import {TextCard} from './TextCard';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

export const FilterCardContainer = ({
  title,
  hasNote = false,
  children,
  isRequired = false,
}) => {
  return (
    <TextCard
      backgroundColor={Color.wheat}
      hasShadow={false}
      containerStyle={[styles.card]}
      style={styles.content}>
      <View
        style={[
          styles.heading,
          hasNote
            ? [Layouting().flexRow, Layouting().spaceBetween]
            : Layouting('flex-start').centered,
        ]}>
        <NotoSans
          title={
            <>
              <NotoSans
                title={title}
                size={Size.ms14}
                type="Bold"
                color={Color.gunGray}
              />
              {isRequired && (
                <NotoSans title="*" color="red" size={Size.ms14} />
              )}
            </>
          }
          type="Bold"
          style={styles.title}
        />
        {hasNote ? (
          <NotoSans
            title="You may select more than one"
            size={Size.ms10}
            color={Color.ashGray}
            type="Italic"
          />
        ) : null}
      </View>
      {children}
    </TextCard>
  );
};

const styles = StyleSheet.create({
  card: {
    paddingVertical: Size.hp2,
    marginTop: Size.hp1,
    marginBottom: Size.hp2,
  },
  content: {
    paddingHorizontal: Size.wp2,
    width: Size.wp92,
  },
  heading: {
    paddingHorizontal: Size.wp2,
    marginBottom: Size.hp2,
  },
});
