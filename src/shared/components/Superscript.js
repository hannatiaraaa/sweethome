import React from 'react';
import {View} from 'react-native';

// component
import NotoSans from './NotoSans';

// global
import {Layouting} from '../global/Layouting';

export const Superscript = ({
  base,
  exponent,
  sizeBase,
  sizeExp,
  color,
  type,
}) => {
  return (
    <View style={Layouting().flexRow}>
      <View style={Layouting('flex-end').align}>
        <NotoSans title={base} size={sizeBase} color={color} type={type} />
      </View>
      <View style={Layouting('flex-start').align}>
        <NotoSans title={exponent} size={sizeExp} color={color} type={type} />
      </View>
    </View>
  );
};
