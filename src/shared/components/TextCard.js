import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';

// global
import {Layouting} from '../global/Layouting';
import {Size} from '../global/config';

export const TextCard = ({
  onPress,
  backgroundColor,
  children,
  hasShadow = true,
  containerStyle,
  style,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={onPress}
      style={[
        hasShadow ? Layouting().shadow : null,
        styles.container,
        {
          backgroundColor: backgroundColor,
          ...style,
        },
        ...containerStyle,
      ]}>
      {children}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: Size.ms4,
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp2,
  },
});
