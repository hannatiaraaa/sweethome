import * as React from 'react';
import {StatusBar} from 'react-native';
import {useIsFocused} from '@react-navigation/native';

export const FocusAwareStatusBar = () => {
  const isFocused = useIsFocused();

  return isFocused ? (
    <StatusBar
      backgroundColor="white"
      barStyle="dark-content"
      translucent={true}
    />
  ) : (
    <StatusBar
      backgroundColor="transparent"
      barStyle="light-content"
      translucent={true}
    />
  );
};
