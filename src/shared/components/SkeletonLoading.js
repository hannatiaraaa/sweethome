import React from 'react';
import ContentLoader, {Rect} from 'react-content-loader/native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';

const SkeletonLoading = (props) => (
  <ContentLoader
    width={700}
    height={heightPercentageToDP(100)}
    backgroundColor="#f5f5f5"
    foregroundColor="#dbdbdb"
    {...props}>
    <Rect x="670" y="9" rx="3" ry="3" width="6" height="285" />
    <Rect
      x={widthPercentageToDP(10)}
      y="42"
      rx="16"
      ry="16"
      width={widthPercentageToDP(80)}
      height="150"
    />

    <Rect
      x={widthPercentageToDP(30)}
      y={moderateScale(190)}
      rx="16"
      ry="16"
      width={moderateScale(150)}
      height={moderateScale(80)}
    />
    <Rect x="42.84" y="350" rx="5" ry="5" width="143.55" height="86.59" />
    <Rect x="192.84" y="350" rx="0" ry="0" width="148.72" height="12.12" />
    <Rect x="192.84" y="370" rx="0" ry="0" width="89" height="9" />

    <Rect x="42.84" y="450" rx="5" ry="5" width="143.55" height="86.59" />
    <Rect x="192.84" y="450" rx="0" ry="0" width="148.72" height="12.12" />
    <Rect x="192.84" y="460" rx="0" ry="0" width="89" height="9" />

    <Rect x="42.84" y="560" rx="5" ry="5" width="143.55" height="86.59" />
    <Rect x="192.84" y="560" rx="0" ry="0" width="148.72" height="12.12" />
    <Rect x="192.84" y="580" rx="0" ry="0" width="89" height="9" />

    <Rect x="376" y="41" rx="3" ry="3" width="231" height="29" />
  </ContentLoader>
);

export default SkeletonLoading;
