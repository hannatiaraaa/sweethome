import React from 'react';
import {View, TouchableHighlight, Text, StyleSheet} from 'react-native';
import {Divider, Overlay} from 'react-native-elements';
import {moderateScale} from 'react-native-size-matters';
import {widthPercentageToDP} from 'react-native-responsive-screen';

// global
import {Layouting} from '../global/Layouting';
import {Color, Size} from '../global/config';

export default function AlertModal({
  isVisible,
  title,
  labelOK = 'Try Again',
  onPressClose = () => {},
  numOfButtons = 1,
  labelButton,
  navOnPress = () => {},
}) {
  const RenderButton = ({button, color, onPress, style}) => {
    return (
      <TouchableHighlight
        onPress={onPress}
        underlayColor={Color.cloud}
        style={[
          Layouting().flex,
          Layouting().centered,
          styles.buttonContainer,
          {...style},
        ]}>
        <Text style={[styles.textButton, {color: color}]}>{button}</Text>
      </TouchableHighlight>
    );
  };

  return (
    <Overlay
      isVisible={isVisible}
      animationType="fade"
      fullScreen={true}
      overlayStyle={[Layouting().spaceAround, styles.container]}>
      <View style={styles.contentContainer}>
        <Text style={styles.title}>{title}</Text>
        <Divider style={styles.divider} />
        <View style={[Layouting().flexRow, Layouting().spaceAround]}>
          <RenderButton
            button={labelOK}
            color={numOfButtons > 1 ? Color.errorPink : Color.infoBlue}
            onPress={onPressClose}
          />
          {numOfButtons > 1 && (
            <RenderButton
              button={labelButton}
              color={Color.infoBlue}
              style={styles.borderLeftButton}
              onPress={() => {
                navOnPress();
                onPressClose();
              }}
            />
          )}
        </View>
      </View>
    </Overlay>
  );
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'transparent',
  },
  contentContainer: {
    backgroundColor: Color.vogue,
    paddingVertical: Size.hp1,
    paddingHorizontal: widthPercentageToDP(8),
    borderRadius: Size.ms8,
  },
  title: {
    fontSize: Size.ms18,
    fontFamily: 'Inter-Medium',
    paddingVertical: Size.hp2,
  },
  divider: {
    borderColor: Color.ashGray,
    borderWidth: moderateScale(0.35),
  },
  buttonContainer: {
    paddingVertical: Size.hp1,
  },
  textButton: {
    fontSize: Size.ms16,
    fontFamily: 'Inter-SemiBold',
  },
  borderLeftButton: {
    borderLeftColor: Color.ashGray,
    borderLeftWidth: moderateScale(0.35),
  },
});
