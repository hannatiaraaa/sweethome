import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import {heightPercentageToDP} from 'react-native-responsive-screen';

// component
import NotoSans from './NotoSans';
import {SavedButton} from './SavedButton';

// global
import {Layouting} from '../global/Layouting';
import {Size} from '../global/config';

export const DataCard = ({
  onPress,
  sourceImage,
  title,
  location,
  hasSaved = false,
  onPressSaved,
  pressedSaved = false,
  style,
}) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={onPress}
      style={[Layouting().shadow, styles.container, {...style}]}>
      <FastImage source={sourceImage} style={[Layouting(undefined, 4).flex]}>
        <LinearGradient
          colors={[
            'rgba(255, 255, 255, 0)',
            'rgba(255, 255, 255, 0.2)',
            'white',
          ]}
          locations={[0, 0.6823, 100]}
          style={Layouting().linearGradient}>
          {hasSaved ? (
            <SavedButton onPress={onPressSaved} pressedSaved={pressedSaved} />
          ) : null}
        </LinearGradient>
      </FastImage>
      <View
        style={[
          Layouting().flex,
          Layouting('flex-start').spaceEvenly,
          styles.content,
        ]}>
        <NotoSans title={title} type="Bold" />
        <NotoSans title={location} />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    width: Size.wp92,
    height: heightPercentageToDP(36),
    marginVertical: Size.ms16,
  },
  content: {
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp1,
  },
});
