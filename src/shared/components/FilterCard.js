import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';

// component
import NotoSans from './NotoSans';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';
import {widthPercentageToDP} from 'react-native-responsive-screen';

export const FilterCard = ({
  title,
  id,
  onPress,
  hasBorder = true,
  pressed = false,
  newSelected = [],
  style,
}) => {
  const selected = newSelected.find((e) => e === id);

  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={[
        Layouting().centered,
        styles.container,
        hasBorder ? styles.borderContainer : styles.nonBorderContainer,
        hasBorder ? styles.border : pressed ? styles.radius : null,
        {
          backgroundColor: pressed || selected ? Color.primaryBlue : null,
          ...style,
        },
      ]}>
      <NotoSans
        title={title}
        color={pressed || selected ? 'white' : Color.primaryBlue}
        type={!hasBorder && pressed ? 'Bold' : 'Regular'}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: widthPercentageToDP(1.3),
    paddingVertical: Size.hp1,
  },
  borderContainer: {
    margin: Size.ms4,
  },
  nonBorderContainer: {
    marginRight: Size.ms10,
    marginVertical: Size.ms10,
  },
  border: {
    borderWidth: Size.ms1,
    borderRadius: Size.ms7,
  },
  radius: {
    borderRadius: Size.ms7,
  },
});
