import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';

// component
import NotoSans from './NotoSans';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

export const YellowButton = ({
  onPress,
  title,
  disabled,
  width = Size.wp100,
  marginHorizontal,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      disabled={disabled}
      style={[
        Layouting().centered,
        Layouting().shadow,
        styles.button,
        {
          backgroundColor: disabled ? Color.cream : Color.secondaryYellow,
          width: width,
          borderRadius: width === Size.wp100 ? null : Size.ms10,
          marginHorizontal: marginHorizontal,
        },
      ]}>
      <NotoSans
        title={title}
        color={Color.gunGray}
        size={Size.ms14}
        type="Bold"
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    height: Size.ms40,
  },
});
