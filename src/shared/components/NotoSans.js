import React from 'react';
import {Text} from 'react-native';
import {Size} from '../../shared/global/config';

export default function NotoSans({
  title,
  onPress,
  color = 'black',
  size = Size.ms12,
  type = 'Regular',
  style,
  props,
}) {
  return (
    <Text
      onPress={onPress}
      style={{
        color,
        fontSize: size,
        fontFamily: `NotoSans-${type}`,
        ...style,
      }}
      {...props}>
      {title}
    </Text>
  );
}
