import React from 'react';
import {StyleSheet, TouchableHighlight, View} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {BottomSheet} from 'react-native-elements';

// component
import NotoSans from './NotoSans';

// utils
import {RequestPermission, UploadedImage} from '../utils/ImagePicker';

// global
import {Color, Size} from '../global/config';
import {Layouting} from '../global/Layouting';

// icon
import Octicons from 'react-native-vector-icons/Octicons';
import Entypo from 'react-native-vector-icons/Entypo';

// redux
import {connect} from 'react-redux';
import {uploadProfileImage} from '../../features/Profile/redux/action';
import {uploadReceipt} from '../../features/Order/redux/action';

const UploadBottomSheet = (props) => {
  let {onRequestClose, isVisible} = props;
  const {route, onClose = () => {}} = props;
  const projectDetails = props.projectDetails ? props.projectDetails : '';
  const chooseCameraImage = async () => {
    const permission = await RequestPermission();
    if (permission) {
      const formData = await UploadedImage('camera');
      if (route.name === 'ProjectDetails') {
        props.uploadReceipt(formData, projectDetails._id);
      } else {
        props.uploadProfileImage(formData, route.name);
      }
      onClose();
    }
  };

  const chooseFileImage = async () => {
    const formData = await UploadedImage();
    if (route.name === 'ProjectDetails') {
      props.uploadReceipt(formData, projectDetails._id);
    } else {
      props.uploadProfileImage(formData, route.name);
    }
    onClose();
  };

  const RenderRow = ({icon, title, backgroundColor, onPress}) => {
    return (
      <TouchableHighlight
        style={styles.rowContainer}
        underlayColor={Color.cloud}
        onPress={onPress}>
        <View style={[Layouting().spaceEvenly, styles.rowContent]}>
          <View
            style={[
              Layouting().centered,
              styles.iconContainer,
              {backgroundColor: backgroundColor},
            ]}>
            {icon}
          </View>
          <NotoSans title={title} />
        </View>
      </TouchableHighlight>
    );
  };

  return (
    <BottomSheet
      isVisible={isVisible}
      modalProps={(onRequestClose = {onRequestClose})}>
      <View style={styles.container}>
        <NotoSans title="Upload image from" size={Size.ms16} />
        <View style={[Layouting().flexRow, Layouting().spaceEvenly]}>
          <RenderRow
            icon={
              <Octicons
                name="device-camera"
                color={Color.wheat}
                size={Size.ms32}
              />
            }
            backgroundColor={Color.infoBlue}
            title="Camera"
            onPress={() => chooseCameraImage()}
          />
          <RenderRow
            icon={
              <Entypo name="folder" color={Color.gunGray} size={Size.ms32} />
            }
            backgroundColor={Color.mint}
            title="File"
            onPress={() => chooseFileImage()}
          />
        </View>
      </View>
    </BottomSheet>
  );
};

const mapStateToProps = (state) => ({
  projectDetails: state.OrderReducer.projectDetails,
});

const mapDispatchToProps = {
  uploadProfileImage,
  uploadReceipt,
};

export default connect(mapStateToProps, mapDispatchToProps)(UploadBottomSheet);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: Size.wp4,
    paddingTop: Size.hp4,
    borderTopRightRadius: Size.ms16,
    borderTopLeftRadius: Size.ms16,
  },
  rowContainer: {
    marginVertical: Size.hp2,
    height: moderateScale(120),
    width: moderateScale(120),
    borderRadius: Size.ms20,
  },
  rowContent: {
    paddingVertical: Size.hp1,
    height: moderateScale(116),
  },
  iconContainer: {
    borderRadius: Size.ms24,
    width: Size.ms60,
    height: Size.ms60,
  },
  textCancel: {
    marginVertical: Size.hp2,
    textAlign: 'center',
  },
});
