import {SET_ALERT, SET_LOADING} from './GlobalAction';

const initialState = {
  isLoading: false,
  alert: {
    status: false,
    title: '',
    numOfButtons: 1,
    labelButton: '',
    navOnPress: () => {},
  },
};

export const GlobalReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_LOADING:
      return {
        ...state,
        isLoading: action.payload,
      };

    case SET_ALERT:
      return {
        ...state,
        alert: action.payload,
      };

    default:
      return state;
  }
};
