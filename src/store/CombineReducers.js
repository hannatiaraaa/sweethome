import {combineReducers} from 'redux';
import {GlobalReducer} from './GlobalReducer';
import {AuthReducer} from '../features/Profile/Authentication/redux/reducer';
import {ProfileReducer} from '../features/Profile/redux/reducer';
import {HomeReducer} from '../features/Home/redux/reducer';
import {ExploreReducer} from '../features/Explore/redux/reducer';
import {OrderReducer} from '../features/Order/redux/reducer';
import {SavedReducer} from '../features/Saved/redux/reducer';

export const CombineReducers = combineReducers({
  GlobalReducer,
  AuthReducer,
  ProfileReducer,
  HomeReducer,
  ExploreReducer,
  OrderReducer,
  SavedReducer,
});
