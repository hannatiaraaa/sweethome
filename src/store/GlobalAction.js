export const SET_LOADING = 'SET_LOADING';
export const SET_ALERT = 'SET_ALERT';

export const setLoading = (payload) => {
  return {
    type: SET_LOADING,
    payload,
  };
};

export const setAlert = (payload) => {
  return {
    type: SET_ALERT,
    payload,
  };
};
