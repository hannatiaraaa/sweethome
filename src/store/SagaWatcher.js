import {all} from 'redux-saga/effects';
import {AuthSaga} from '../features/Profile/Authentication/redux/saga';
import {ProfileSaga} from '../features/Profile/redux/saga';
import {HomeSaga} from '../features/Home/redux/saga';
import {ExploreSaga} from '../features/Explore/redux/saga';
import {OrderSaga} from '../features/Order/redux/saga';
import {SavedSaga} from '../features/Saved/redux/saga';

export function* SagaWatcher() {
  yield all([
    AuthSaga(),
    ProfileSaga(),
    HomeSaga(),
    ExploreSaga(),
    OrderSaga(),
    SavedSaga(),
  ]);
}
