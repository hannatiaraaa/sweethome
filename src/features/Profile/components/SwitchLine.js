import React, {useState} from 'react';
import {View, Switch, StyleSheet} from 'react-native';

// component
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

export const SwitchLine = ({title, value, onValueChange}) => {
  return (
    <View
      style={[Layouting().flexRow, Layouting().spaceBetween, styles.container]}>
      <NotoSans title={title} size={Size.ms14} color={Color.primaryBlue} />
      <Switch
        trackColor={{false: Color.ashGray, true: Color.primaryBlue}}
        thumbColor={value === true ? 'white' : Color.wheat}
        ios_backgroundColor={Color.ashGray}
        onValueChange={onValueChange}
        value={value}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Size.wp92,
    marginVertical: Size.ms4,
  },
});
