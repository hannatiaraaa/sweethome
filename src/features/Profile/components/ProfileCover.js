import React, {useEffect} from 'react';
import {
  View,
  StyleSheet,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import {moderateScale} from 'react-native-size-matters';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

// component
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// redux
import {connect} from 'react-redux';
import {getShowcaseAll, getShowcaseDetails} from '../../Home/redux/action';

const ProfileCover = (props) => {
  const {
    isLogin = false,
    routeName,
    user,
    isFinished,
    showcaseAll,
    onPress,
  } = props;
  const routeNameSettings =
    routeName === 'Profile Settings' || routeName === 'Account Settings';

  useEffect(() => {
    props.getShowcaseAll();
  }, []);

  const goToDetails = (id) => {
    props.getShowcaseDetails(id);
  };

  const RenderUploadImageButton = () => {
    return (
      <TouchableHighlight
        onPress={onPress}
        underlayColor={Color.infoBlue}
        style={[
          !user.photo
            ? [Layouting('flex-end').centered, {paddingRight: moderateScale(2)}]
            : Layouting().centered,
          styles.iconContainer,
        ]}>
        <MaterialCommunityIcons
          name={!user.photo ? 'pencil-plus' : 'pencil'}
          size={Size.ms16}
          color="white"
        />
      </TouchableHighlight>
    );
  };

  const RenderAvatarIcon = () => {
    return (
      <View style={[Layouting().centered, styles.avatar]}>
        <Ionicons
          name="md-person-outline"
          size={Size.ms60}
          color={Color.gunGray}
        />
      </View>
    );
  };

  const RenderAvatarImage = () => {
    return <FastImage source={{uri: user.photo}} style={styles.avatar} />;
  };

  const RenderHeading = () => {
    return (
      <View
        style={[
          Layouting().flex,
          Layouting('flex-start').centered,
          styles.content,
        ]}>
        <NotoSans
          size={Size.ms14}
          title={
            isLogin && isFinished
              ? `Welcome Home, ${
                  user.firstname[0].toUpperCase() + user.firstname.substring(1)
                }!`
              : 'Please Login'
          }
          type="Bold"
          color={Color.primaryBlue}
        />
      </View>
    );
  };

  const RenderSubHeading = () => {
    return (
      <View
        style={[
          Layouting('flex-start').centered,
          Layouting().shadow,
          styles.contentSettings,
        ]}>
        <NotoSans
          title={routeName}
          size={Size.ms14}
          type="Bold"
          color={Color.primaryBlue}
        />
      </View>
    );
  };

  return (
    <View style={Layouting().flex}>
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => goToDetails(showcaseAll[0]._id)}>
        <FastImage
          source={{uri: showcaseAll[0].gallery[1].photo}}
          style={[
            Layouting().shadow,
            styles.image,
            routeNameSettings ? styles.coverSettings : styles.cover,
          ]}>
          <LinearGradient
            colors={[
              'rgba(255, 255, 255, 0)',
              'rgba(255, 255, 255, 0.2)',
              'white',
            ]}
            locations={[0, 0.5122, 0.7508]}
            style={Layouting().linearGradient}>
            <View
              style={[
                Layouting(undefined, 3).flex,
                Layouting().flexEnd,
                styles.containerSettings,
              ]}>
              {isLogin && isFinished ? (
                <>
                  <RenderUploadImageButton />
                  {!user.photo || user.photo === 'none' ? (
                    <RenderAvatarIcon />
                  ) : (
                    <RenderAvatarImage />
                  )}
                </>
              ) : (
                <RenderAvatarIcon />
              )}
              {isLogin && isFinished ? (
                <NotoSans
                  title={
                    user.firstname[0].toUpperCase() +
                    user.firstname.substring(1) +
                    ' ' +
                    user.lastname[0].toUpperCase() +
                    user.lastname.substring(1)
                  }
                  size={Size.ms14}
                  type="Bold"
                  color={Color.primaryBlue}
                />
              ) : null}
            </View>
            {routeNameSettings ? null : <RenderHeading />}
          </LinearGradient>
        </FastImage>
      </TouchableOpacity>
      {routeNameSettings ? <RenderSubHeading /> : null}
    </View>
  );
};

const mapStateToProps = (state) => ({
  user: state.ProfileReducer.user,
  isFinished: state.ProfileReducer.isFinished,
  showcaseAll: state.HomeReducer.showcaseAll,
});

const mapDispatchToProps = {
  getShowcaseAll,
  getShowcaseDetails,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileCover);

const styles = StyleSheet.create({
  image: {
    width: Size.wp100,
    borderBottomColor: Color.wheat,
  },
  cover: {
    height: heightPercentageToDP(44),
  },
  coverSettings: {
    height: heightPercentageToDP(32),
  },
  containerSettings: {
    paddingBottom: Size.hp2,
  },
  avatar: {
    borderWidth: moderateScale(2),
    backgroundColor: Color.cream,
    width: moderateScale(92),
    height: moderateScale(92),
    borderColor: Color.gunGray,
    borderRadius: moderateScale(184),
    marginBottom: Size.ms12,
  },
  iconContainer: {
    width: Size.ms24,
    height: Size.ms24,
    borderRadius: Size.ms48,
    backgroundColor: Color.primaryBlue,
    zIndex: 2,
    top: Size.ms24,
    left: Size.ms32,
  },
  content: {
    paddingHorizontal: widthPercentageToDP(8),
  },
  contentSettings: {
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp1,
    marginHorizontal: Size.wp4,
    marginVertical: Size.hp2,
    width: Size.wp92,
    backgroundColor: Color.cream,
    borderRadius: Size.ms4,
  },
});
