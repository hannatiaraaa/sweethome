import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {Input} from 'react-native-elements';

// global
import {Color, Size} from '../../../shared/global/config';

export const ProfileInput = ({
  label,
  value,
  autoCapitalize,
  onChangeText,
  disabled = false,
  secureTextEntry = false,
  validation = false,
  errorMessage,
  error = true,
  keyboardType,
}) => {
  const [focused, setFocused] = useState(true);

  useEffect(() => {
    handleFocus();
    handleBlur();
  }, []);

  const handleFocus = () => {
    setFocused(true);
  };

  const handleBlur = () => {
    setFocused(false);
  };

  return (
    <Input
      onFocus={handleFocus}
      onBlur={handleBlur}
      autoCapitalize={autoCapitalize}
      disabled={disabled}
      placeholder={label}
      placeholderTextColor={Color.ashGray}
      keyboardType={keyboardType}
      value={value}
      onChangeText={onChangeText}
      label={label}
      labelStyle={[styles.text, styles.textLabel]}
      errorStyle={styles.textError}
      errorMessage={
        focused && !value && error
          ? 'This column should be field'
          : focused && validation
          ? errorMessage
          : null
      }
      secureTextEntry={secureTextEntry}
      inputContainerStyle={styles.box}
      inputStyle={[styles.text, styles.textInput]}
    />
  );
};

const styles = StyleSheet.create({
  text: {
    color: Color.primaryBlue,
    fontSize: Size.ms14,
  },
  textLabel: {
    fontFamily: 'NotoSans-Bold',
  },
  textInput: {
    fontFamily: 'NotoSans-Regular',
  },
  textError: {
    color: Color.errorPink,
  },
  box: {
    marginTop: Size.ms8,
    borderWidth: Size.ms1,
    borderRadius: Size.ms4,
    paddingHorizontal: Size.wp4,
    width: Size.wp92,
    borderColor: Color.primaryBlue,
  },
});
