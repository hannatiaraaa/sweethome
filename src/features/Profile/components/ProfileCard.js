import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';

// component
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

export const ProfileCard = ({
  title,
  content,
  icon,
  icon2,
  arrow = false,
  onPress,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      style={[
        Layouting().flexRow,
        Layouting().centered,
        Layouting().shadow,
        styles.container,
        {
          backgroundColor:
            title === 'Login' ? Color.secondaryYellow : Color.cream,
        },
      ]}>
      <View style={Layouting().flex}>
        {icon ? (
          <Ionicons name={icon} size={Size.ms32} color="#262F56" />
        ) : (
          <MaterialIcons name={icon2} size={Size.ms32} color="#262F56" />
        )}
      </View>
      <View
        style={[
          title === 'Login'
            ? Layouting(undefined, 6).flex
            : Layouting(undefined, 5).flex,
          Layouting('flex-start').spaceAround,
          styles.details,
        ]}>
        <NotoSans
          title={title}
          type="Bold"
          color={Color.primaryBlue}
          size={Size.ms14}
        />
        <NotoSans title={content} color={Color.primaryBlue} />
      </View>

      {arrow && (
        <Ionicons name="chevron-forward" size={Size.ms28} color="#262F56" />
      )}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  details: {
    paddingVertical: Size.hp2,
    height: Size.ms80,
  },
  container: {
    height: Size.ms80,
    borderRadius: Size.ms4,
    marginVertical: Size.ms16,
    paddingHorizontal: Size.wp4,
  },
});
