export const GET_PROFILE = 'GET_PROFILE';
export const GET_PRIVACY = 'GET_PRIVACY';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const UPDATE_ACCOUNT = 'UPDATE_ACCOUNT';
export const SET_PROFILE = 'SET_PROFILE';
export const SET_PRIVACY = 'SET_PRIVACY';
export const SET_FINISHED = 'SET_FINISHED';
export const UPLOAD_PROFILE_IMAGE = 'UPLOAD_PROFILE_IMAGE';

export const getProfile = () => {
  return {
    type: GET_PROFILE,
  };
};

export const getPrivacy = () => {
  return {
    type: GET_PRIVACY,
  };
};

export const updateProfile = (userUpdate) => {
  return {
    type: UPDATE_PROFILE,
    userUpdate,
  };
};

export const updateAccount = (accountUpdate) => {
  return {
    type: UPDATE_ACCOUNT,
    accountUpdate,
  };
};

export const setProfile = (payload) => {
  return {
    type: SET_PROFILE,
    payload,
  };
};

export const setPrivacy = (payload) => {
  return {
    type: SET_PRIVACY,
    payload,
  };
};

export const setFinished = (payload) => {
  return {
    type: SET_FINISHED,
    payload,
  };
};

export const uploadProfileImage = (formData, screen) => {
  return {
    type: UPLOAD_PROFILE_IMAGE,
    formData,
    screen,
  };
};
