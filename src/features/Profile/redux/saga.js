import {Alert, ToastAndroid} from 'react-native';
import {all, call, put, takeLatest} from 'redux-saga/effects';

// router
import {navigate} from '../../../router/navigate';

// api
import {request} from '../../../shared/utils/api';
import {
  PROFILE,
  PASSWORD,
  PRIVACY,
  UPLOAD,
} from '../../../shared/utils/endPoint';

// action
import {setAlert, setLoading} from '../../../store/GlobalAction';
import {
  GET_PROFILE,
  GET_PRIVACY,
  UPDATE_PROFILE,
  UPDATE_ACCOUNT,
  setProfile,
  setFinished,
  setPrivacy,
  getProfile,
  UPLOAD_PROFILE_IMAGE,
} from './action';

function* getProfileSaga() {
  const res = yield call(request, `${PROFILE}`, 'GET');

  if (res.status) {
    yield put(setProfile(res.data.data));
    yield put(setFinished(true));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getPrivacySaga() {
  const res = yield call(request, `${PRIVACY}`, 'GET');

  if (res.status) {
    yield put(setPrivacy(res.data.data));
    yield navigate('MainTab', {
      screen: 'ProfileRouter',
      params: {screen: 'AccountSettings'},
    });
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* updateProfileSaga({userUpdate}) {
  try {
    yield put(setLoading(true));
    const res = yield call(request, `${PROFILE}`, 'PUT', {body: userUpdate});

    if (res.status) {
      yield put(getProfile());
      yield navigate('MainTab', {
        screen: 'ProfileRouter',
        params: {screen: 'LoggedIn'},
      });
    } else {
      yield put(
        setAlert({
          status: true,
          title: 'Failed to save your changes',
        }),
      );
    }
  } finally {
    yield put(setLoading(false));
  }
}

function* updateAccountSaga({accountUpdate}) {
  try {
    yield put(setLoading(true));
    const res = yield call(request, `${PROFILE}${PASSWORD}`, 'PUT', {
      body: accountUpdate,
    });

    if (res.status) {
      yield put(getProfile());
      yield navigate('MainTab', {
        screen: 'ProfileRouter',
        params: {screen: 'LoggedIn'},
      });
    } else {
      yield put(
        setAlert({
          status: true,
          title: 'Failed to save your changes',
        }),
      );
    }
  } finally {
    yield put(setLoading(false));
  }
}

function* uploadProfileImageSaga({formData, screen}) {
  try {
    yield put(setLoading(true));
    const {uri, fileName, type} = formData;
    const body = new FormData();
    body.append('photo', {uri, name: fileName, type});
    const res = yield call(request, `${UPLOAD}`, 'POST', {
      body: body,
    });

    if (res.status) {
      yield put(getProfile());
      if (screen === 'LoggedIn') {
        yield navigate('MainTab', {
          screen: 'ProfileRouter',
          params: {screen: 'LoggedIn'},
        });
      }
      if (screen === 'ProfileSettings') {
        yield navigate('MainTab', {
          screen: 'ProfileRouter',
          params: {screen: 'ProfileSettings'},
        });
      }
      if (screen === 'AccountSettings') {
        yield navigate('MainTab', {
          screen: 'ProfileRouter',
          params: {screen: 'AccountSettings'},
        });
      }
    } else {
      ToastAndroid.showWithGravity(
        'Try to upload your profile photo again',
        ToastAndroid.LONG,
        ToastAndroid.TOP,
      );
    }
  } finally {
    yield put(setLoading(false));
  }
}

export function* ProfileSaga() {
  yield all([
    takeLatest(GET_PROFILE, getProfileSaga),
    takeLatest(GET_PRIVACY, getPrivacySaga),
    takeLatest(UPLOAD_PROFILE_IMAGE, uploadProfileImageSaga),
    takeLatest(UPDATE_PROFILE, updateProfileSaga),
    takeLatest(UPDATE_ACCOUNT, updateAccountSaga),
  ]);
}
