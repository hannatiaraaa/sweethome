import {SET_PROFILE, SET_FINISHED, SET_PRIVACY} from './action';

const initialState = {
  user: {},
  privacy: [],
  isFinished: false,
};

export const ProfileReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PROFILE:
      return {
        ...state,
        user: action.payload,
      };

    case SET_PRIVACY:
      return {
        ...state,
        privacy: action.payload,
      };

    case SET_FINISHED:
      return {
        ...state,
        isFinished: action.payload,
      };

    default:
      return state;
  }
};
