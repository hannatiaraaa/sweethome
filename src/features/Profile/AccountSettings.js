import React, {useEffect, useState} from 'react';
import {ScrollView, View, StyleSheet} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';

// component
import Loading from '../../shared/components/Loading';
import SkeletonLoading from '../../shared/components/SkeletonLoading';
import UploadBottomSheet from '../../shared/components/UploadBottomSheet';
import NotoSans from '../../shared/components/NotoSans';
import ProfileCover from './components/ProfileCover';
import {ProfileInput} from './components/ProfileInput';
import {SwitchLine} from './components/SwitchLine';
import {Dropdown} from '../../shared/components/DropDown';
import {YellowButton} from '../../shared/components/YellowButton';

// global
import {Color, Size} from '../../shared/global/config';
import {Layouting} from '../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {updateAccount} from './redux/action';

function AccountSettings(props) {
  const {user, privacy} = props.ProfileReducer;
  const {showcaseAll, isLoading} = props;
  const [bottomSheetVisible, setBottomSheetVisible] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [statusUpdate, setStatusUpdate] = useState(user.statusUpdate);
  const [event, setEvent] = useState(user.event);
  const [newsletter, setNewsletter] = useState(user.newsletter);
  const [privacyValue, setPrivacyValue] = useState('');

  useEffect(() => {
    if (user.privacy) {
      privacy.map((item) => {
        if (user.privacy === item._id) {
          setPrivacyValue(item._id);
        }
      });
    } else {
      setPrivacyValue(privacy[0]._id);
    }
  }, []);

  const validatePassword = (text) => {
    setPassword(text);
    if (password.length >= 5) {
      setPasswordError(null);
      setDisabled(false);
    } else {
      setDisabled(true);
      setPasswordError('Password length must be at least 6 characters long');
    }
  };

  const privacyItem = privacy.length
    ? privacy.map((item) => ({value: item._id, label: item.name}))
    : [];

  const saveUpdate = () => {
    const accountUpdate = {
      password,
      statusUpdate,
      event,
      newsletter,
      privacy: privacyValue,
    };
    props.updateAccount(accountUpdate);
  };

  const RenderNotification = () => {
    return (
      <View style={[Layouting('flex-start').spaceAround, styles.notification]}>
        <NotoSans
          title="Notification"
          type="Bold"
          color={Color.primaryBlue}
          size={Size.ms14}
        />
        <SwitchLine
          title="Status Update"
          value={statusUpdate}
          onValueChange={() => setStatusUpdate((prevState) => !prevState)}
        />
        <SwitchLine
          title="Events/Recommended Contents"
          value={event}
          onValueChange={() => setEvent((prevState) => !prevState)}
        />
        <SwitchLine
          title="Newsletter"
          value={newsletter}
          onValueChange={() => setNewsletter((prevState) => !prevState)}
        />
      </View>
    );
  };

  const RenderPrivacy = () => {
    return (
      <View style={styles.privacy}>
        <NotoSans
          title="Privacy"
          type="Bold"
          size={Size.ms14}
          color={Color.primaryBlue}
        />
        <NotoSans
          title="Who can see my account?"
          size={Size.ms14}
          color={Color.primaryBlue}
          style={styles.content}
        />
        <Dropdown
          items={privacyItem}
          defaultValue={privacyValue}
          onChangeItem={(item) => setPrivacyValue(item.value)}
        />
      </View>
    );
  };

  if (!showcaseAll.length && !user.length) {
    return <SkeletonLoading />;
  } else {
    return (
      <View style={Layouting().flex}>
        {isLoading ? (
          <Loading />
        ) : (
          <>
            <ScrollView>
              <ProfileCover
                routeName="Account Settings"
                isLogin={props.route.name === 'AccountSettings' ? true : false}
                onPress={() => setBottomSheetVisible(true)}
              />
              <View style={[Layouting().flexStart, styles.container]}>
                <View>
                  <ProfileInput
                    label="Enter new password"
                    value={password}
                    secureTextEntry={true}
                    validation={true}
                    onChangeText={(text) => validatePassword(text)}
                    errorMessage={passwordError}
                  />
                </View>
                <RenderNotification />
                <RenderPrivacy />

                <YellowButton
                  title="Save Changes"
                  width={Size.wp92}
                  disabled={disabled}
                  onPress={saveUpdate}
                />
              </View>
            </ScrollView>
            <UploadBottomSheet
              route={props.route}
              isVisible={bottomSheetVisible}
              onRequestClose={() => setBottomSheetVisible(false)}
              onClose={() => setBottomSheetVisible(false)}
            />
          </>
        )}
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  ProfileReducer: state.ProfileReducer,
  showcaseAll: state.HomeReducer.showcaseAll,
  isLoading: state.GlobalReducer.isLoading,
});

const mapDispatchToProps = {
  updateAccount,
};

export default connect(mapStateToProps, mapDispatchToProps)(AccountSettings);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp2,
    height: heightPercentageToDP(70),
    paddingVertical: Size.hp1,
    marginBottom: Size.hp4,
  },
  notification: {
    marginVertical: Size.hp1,
  },
  privacy: {
    marginTop: Size.hp2,
    marginBottom: heightPercentageToDP(14),
  },
  content: {
    marginVertical: Size.ms4,
    width: Size.wp92,
  },
});
