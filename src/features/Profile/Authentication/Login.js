import React, {useState} from 'react';
import {View, ImageBackground, ScrollView, StyleSheet} from 'react-native';

// component
import SkeletonLoading from '../../../shared/components/SkeletonLoading';
import Loading from '../../../shared/components/Loading';
import NotoSans from '../../../shared/components/NotoSans';
import {BackdropImage} from './components/BackdropImage';
import {MintButton} from './components/MintButton';
import {AuthInput} from './components/AuthInput';
import {YellowButton} from '../../../shared/components/YellowButton';
import {FacebookModal} from './components/FacebookModal';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import FontAwesome from 'react-native-vector-icons/FontAwesome';

// redux
import {connect} from 'react-redux';
import {actionLogin, getGoogle, getFacebook} from './redux/action';
import {setLoading} from '../../../store/GlobalAction';

function Login(props) {
  const {isLoading, showcaseAll} = props;
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [facebookVisible, setFacebookVisible] = useState(false);

  const validateEmail = (text) => {
    setEmail(text);
  };

  const validatePassword = (text) => {
    setPassword(text);
  };

  const goRegister = () => {
    props.navigation.navigate('Register');
  };

  const loginProcess = () => {
    props.actionLogin({
      email,
      password,
    });
  };

  const goFacebook = () => {
    props.getFacebook();
    setFacebookVisible(true);
  };

  if (
    !showcaseAll.length &&
    !showcaseAll[5] &&
    !showcaseAll[5].gallery.length
  ) {
    return <SkeletonLoading />;
  } else {
    return (
      <>
        {isLoading ? (
          <Loading />
        ) : (
          <>
            <FacebookModal
              isVisible={facebookVisible}
              onPress={() => setFacebookVisible(!facebookVisible)}
            />
            <ImageBackground
              source={{
                uri:
                  showcaseAll[5].gallery[showcaseAll[5].gallery.length - 1]
                    .photo,
              }}
              style={Layouting().flex}>
              <View style={Layouting().flex}>
                <ScrollView
                  alwaysBounalceVertical={true}
                  contentContainerStyle={styles.cover}>
                  <BackdropImage image={showcaseAll[5].gallery[1].photo} />
                </ScrollView>
              </View>

              <View
                style={[Layouting(undefined, 1.8).flex, Layouting().flexEnd]}>
                <ScrollView
                  contentContainerStyle={[
                    Layouting().spaceEvenly,
                    styles.container,
                  ]}>
                  <View
                    style={[
                      Layouting('flex-start').spaceAround,
                      styles.heading,
                    ]}>
                    <NotoSans
                      title="Welcome Back!"
                      size={Size.ms24}
                      type="Bold"
                    />
                    <View style={Layouting().flexRow}>
                      <NotoSans
                        title="Don’t have account? "
                        color={Color.ashGray}
                      />
                      <NotoSans
                        title="Sign up"
                        color={Color.primaryBlue}
                        style={styles.textLinked}
                        onPress={goRegister}
                      />
                    </View>
                  </View>

                  <View
                    style={[
                      Layouting().flexRow,
                      Layouting().spaceBetween,
                      styles.content,
                    ]}>
                    <MintButton
                      icon={
                        <FontAwesome
                          name="google"
                          size={Size.ms16}
                          color={Color.primaryBlue}
                        />
                      }
                      title="Google"
                      onPress={() => props.getGoogle()}
                    />
                    <MintButton
                      icon={
                        <FontAwesome
                          name="facebook"
                          size={Size.ms16}
                          color={Color.primaryBlue}
                        />
                      }
                      title="Facebook"
                      onPress={goFacebook}
                    />
                  </View>

                  <View style={Layouting().spaceAround}>
                    <AuthInput
                      placeholder="Email"
                      autoCapitalize="none"
                      onChangeText={(text) => {
                        validateEmail(text);
                      }}
                      errorMessage={
                        !email.trim() ? 'Please enter your email address' : null
                      }
                    />
                    <AuthInput
                      placeholder="Password"
                      onChangeText={(text) => {
                        validatePassword(text);
                      }}
                      errorMessage={
                        !password.trim() ? 'Please enter your password' : null
                      }
                      secureTextEntry={true}
                    />

                    <YellowButton
                      title="Login"
                      width={Size.wp92}
                      onPress={loginProcess}
                      disabled={email && password ? false : true}
                    />
                  </View>

                  <NotoSans
                    title="Forgot Password"
                    color={Color.primaryBlue}
                    style={styles.textLinked}
                  />
                </ScrollView>
              </View>
            </ImageBackground>
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  isLoading: state.GlobalReducer.isLoading,
  showcaseAll: state.HomeReducer.showcaseAll,
});

const mapDispatchToProps = {
  actionLogin,
  getGoogle,
  getFacebook,
  setLoading,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

const styles = StyleSheet.create({
  cover: {
    height: Size.hp40,
  },
  container: {
    paddingHorizontal: Size.wp2,
    backgroundColor: 'white',
    height: Size.hp60,
    flexGrow: 1,
  },
  heading: {
    width: Size.wp84,
  },
  content: {
    width: Size.wp92,
  },
  textLinked: {
    textDecorationLine: 'underline',
  },
});
