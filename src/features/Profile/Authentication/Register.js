import React, {useState} from 'react';
import {
  View,
  ImageBackground,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';

// component
import SkeletonLoading from '../../../shared/components/SkeletonLoading';
import Loading from '../../../shared/components/Loading';
import NotoSans from '../../../shared/components/NotoSans';
import {BackdropImage} from './components/BackdropImage';
import {MintButton} from './components/MintButton';
import {AuthInput} from './components/AuthInput';
import {FacebookModal} from './components/FacebookModal';
import {YellowButton} from '../../../shared/components/YellowButton';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import FontAwesome from 'react-native-vector-icons/FontAwesome';

// redux
import {connect} from 'react-redux';
import {actionRegister, getGoogle, getFacebook} from './redux/action';
import {getShowcaseDetails} from '../../Home/redux/action';
import {setLoading} from '../../../store/GlobalAction';

function Register(props) {
  const {isLoading, showcaseAll} = props;
  const [disabled, setDisabled] = useState(true);
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastname] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [firstnameError, setFirstnameError] = useState('');
  const [lastnameError, setLastnameError] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [facebookVisible, setFacebookVisible] = useState(false);

  const validateFirstName = (text) => {
    setFirstname(text);
    if (firstname.length >= 2) {
      setFirstnameError(null);
      if (firstname && lastname && email && password) {
        setDisabled(false);
      }
    } else {
      setDisabled(true);
      setFirstnameError('First Name length must be at least 3 characters long');
    }
  };

  const validateLastName = (text) => {
    setLastname(text);
    if (lastname.length >= 2) {
      setLastnameError(null);
      if (firstname && lastname && email && password) {
        setDisabled(false);
      }
    } else {
      setDisabled(true);
      setLastnameError('Last Name length must be at least 3 characters long');
    }
  };

  const validateEmail = (text) => {
    setEmail(text);
    const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[com|net]{3,}))$/;
    if (email && regex.test(text)) {
      setEmailError(null);
      if (firstname && lastname && email && password) {
        setDisabled(false);
      }
    } else {
      setDisabled(true);
      setEmailError('Please enter a valid email address');
    }
  };

  const validatePassword = (text) => {
    setPassword(text);
    if (password.length >= 5) {
      setPasswordError(null);
      if (firstname && lastname && email && password) {
        setDisabled(false);
      }
    } else {
      setDisabled(true);
      setPasswordError('Password length must be at least 6 characters long');
    }
  };

  const goLogin = () => {
    props.navigation.navigate('Login');
  };

  const registerProcess = () => {
    props.actionRegister({
      firstname,
      lastname,
      email,
      password,
    });
  };

  const goFacebook = () => {
    props.getFacebook();
    setFacebookVisible(true);
  };

  if (
    !showcaseAll.length &&
    !showcaseAll[3] &&
    !showcaseAll[3].gallery.length
  ) {
    return <SkeletonLoading />;
  } else {
    return (
      <>
        {isLoading ? (
          <Loading />
        ) : (
          <>
            <FacebookModal
              isVisible={facebookVisible}
              onPress={() => setFacebookVisible(!facebookVisible)}
            />
            <ImageBackground
              source={{
                uri:
                  showcaseAll[3].gallery[showcaseAll[3].gallery.length - 2]
                    .photo,
              }}
              style={Layouting().flex}>
              <View style={Layouting().flex}>
                <ScrollView
                  alwaysBounalceVertical={true}
                  contentContainerStyle={styles.screen}>
                  <View style={Layouting().flex}>
                    <BackdropImage
                      image={
                        showcaseAll[3].gallery[
                          showcaseAll[3].gallery.length - 1
                        ].photo
                      }
                    />
                  </View>

                  <View
                    style={[
                      Layouting(undefined, 2.2).flex,
                      Layouting().spaceEvenly,
                      styles.container,
                    ]}>
                    <View
                      style={[
                        Layouting('flex-start').spaceAround,
                        styles.heading,
                      ]}>
                      <NotoSans
                        title="Getting Started"
                        size={Size.ms24}
                        type="Bold"
                      />
                      <View style={Layouting().flexRow}>
                        <NotoSans
                          title="Already have account? "
                          color={Color.ashGray}
                        />
                        <NotoSans
                          title="Login"
                          color={Color.primaryBlue}
                          style={styles.textLinked}
                          onPress={goLogin}
                        />
                      </View>
                    </View>

                    <View
                      style={[
                        Layouting().flexRow,
                        Layouting().spaceBetween,
                        styles.content,
                      ]}>
                      <MintButton
                        icon={
                          <FontAwesome
                            name="google"
                            size={Size.ms16}
                            color={Color.primaryBlue}
                          />
                        }
                        title="Google"
                        onPress={() => props.getGoogle()}
                      />
                      <MintButton
                        icon={
                          <FontAwesome
                            name="facebook"
                            size={Size.ms16}
                            color={Color.primaryBlue}
                          />
                        }
                        title="Facebook"
                        onPress={goFacebook}
                      />
                    </View>

                    <View style={Layouting().spaceAround}>
                      <AuthInput
                        placeholder="First Name"
                        autoCapitalize="words"
                        onChangeText={(text) => {
                          validateFirstName(text);
                        }}
                        errorMessage={firstnameError}
                      />
                      <AuthInput
                        placeholder="Last Name"
                        autoCapitalize="words"
                        onChangeText={(text) => {
                          validateLastName(text);
                        }}
                        errorMessage={lastnameError}
                      />
                      <AuthInput
                        placeholder="Email"
                        autoCapitalize="none"
                        onChangeText={(text) => {
                          validateEmail(text);
                        }}
                        errorMessage={emailError}
                      />
                      <AuthInput
                        placeholder="Password"
                        onChangeText={(text) => {
                          validatePassword(text);
                        }}
                        errorMessage={passwordError}
                        secureTextEntry={true}
                      />

                      <YellowButton
                        title="Sign Up"
                        width={Size.wp92}
                        onPress={registerProcess}
                        disabled={disabled}
                      />
                    </View>
                  </View>
                </ScrollView>
              </View>
            </ImageBackground>
          </>
        )}
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  isLoading: state.GlobalReducer.isLoading,
  showcaseAll: state.HomeReducer.showcaseAll,
});

const mapDispatchToProps = {
  actionRegister,
  getGoogle,
  getFacebook,
  setLoading,
};

export default connect(mapStateToProps, mapDispatchToProps)(Register);

const styles = StyleSheet.create({
  screen: {
    height: heightPercentageToDP(114),
  },
  container: {
    paddingHorizontal: Size.wp2,
    backgroundColor: 'white',
    height: Size.hp60,
  },
  heading: {
    width: Size.wp84,
  },
  content: {
    width: Size.wp92,
  },
  textLinked: {
    textDecorationLine: 'underline',
  },
});
