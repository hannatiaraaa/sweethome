export const REGISTER = 'REGISTER';
export const LOGIN = 'LOGIN';
export const GET_GOOGLE = 'GET_GOOGLE';
export const GET_FACEBOOK = 'GET_FACEBOOK';
export const SET_FACEBOOK = 'SET_FACEBOOK';
export const SET_TOKEN = 'SET_TOKEN';
export const LOGOUT = 'LOGOUT';

export const actionRegister = (payload) => {
  return {
    type: REGISTER,
    firstname: payload.firstname,
    lastname: payload.lastname,
    email: payload.email,
    password: payload.password,
  };
};

export const actionLogin = (payload) => {
  return {
    type: LOGIN,
    email: payload.email,
    password: payload.password,
  };
};

export const getGoogle = () => {
  return {
    type: GET_GOOGLE,
  };
};

export const getFacebook = () => {
  return {
    type: GET_FACEBOOK,
  };
};

export const setFacebook = (payload) => {
  return {
    type: SET_FACEBOOK,
    payload,
  };
};

export const setToken = (token) => {
  return {
    type: SET_TOKEN,
    token,
  };
};

export const actionLogout = () => {
  return {
    type: LOGOUT,
  };
};
