import {Alert, Linking} from 'react-native';
import {all, call, put, takeLatest} from 'redux-saga/effects';

// api
import {request, BASE_URL} from '../../../../shared/utils/api';
import {
  FACEBOOK,
  GOOGLE,
  LOGIN_ENDPOINT,
  REGISTER_ENDPOINT,
} from '../../../../shared/utils/endPoint';

// router
import {navigate} from '../../../../router/navigate';

// action
import {setAlert, setLoading} from '../../../../store/GlobalAction';
import {
  REGISTER,
  LOGIN,
  GET_GOOGLE,
  GET_FACEBOOK,
  setToken,
  actionLogin,
  setFacebook,
  LOGOUT,
} from './action';
import {getProfile, setFinished} from '../../redux/action';
import {getAll} from '../../../Home/redux/action';
import {
  getSaved,
  getSavedAll,
  getShowcaseSavedAll,
} from '../../../Saved/redux/action';

function* LoginSaga({email, password}) {
  try {
    yield put(setLoading(true));
    const res = yield call(request, `${LOGIN_ENDPOINT}`, 'POST', {
      body: {
        email,
        password,
      },
    });

    if (res.status) {
      yield put(setToken(res.data.data.token));
      yield put(getProfile());
      yield put(getShowcaseSavedAll());
      yield put(getSavedAll());
      yield put(getSaved());
      yield navigate('MainTab', {screen: 'Home'});
    } else {
      yield put(
        setAlert({
          status: true,
          title: res.data || 'Enter a registered email address',
        }),
      );
    }
  } finally {
    yield put(setLoading(false));
  }
}

function* RegisterSaga({firstname, lastname, email, password}) {
  try {
    yield put(setLoading(true));
    const res = yield call(request, `${REGISTER_ENDPOINT}`, 'POST', {
      body: {
        firstname,
        lastname,
        email,
        password,
      },
    });

    if (res.status) {
      yield put(
        actionLogin({
          email,
          password,
        }),
      );
    } else {
      yield put(
        setAlert({
          status: true,
          title: res.data,
          numOfButtons: 2,
          labelButton: 'Login',
          navOnPress: () => navigate('Auth', {screen: 'Login'}),
        }),
      );
    }
  } finally {
    yield put(setLoading(false));
  }
}

function* GoogleSaga() {
  try {
    yield put(setLoading(true));
    const res = yield call(request, `${GOOGLE}`, 'GET');

    if (res.status) {
      yield Linking.openURL(`${BASE_URL}${GOOGLE}`);
      // yield put(setToken(res.data.data.token));
      // yield navigate({
      //   name: 'MainTab', {'Home'}},
      // });
    } else {
      yield put(
        setAlert({
          status: true,
          title: 'Failed to Login with Google',
        }),
      );
    }
  } finally {
    yield put(setLoading(false));
  }
}

function* FacebookSaga() {
  try {
    yield put(setLoading(true));
    const res = yield call(request, `${FACEBOOK}`, 'GET');

    if (res.status) {
      yield put(setFacebook(true));
      // yield put(setToken(res.data.data.token));
      // yield navigate({
      //   name: 'MainTab',
      //   params: {screen: 'Home'}},
      // });
    } else {
      yield put(
        setAlert({
          status: true,
          title: 'Failed to Login with Facebook',
        }),
      );
    }
  } finally {
    yield put(setLoading(false));
  }
}

function* LogoutSaga() {
  yield put(setFinished(false));
  yield put(getAll());
  yield navigate({
    name: 'MainTab',
    params: {screen: 'ProfileRouter', params: {screen: 'LoggedOut'}},
  });
}

export function* AuthSaga() {
  yield all([
    takeLatest(LOGIN, LoginSaga),
    takeLatest(REGISTER, RegisterSaga),
    takeLatest(GET_GOOGLE, GoogleSaga),
    takeLatest(GET_FACEBOOK, FacebookSaga),
    takeLatest(LOGOUT, LogoutSaga),
  ]);
}
