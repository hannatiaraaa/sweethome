import {REGISTER} from './action';
import {LOGIN, SET_FACEBOOK, SET_TOKEN, LOGOUT} from './action';

const initialState = {
  firstname: '',
  lastname: '',
  email: '',
  token: '',
  isLoggged: false,
  facebook: false,
};

export const AuthReducer = (state = initialState, action) => {
  switch (action.type) {
    case REGISTER:
      return {
        ...state,
        firstname: action.firstname,
        lastname: action.lastname,
        email: action.email,
      };

    case LOGIN:
      return {
        ...state,
        email: action.email,
      };

    case SET_FACEBOOK:
      return {
        ...state,
        facebook: action.payload,
      };

    case SET_TOKEN:
      return {
        ...state,
        token: action.token,
        isLoggged: true,
      };

    case LOGOUT:
      return {
        ...state,
        token: '',
        isLogged: false,
      };

    default:
      return state;
  }
};
