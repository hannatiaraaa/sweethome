import React from 'react';
import {StyleSheet, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';

// global
import {Size} from '../../../../shared/global/config';
import {Layouting} from '../../../../shared/global/Layouting';

export const BackdropImage = ({image}) => {
  return (
    <FastImage source={{uri: image}} style={Layouting().flex}>
      <LinearGradient
        colors={['rgba(33, 68, 87, 0.9)', 'rgba(33, 68, 87, 0.9)']}
        locations={[0.9, 1]}
        style={[Layouting().linearGradient]}>
        <View style={[Layouting().flex, Layouting().centered]}>
          <FastImage
            source={require('../../../../assets/images/logo.png')}
            style={styles.logo}
          />
        </View>
      </LinearGradient>
    </FastImage>
  );
};

const styles = StyleSheet.create({
  logo: {
    width: Size.ms48,
    height: Size.ms48,
  },
});
