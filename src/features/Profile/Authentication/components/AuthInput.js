import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {Input} from 'react-native-elements';

// global
import {Color, Size} from '../../../../shared/global/config';

export const AuthInput = ({
  autoCapitalize,
  placeholder,
  onChangeText,
  secureTextEntry = false,
  errorMessage,
}) => {
  const [focused, setFocused] = useState(true);

  useEffect(() => {
    handleFocus();
    handleBlur();
  }, []);

  const handleFocus = () => {
    setFocused(true);
  };

  const handleBlur = () => {
    setFocused(false);
  };

  return (
    <Input
      onFocus={handleFocus}
      onBlur={handleBlur}
      autoCapitalize={autoCapitalize}
      placeholder={placeholder}
      placeholderTextColor={Color.ashGray}
      onChangeText={onChangeText}
      errorStyle={styles.textError}
      errorMessage={focused ? errorMessage : null}
      secureTextEntry={secureTextEntry}
      inputContainerStyle={[
        styles.box,
        focused ? styles.boxInput : styles.boxNonInput,
      ]}
      inputStyle={styles.textInput}
    />
  );
};

const styles = StyleSheet.create({
  textInput: {
    color: Color.primaryBlue,
    fontSize: Size.ms14,
    fontFamily: 'NotoSans-Regular',
  },
  textError: {
    color: Color.errorPink,
  },
  box: {
    borderWidth: Size.ms1,
    borderRadius: Size.ms4,
    paddingHorizontal: Size.wp4,
    width: Size.wp92,
  },
  boxInput: {
    borderColor: Color.primaryBlue,
  },
  boxNonInput: {
    borderColor: Color.ashGray,
  },
});
