import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import WebView from 'react-native-webview';
import {Overlay} from 'react-native-elements';

// global
import {Layouting} from '../../../../shared/global/Layouting';
import {Color, Size} from '../../../../shared/global/config';

// icon
import Ionicons from 'react-native-vector-icons/Ionicons';

// api
import {BASE_URL} from '../../../../shared/utils/api';
import {FACEBOOK} from '../../../../shared/utils/endPoint';

export const FacebookModal = ({isVisible, onPress}) => {
  return (
    <Overlay isVisible={isVisible}>
      <>
        <TouchableOpacity
          style={[Layouting('flex-end').centered, styles.topContainer]}>
          <Ionicons
            name="ios-close-circle-sharp"
            size={Size.ms32}
            color={Color.secondaryYellow}
            onPress={onPress}
            style={styles.icon}
          />
        </TouchableOpacity>
        <WebView
          style={{width: Size.wp100}}
          source={{uri: `${BASE_URL}${FACEBOOK}`}}
        />
      </>
    </Overlay>
  );
};

const styles = StyleSheet.create({
  topContainer: {
    backgroundColor: '#3B5998',
    paddingHorizontal: Size.wp2,
    paddingTop: Size.hp1,
  },
  icon: {
    opacity: Size.ms10,
  },
});
