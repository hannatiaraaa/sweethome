import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';

// component
import NotoSans from '../../../../shared/components/NotoSans';

// global
import {Color, Size} from '../../../../shared/global/config';
import {Layouting} from '../../../../shared/global/Layouting';

export const MintButton = ({
  onPress,
  title,
  width = widthPercentageToDP(44),
  icon,
}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      activeOpacity={0.8}
      style={[
        Layouting().centered,
        Layouting().shadow,
        styles.button,
        {
          width: width,
        },
      ]}>
      <View style={[Layouting().flexRow, Layouting().spaceEvenly]}>
        {icon}
        <NotoSans
          title={title}
          color={Color.primaryBlue}
          size={Size.ms16}
          style={styles.content}
        />
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  button: {
    height: Size.ms40,
    backgroundColor: Color.mint,
    borderRadius: Size.ms10,
    paddingVertical: Size.hp1,
  },
  content: {
    marginLeft: Size.ms12,
  },
});
