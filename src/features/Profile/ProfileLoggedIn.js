import React, {useEffect, useState} from 'react';
import {View, StyleSheet, BackHandler} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

// component
import Loading from '../../shared/components/Loading';
import SkeletonLoading from '../../shared/components/SkeletonLoading';
import UploadBottomSheet from '../../shared/components/UploadBottomSheet';
import ProfileCover from './components/ProfileCover';
import {ProfileCard} from './components/ProfileCard';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Size} from '../../shared/global/config';

// redux
import {connect} from 'react-redux';
import {actionLogout} from './Authentication/redux/action';
import {getShowcaseAll} from '../Home/redux/action';
import {getPrivacy, getProfile} from './redux/action';

function ProfileLoggedIn(props) {
  const {token, showcaseAll, user, isLoading} = props;
  const {navigate} = props.navigation;
  const [bottomSheetVisible, setBottomSheetVisible] = useState(false);

  useEffect(() => {
    props.getShowcaseAll();
  }, []);

  useEffect(() => {
    if (!token) {
      props.navigation.navigate('LoggedOut');
    } else {
      props.getProfile();
    }
  }, [token]);

  BackHandler.addEventListener('hardwareBackPress', function () {
    BackHandler.exitApp();
  });

  const goToProfileSettings = () => {
    navigate('ProfileSettings');
  };
  const goToAccountSettings = () => {
    props.getPrivacy();
  };
  const LogoutProcess = () => {
    props.actionLogout();
  };

  if (token) {
    if (!showcaseAll.length && !user.length) {
      return <SkeletonLoading />;
    } else {
      return (
        <View style={Layouting().flex}>
          {isLoading ? (
            <Loading />
          ) : (
            <>
              <ScrollView>
                <ProfileCover
                  isLogin={true}
                  onPress={() => setBottomSheetVisible(true)}
                />
                <View
                  style={[
                    styles.container,
                    Layouting('flex-start').spaceAround,
                  ]}>
                  <ProfileCard
                    title="Profile Settings"
                    content="Profile Information Fields"
                    icon="person"
                    arrow={true}
                    onPress={goToProfileSettings}
                  />
                  <ProfileCard
                    title="Account Settings "
                    content="Password, Notification, Privacy"
                    icon2="settings"
                    arrow={true}
                    onPress={goToAccountSettings}
                  />
                  <ProfileCard
                    title="Help"
                    content="FAQs and Help Center"
                    icon="help-circle-outline"
                    arrow={true}
                  />
                  <ProfileCard
                    title="Legal Term"
                    content="Terms of Service"
                    icon2="branding-watermark"
                    arrow={true}
                  />
                  <ProfileCard
                    title="Logout"
                    content="Logout From Your Account"
                    icon="log-in-outline"
                    arrow={true}
                    onPress={LogoutProcess}
                  />
                </View>
              </ScrollView>
              <UploadBottomSheet
                route={props.route}
                isVisible={bottomSheetVisible}
                onRequestClose={() => setBottomSheetVisible(false)}
                onClose={() => setBottomSheetVisible(false)}
              />
            </>
          )}
        </View>
      );
    }
  } else {
    return <SkeletonLoading />;
  }
}

const mapStateToProps = (state) => ({
  token: state.AuthReducer.token,
  showcaseAll: state.HomeReducer.showcaseAll,
  user: state.ProfileReducer.user,
  isLoading: state.GlobalReducer.isLoading,
});

const mapDispatchToProps = {
  getShowcaseAll,
  getProfile,
  getPrivacy,
  actionLogout,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileLoggedIn);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp1,
  },
});
