import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

// component
import ProfileCover from './components/ProfileCover';
import UploadBottomSheet from '../../shared/components/UploadBottomSheet';
import SkeletonLoading from '../../shared/components/SkeletonLoading';
import {ProfileInput} from './components/ProfileInput';
import Loading from '../../shared/components/Loading';
import {YellowButton} from '../../shared/components/YellowButton';

// global
import {Size} from '../../shared/global/config';
import {Layouting} from '../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {updateProfile} from './redux/action';

function ProfileSettings(props) {
  const {user, isLoading, showcaseAll} = props;
  const [bottomSheetVisible, setBottomSheetVisible] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [firstname, setFirstname] = useState(
    user.firstname[0].toUpperCase() + user.firstname.substring(1),
  );
  const [lastname, setLastname] = useState(
    user.lastname[0].toUpperCase() + user.lastname.substring(1),
  );
  const [phone, setPhone] = useState(user.phone === 'none' ? '' : user.phone);
  const [address, setAddress] = useState(
    user.address === 'none' ? '' : user.address,
  );
  const [firstnameError, setFirstnameError] = useState('');
  const [lastnameError, setLastnameError] = useState('');

  const validateFirstName = (text) => {
    setFirstname(text);
    if (firstname.length >= 2) {
      setFirstnameError(null);
      if (firstname && lastname) {
        setDisabled(false);
      }
    } else {
      setDisabled(true);
      setFirstnameError('First Name length must be at least 3 characters long');
    }
  };

  const validateLastName = (text) => {
    setLastname(text);
    if (lastname.length >= 2) {
      setLastnameError(null);
      if (firstname && lastname) {
        setDisabled(false);
      }
    } else {
      setDisabled(true);
      setLastnameError('Last Name length must be at least 3 characters long');
    }
  };

  const saveUpdate = () => {
    const userUpdate = {
      firstname,
      lastname,
      phone,
      address,
    };
    props.updateProfile(userUpdate);
  };

  if (!showcaseAll.length && !user.length) {
    return <SkeletonLoading />;
  } else {
    return (
      <View style={Layouting().flex}>
        {isLoading ? (
          <Loading />
        ) : (
          <ScrollView>
            <ProfileCover
              routeName="Profile Settings"
              isLogin={props.route.name === 'ProfileSettings' ? true : false}
              onPress={() => setBottomSheetVisible(true)}
            />
            <View style={[Layouting().spaceAround, styles.container]}>
              <ProfileInput
                autoCapitalize="words"
                label="First Name"
                value={firstname}
                validation={true}
                onChangeText={(text) => validateFirstName(text)}
                errorMessage={firstnameError}
              />
              <ProfileInput
                autoCapitalize="words"
                label="Last Name"
                value={lastname}
                validation={true}
                onChangeText={(text) => validateLastName(text)}
                errorMessage={lastnameError}
              />
              <ProfileInput
                label="Phone Number"
                keyboardType="numeric"
                value={phone}
                onChangeText={(text) => {
                  setPhone(text);
                  setDisabled(false);
                }}
                error={false}
              />
              <ProfileInput label="Email" value={user.email} disabled={true} />
              <ProfileInput
                autoCapitalize="words"
                label="Address"
                value={address}
                onChangeText={(text) => {
                  setAddress(text);
                  setDisabled(false);
                }}
                error={false}
              />

              <YellowButton
                title="Save Changes"
                width={Size.wp92}
                disabled={disabled}
                onPress={saveUpdate}
              />
            </View>
          </ScrollView>
        )}
        <UploadBottomSheet
          route={props.route}
          isVisible={bottomSheetVisible}
          onRequestClose={() => setBottomSheetVisible(false)}
          onClose={() => setBottomSheetVisible(false)}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.ProfileReducer.user,
  showcaseAll: state.HomeReducer.showcaseAll,
  isLoading: state.GlobalReducer.isLoading,
});

const mapDispatchToProps = {
  updateProfile,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileSettings);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp2,
    paddingVertical: Size.hp1,
    marginBottom: Size.hp4,
  },
});
