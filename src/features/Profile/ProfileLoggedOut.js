import React, {useEffect} from 'react';
import {StyleSheet, View, ScrollView} from 'react-native';

// component
import SkeletonLoading from '../../shared/components/SkeletonLoading';
import {ProfileCard} from './components/ProfileCard';
import ProfileCover from './components/ProfileCover';

// global
import {Size} from '../../shared/global/config';
import {Layouting} from '../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {getProfile} from './redux/action';
import {getShowcaseAll} from '../Home/redux/action';

function ProfileLoggedOut(props) {
  const {token, showcaseAll} = props;
  const {navigate} = props.navigation;

  useEffect(() => {
    props.getShowcaseAll();
  }, []);

  useEffect(() => {
    if (token) {
      props.navigation.navigate('LoggedIn');
      props.getProfile();
    }
  }, [token]);

  const goLogin = () => {
    navigate('Auth', {screen: 'Login'});
  };

  if (token) {
    return <SkeletonLoading />;
  } else {
    if (!showcaseAll.length) {
      return <SkeletonLoading />;
    } else {
      return (
        <View>
          <ScrollView>
            <ProfileCover />
            <View
              style={[styles.container, Layouting('flex-start').spaceAround]}>
              <ProfileCard
                title="Login"
                content="Login to your account"
                icon="person"
                onPress={goLogin}
              />
              <ProfileCard
                title="Help"
                content="FAQs and Help Center"
                icon="md-help-circle-outline"
                arrow={true}
              />
              <ProfileCard
                title="Legal Term"
                content="Term of Service"
                icon2="branding-watermark"
                arrow={true}
              />
            </View>
          </ScrollView>
        </View>
      );
    }
  }
}

const mapStateToProps = (state) => ({
  token: state.AuthReducer.token,
  showcaseAll: state.HomeReducer.showcaseAll,
});

const mapDispatchToProps = {
  getProfile,
  getShowcaseAll,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileLoggedOut);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp1,
  },
});
