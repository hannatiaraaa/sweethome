import React from 'react';
import {StyleSheet, FlatList, View} from 'react-native';
import FastImage from 'react-native-fast-image';

// component
import SkeletonLoading from '../../shared/components/SkeletonLoading';
import ShowcaseBottomSheet from './components/ShowcaseBottomSheet';
import ShowcaseBackButton from './components/ShowcaseBackButton';

// global
import {Size} from '../../shared/global/config';

// redux
import {connect} from 'react-redux';
import {getFormAppointment} from '../Order/redux/action';

function Showcase(props) {
  const {showcaseDetails, gallery} = props;

  const goBack = () => {
    props.navigation.goBack();
  };

  const goToAppointment = () => {
    props.getFormAppointment();
  };

  if (!showcaseDetails) {
    return <SkeletonLoading />;
  } else {
    return (
      <>
        <View>
          {gallery.length && (
            <FlatList
              data={gallery}
              keyExtractor={(item) => item._id.toString()}
              renderItem={({item}) => {
                return (
                  <FastImage
                    source={{uri: item.photo}}
                    style={styles.cover}
                    resizeMode="cover"
                  />
                );
              }}
            />
          )}
          <ShowcaseBottomSheet
            onPress={goToAppointment}
            data={showcaseDetails}
          />
        </View>

        <ShowcaseBackButton onPress={goBack} />
      </>
    );
  }
}

const mapStateToProps = (state) => ({
  showcaseDetails: state.HomeReducer.showcaseDetails,
  gallery: state.HomeReducer.gallery,
});

const mapDispatchToProps = {
  getFormAppointment,
};

export default connect(mapStateToProps, mapDispatchToProps)(Showcase);

const styles = StyleSheet.create({
  cover: {
    marginBottom: Size.hp2,
    width: Size.wp100,
    aspectRatio: 1,
  },
});
