import React from 'react';
import {StyleSheet, TouchableHighlight} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function ShowcaseBackButton({onPress}) {
  return (
    <TouchableHighlight
      activeOpacity={0.8}
      onPress={onPress}
      underlayColor={Color.cloud}
      style={[Layouting().centered, styles.circle]}>
      <FontAwesome5
        name="arrow-left"
        color={Color.primaryBlue}
        size={Size.ms16}
      />
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  circle: {
    width: Size.ms40,
    height: Size.ms40,
    borderRadius: Size.ms80,
    backgroundColor: 'white',
    position: 'absolute',
    top: heightPercentageToDP(6),
    left: Size.hp2,
  },
});
