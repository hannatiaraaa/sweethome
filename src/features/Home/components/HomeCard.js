import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import FastImage from 'react-native-fast-image';
import moment from 'moment';
import {moderateScale} from 'react-native-size-matters';
import {widthPercentageToDP} from 'react-native-responsive-screen';

// component
import NotoSans from '../../../shared/components/NotoSans';
import {TextCard} from '../../../shared/components/TextCard';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// redux
import {connect} from 'react-redux';
import {getAppointment, getProject} from '../../Order/redux/action';

export const HomeCard = (props) => {
  const {
    token,
    appointment,
    project,
    navProject,
    navAppointment,
    navNewAppointment,
    navAuth,
  } = props;
  const [haveProject, setHaveProject] = useState(false);
  const [haveAppointment, setHaveAppointment] = useState(false);
  const [slide, setSlide] = useState(0);

  useEffect(() => {
    if (token) {
      props.getAppointment();
      props.getProject();
    }
  }, [token]);

  useEffect(() => {
    if (token) {
      const slideshow = setInterval(() => {
        if (slide < 3) {
          setSlide(slide + 1);
        } else {
          setSlide(0);
        }
      }, 5000);
      if (project.length && !slide) {
        setHaveProject(true);
      } else if (appointment.length && slide === 1) {
        setHaveProject(false);
        setHaveAppointment(true);
      } else {
        setHaveProject(false);
        setHaveAppointment(false);
      }
      return () => clearInterval(slideshow);
    } else {
      setHaveProject(false);
      setHaveAppointment(false);
    }
  }, [slide, token]);

  const haveOrder =
    (haveAppointment && haveProject) || haveAppointment || haveProject;

  const date =
    appointment.length && moment(appointment[0].date).format('DD MMMM YYYY');

  const RenderHaveOrderTop = () => {
    return (
      <View
        style={[Layouting().flexRow, Layouting().spaceBetween, styles.content]}>
        <NotoSans
          title={
            haveProject
              ? 'Ongoing Project'
              : haveAppointment
              ? 'Upcoming Appointment'
              : null
          }
          color={Color.ashGray}
        />
        <FontAwesome5 name="arrow-right" color={Color.primaryBlue} />
      </View>
    );
  };

  const RenderHaveProjectContent = () => {
    return (
      <View
        style={
          project[0].packages.length === 1
            ? Layouting().spaceAround
            : Layouting().centered
        }>
        <View
          style={[
            Layouting().flexRow,
            Layouting().spaceBetween,
            styles.content,
          ]}>
          <NotoSans
            title={
              <>
                {project[0].packages[0].location.name} -{' '}
                {project[0].packages[0].projectType.name}
              </>
            }
            type="Bold"
            size={Size.ms14}
            color={Color.primaryBlue}
            style={styles.contentWrap}
          />
          <View
            style={[
              Layouting().flexRow,
              Layouting('flex-end').spaceAround,
              {width: Size.ms80},
            ]}>
            <MaterialCommunityIcons
              name="timer-sand"
              size={Size.ms20}
              color={Color.primaryBlue}
            />
            <NotoSans
              title={
                project[0].totalDuration > 1 ? (
                  <>{project[0].totalDuration} Weeks</>
                ) : (
                  <>{project[0].totalDuration} Week</>
                )
              }
              color={Color.primaryBlue}
            />
          </View>
        </View>
        {project[0].packages.length > 1 ? (
          <NotoSans
            title={
              project[0].packages.length > 2 ? (
                <>+ {project[0].packages.length - 1} more packages</>
              ) : (
                <>+ {project[0].packages.length - 1} more package</>
              )
            }
            color={Color.ashGray}
            size={Size.ms10}
            style={styles.content}
          />
        ) : null}
      </View>
    );
  };

  const RenderHaveAppointmentContent = () => {
    return (
      <View style={[Layouting('flex-start').spaceAround, styles.content]}>
        <NotoSans
          title={
            <>
              <NotoSans
                title={<>{date} -</>}
                size={Size.ms14}
                type="Bold"
                color={Color.primaryBlue}
              />{' '}
              {appointment[0]?.timeslot.start} - {appointment[0]?.timeslot.end}
            </>
          }
          color={Color.primaryBlue}
        />
        <NotoSans
          title={
            <>
              {appointment[0].buildType.name} -{' '}
              {appointment[0].serviceType.name}
            </>
          }
          color={Color.primaryBlue}
        />
      </View>
    );
  };

  const RenderNotHaveOrder = () => {
    return (
      <View
        style={[Layouting().flex, Layouting().flexRow, Layouting().centered]}>
        <View style={Layouting().flex}>
          <FastImage
            source={require('../../../assets/images/iconHome.png')}
            resizeMode="contain"
            style={[Layouting().flex, styles.iconImage]}
          />
        </View>
        <View
          style={[
            Layouting(undefined, 3).flex,
            Layouting('flex-start').spaceEvenly,
            styles.noOrderContent,
          ]}>
          <NotoSans title="We help to find the best solution for your better home living" />
          <NotoSans
            title={
              <>
                Get Free Consultation{'  '}
                <FontAwesome5
                  name="arrow-right"
                  size={Size.ms12}
                  color={Color.primaryBlue}
                />
              </>
            }
            color={Color.primaryBlue}
            type="Bold"
          />
        </View>
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <TextCard
        backgroundColor={haveProject ? Color.mint : Color.cream}
        onPress={
          token
            ? haveProject
              ? navProject
              : haveAppointment
              ? navAppointment
              : navNewAppointment
            : navAuth
        }
        containerStyle={[
          styles.card,
          haveOrder ? styles.order : styles.noOrder,
        ]}>
        {haveOrder ? (
          <View style={[Layouting().flex, Layouting().spaceAround]}>
            <RenderHaveOrderTop />
            {haveProject ? (
              <RenderHaveProjectContent />
            ) : haveAppointment ? (
              <RenderHaveAppointmentContent />
            ) : null}
          </View>
        ) : (
          <RenderNotHaveOrder />
        )}
      </TextCard>
    </View>
  );
};

const mapStateToProps = (state) => ({
  token: state.AuthReducer.token,
  appointment: state.OrderReducer.appointment,
  project: state.OrderReducer.project,
});

const mapDispatchToProps = {
  getAppointment,
  getProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeCard);

const styles = StyleSheet.create({
  container: {
    height: Size.hp4,
  },
  card: {
    marginTop: -Size.hp10,
  },
  order: {
    paddingVertical: Size.hp1,
    height: moderateScale(92),
  },
  noOrder: {
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp2,
    height: Size.ms100,
  },
  content: {
    width: Size.wp84,
  },
  contentWrap: {
    width: widthPercentageToDP(60),
  },
  noOrderContent: {
    height: Size.ms84,
  },
  iconImage: {
    marginVertical: Size.hp2,
  },
});
