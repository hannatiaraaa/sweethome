import React, {useState} from 'react';
import {StyleSheet, ScrollView, FlatList, View} from 'react-native';
import BottomSheet from 'react-native-simple-bottom-sheet';
import {formatMoney} from 'accounting-js';

// component
import NotoSans from '../../../shared/components/NotoSans';
import {SavedButton} from '../../../shared/components/SavedButton';
import {YellowButton} from '../../../shared/components/YellowButton';

// global
import {Layouting} from '../../../shared/global/Layouting';
import {Color, Size} from '../../../shared/global/config';

// icon
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

// redux
import {connect} from 'react-redux';
import {actionSaved, delSaved} from '../../Saved/redux/action';
import {Superscript} from '../../../shared/components/Superscript';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

const ShowcaseBottomSheet = (props) => {
  const {onPress, data, token, user} = props;
  const [isPressed, setIsPressed] = useState(
    data.favorites.length &&
      data.favorites.find((value) => {
        return value.user === user._id;
      })
      ? true
      : false,
  );

  const itemSaved = () => {
    const findSaved =
      data.favorites.length &&
      data.favorites.find((value) => {
        return value.user === user._id;
      });
    if (!!findSaved) {
      props.delSaved(data._id, 'showcase');
      setIsPressed(!isPressed);
    } else {
      props.actionSaved(data._id, 'showcase');
      setIsPressed(!isPressed);
    }
  };

  const RenderIconContent = ({name, title}) => {
    return (
      <View
        style={[Layouting().flex, Layouting().flexRow, Layouting().flexStart]}>
        <MaterialCommunityIcons
          name={name}
          size={Size.ms20}
          color={Color.primaryBlue}
          style={styles.icon}
        />
        <NotoSans title={title} size={Size.ms14} />
      </View>
    );
  };

  const RenderContent = ({title, content}) => {
    return (
      <View style={Layouting().flex}>
        <NotoSans title={title} />
        <NotoSans title={content} type="Bold" size={Size.ms14} />
      </View>
    );
  };

  return (
    <View style={Layouting().flex}>
      <BottomSheet
        isOpen
        sliderMaxHeight={Size.hp80}
        sliderMinHeight={heightPercentageToDP(22)}>
        <View style={[Layouting().centered, styles.container]}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.contentContainer}>
              <View
                style={token ? Layouting().flexRow : styles.containerLoggedOut}>
                <View>
                  <NotoSans
                    title={data.name}
                    size={Size.ms16}
                    type={'Bold'}
                    style={{width: widthPercentageToDP(60)}}
                  />
                  <NotoSans
                    title={data.address}
                    size={Size.ms12}
                    style={{width: widthPercentageToDP(60)}}
                  />
                  <View
                    style={[
                      Layouting().flex,
                      Layouting().flexRow,
                      styles.content,
                    ]}>
                    <RenderIconContent
                      name={'clock-time-five'}
                      title={
                        data.project?.totalDuration > 1 ? (
                          <>{data.project?.totalDuration} Weeks</>
                        ) : data.project?.totalDuration === 1 ? (
                          <>{data.project?.totalDuration} Week</>
                        ) : null
                      }
                    />
                    <RenderIconContent
                      name={'currency-usd-circle'}
                      title={`Rp ${formatMoney(data.project?.totalPrice, {
                        symbol: '',
                        thousand: ',',
                        precision: 0,
                      })}`}
                    />
                  </View>
                </View>
                {token ? (
                  <SavedButton
                    onPress={itemSaved}
                    pressedSaved={isPressed ? true : false}
                  />
                ) : null}
              </View>

              <View
                style={[
                  Layouting().flex,
                  Layouting().flexRow,
                  Layouting('flex-start').spaceAround,
                  styles.content,
                ]}>
                <RenderContent
                  title="Property Type"
                  content={data.project?.appointment.buildType.name}
                />
                <RenderContent
                  title="Area Size"
                  content={
                    <Superscript
                      base={`${data.project?.totalArea} m`}
                      exponent="2"
                      sizeBase={Size.ms14}
                      sizeExp={Size.ms10}
                      type="Bold"
                    />
                  }
                />
              </View>

              <View style={styles.content}>
                <RenderContent
                  title="Style"
                  content={data.styles?.map((item, index) => {
                    if (data.styles.length === 1) {
                      return item.name;
                    } else {
                      if (index !== data.styles.length - 1) {
                        if (data.styles.length === 2) {
                          return `${item.name} `;
                        } else {
                          return `${item.name}, `;
                        }
                      } else {
                        return `and ${item.name}`;
                      }
                    }
                  })}
                />
              </View>
              {data.projectTypes && (
                <View>
                  <View style={[styles.content, styles.bottomBorder]}>
                    <NotoSans
                      title="Work Include"
                      size={Size.ms14}
                      type="Bold"
                    />
                  </View>
                  <ScrollView
                    contentContainerStyle={Layouting().flex}
                    horizontal={true}>
                    <View style={[Layouting().flex, styles.list]}>
                      <FlatList
                        numColumns={2}
                        data={data.projectTypes}
                        keyExtractor={(item) => item._id.toString()}
                        renderItem={({item}) => {
                          return (
                            <View
                              style={[
                                Layouting().flex,
                                Layouting('flex-start').align,
                              ]}>
                              <NotoSans
                                title={`- ${item.name}`}
                                size={Size.ms14}
                              />
                            </View>
                          );
                        }}
                      />
                    </View>
                  </ScrollView>
                </View>
              )}
            </View>
            {token ? (
              <YellowButton
                title="Request Appointment"
                type="Bold"
                onPress={onPress}
              />
            ) : null}
          </ScrollView>
        </View>
      </BottomSheet>
    </View>
  );
};

const mapStateToProps = (state) => ({
  token: state.AuthReducer.token,
  user: state.ProfileReducer.user,
});

const mapDispatchToProps = {
  actionSaved,
  delSaved,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ShowcaseBottomSheet);

const styles = StyleSheet.create({
  containerLoggedOut: {
    width: Size.wp92,
  },
  container: {
    marginHorizontal: -21,
  },
  contentContainer: {
    paddingHorizontal: Size.ms20,
    paddingBottom: Size.hp2,
  },
  icon: {
    marginRight: Size.wp2,
  },
  content: {
    marginTop: Size.hp3,
  },
  bottomBorder: {
    borderBottomWidth: Size.ms1,
    borderBottomColor: Color.vogue,
    paddingVertical: Size.hp1,
  },
  list: {
    justifyContent: 'space-between',
    paddingVertical: Size.hp2,
  },
});
