import {all, call, put, takeEvery, takeLatest} from 'redux-saga/effects';
import SplashScreen from 'react-native-splash-screen';

// router
import {navigate} from '../../../router/navigate';

// api
import {request} from '../../../shared/utils/api';
import {LOCATION, PROJECT, SHOWCASE} from '../../../shared/utils/endPoint';

// action
import {
  GET_ALL,
  GET_SHOWCASE_ALL,
  GET_SHOWCASE_DETAILS,
  GET_SHOWCASE_HOME_BY_LOCATION,
  GET_SHOWCASE_LIMIT,
  setGallery,
  setShowcaseHome,
  setShowcaseAll,
  setShowcaseDetails,
  getShowcaseAll,
  getShowcaseLimit,
} from './action';
import {
  getLocation,
  getShowcaseExplore,
  getShowcaseExploreAll,
  setShowcaseExploreAll,
} from '../../Explore/redux/action';

function* getAllSaga() {
  try {
    yield put(getLocation());
    yield put(getShowcaseLimit());
    yield put(getShowcaseAll());
    yield put(getShowcaseExploreAll());
    yield put(getShowcaseExplore());
  } finally {
    SplashScreen.hide();
  }
}

function* getShowcaseAllSaga() {
  const res = yield call(request, `${SHOWCASE}${PROJECT}`, 'GET');

  if (res.status) {
    yield put(setShowcaseAll(res.data.data));
    yield put(setShowcaseExploreAll(res.data.data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getShowcaseLimitSaga({limit = 0}) {
  const res = yield call(
    request,
    `${SHOWCASE}${PROJECT}?limit=${3 + limit}`,
    'GET',
  );

  if (res.status) {
    yield put(
      setShowcaseHome(res.data.data, res.data.activePage, res.data.totalPage),
    );
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getShowcaseHomeByLocationSaga({id, limit = 0}) {
  const res = yield call(
    request,
    `${SHOWCASE}${PROJECT}${LOCATION}?query=${id}&limit=${3 + limit}`,
    'GET',
  );

  if (res.status) {
    yield put(
      setShowcaseHome(res.data.data, res.data.activePage, res.data.totalPage),
    );
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getShowcaseDetailsSaga({id}) {
  const res = yield call(request, `${SHOWCASE}/${id}`, 'GET');

  if (res.status) {
    yield put(setShowcaseDetails(res.data.data));
    yield put(setGallery(res.data.data.gallery));
    yield navigate('Showcase');
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

export function* HomeSaga() {
  yield all([
    takeLatest(GET_SHOWCASE_ALL, getShowcaseAllSaga),
    takeLatest(GET_SHOWCASE_LIMIT, getShowcaseLimitSaga),
    takeEvery(GET_ALL, getAllSaga),
    takeLatest(GET_SHOWCASE_HOME_BY_LOCATION, getShowcaseHomeByLocationSaga),
    takeLatest(GET_SHOWCASE_DETAILS, getShowcaseDetailsSaga),
  ]);
}
