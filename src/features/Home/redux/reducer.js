import {
  SET_SHOWCASE_HOME,
  SET_SHOWCASE_ALL,
  SET_SHOWCASE_DETAILS,
  SET_GALLERY,
} from './action';

const initialState = {
  showcaseAll: [],
  showcase: [],
  showcaseDetails: {},
  gallery: [],
  activePage: 0,
  totalPage: 0,
};

export const HomeReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SHOWCASE_HOME:
      return {
        ...state,
        showcase: action.payload,
        activePage: action.activePage,
        totalPage: action.totalPage,
      };

    case SET_SHOWCASE_ALL:
      return {
        ...state,
        showcaseAll: action.payload,
      };

    case SET_SHOWCASE_DETAILS:
      return {
        ...state,
        showcaseDetails: action.payload,
      };

    case SET_GALLERY:
      return {
        ...state,
        gallery: action.payload,
      };

    default:
      return {...state, activePage: 1};
  }
};
