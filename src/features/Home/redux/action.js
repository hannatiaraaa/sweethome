export const GET_ALL = 'GET_ALL';
export const GET_SHOWCASE_ALL = 'GET_SHOWCASE_ALL';
export const GET_SHOWCASE_LIMIT = 'GET_SHOWCASE_LIMIT';
export const GET_SHOWCASE_HOME_BY_LOCATION = 'GET_SHOWCASE_HOME_BY_LOCATION';
export const GET_SHOWCASE_DETAILS = 'GET_SHOWCASE_DETAILS';
export const SET_SHOWCASE_HOME = 'SET_SHOWCASE_HOME';
export const SET_SHOWCASE_ALL = 'SET_SHOWCASE_ALL';
export const SET_SHOWCASE_DETAILS = 'SET_SHOWCASE_DETAILS';
export const SET_GALLERY = 'SET_GALLERY';

export const getAll = () => {
  return {
    type: GET_ALL,
  };
};

export const getShowcaseAll = () => {
  return {
    type: GET_SHOWCASE_ALL,
  };
};

export const getShowcaseLimit = (limit) => {
  return {
    type: GET_SHOWCASE_LIMIT,
    limit,
  };
};

export const getShowcaseHomeByLocation = (id, limit) => {
  return {
    type: GET_SHOWCASE_HOME_BY_LOCATION,
    id,
    limit,
  };
};

export const getShowcaseDetails = (id) => {
  return {
    type: GET_SHOWCASE_DETAILS,
    id,
  };
};

export const setShowcaseHome = (payload, activePage, totalPage) => {
  return {
    type: SET_SHOWCASE_HOME,
    payload,
    activePage,
    totalPage,
  };
};

export const setShowcaseAll = (payload) => {
  return {
    type: SET_SHOWCASE_ALL,
    payload,
  };
};

export const setShowcaseDetails = (payload) => {
  return {
    type: SET_SHOWCASE_DETAILS,
    payload,
  };
};

export const setGallery = (payload) => {
  return {
    type: SET_GALLERY,
    payload,
  };
};
