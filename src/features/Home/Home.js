import React, {useState, useEffect, useCallback} from 'react';
import {
  StyleSheet,
  FlatList,
  View,
  TouchableOpacity,
  ScrollView,
  BackHandler,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import LinearGradient from 'react-native-linear-gradient';
import {heightPercentageToDP} from 'react-native-responsive-screen';

// component
import SkeletonLoading from '../../shared/components/SkeletonLoading';
import NotoSans from '../../shared/components/NotoSans';
import HomeCard from './components/HomeCard';
import {FilterCard} from '../../shared/components/FilterCard';
import {DataCard} from '../../shared/components/DataCard';

// global
import {Color, Size} from '../../shared/global/config';
import {Layouting} from '../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {
  getShowcaseAll,
  getShowcaseLimit,
  getShowcaseHomeByLocation,
  getShowcaseDetails,
} from './redux/action';
import {getLocation} from '../Explore/redux/action';
import {getFormAppointment} from '../Order/redux/action';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function Home(props) {
  const {token, user, isFinished, location} = props;
  const {showcase, showcaseAll, activePage, totalPage} = props.HomeReducer;
  const {navigate} = props.navigation;
  const [refreshPage, setRefreshPage] = useState(false);
  const [newLocation, setNewLocation] = useState([]);
  const [isPressed, setIsPressed] = useState('all');
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    setRefreshPage(!refreshPage);
    setNewLocation(location);
    addLocation();
  }, [props]);

  BackHandler.addEventListener('hardwareBackPress', function () {
    BackHandler.exitApp();
  });

  const onRefresh = () => {
    setRefreshing(true);

    wait(1).then(() => {
      setRefreshing(false);
    });
  };

  const showNewShowcaseState = useCallback(() => {
    props.getShowcaseLimit(3);
  }, [props]);

  const addLocation = useCallback(() => {
    setNewLocation((prevState) => {
      if (prevState.length && prevState[0]._id !== 'all') {
        return [{_id: 'all', name: 'All'}, ...prevState];
      } else {
        return [...prevState];
      }
    });
  }, [props]);

  const displayShowcase = (id) => {
    setIsPressed(id);
    if (id === 'all') {
      props.getShowcaseLimit();
    } else {
      props.getShowcaseHomeByLocation(id);
    }
  };

  const goToAuth = () => {
    navigate('Auth', {screen: 'Register'});
  };

  const goToNewAppointment = () => {
    props.getFormAppointment();
  };

  const goToAppointment = () => {
    navigate('MainTab', {
      screen: 'OrderRouter',
      params: {screen: 'Order', params: {screen: 'Appointment'}},
    });
  };

  const goToProject = () => {
    navigate('MainTab', {
      screen: 'OrderRouter',
      params: {screen: 'Order', params: {screen: 'Project'}},
    });
  };

  const goToDetails = (id) => {
    props.getShowcaseDetails(id);
  };

  const RenderCover = () => {
    return (
      <TouchableOpacity
        activeOpacity={1}
        onPress={() => goToDetails(showcaseAll[3]._id)}>
        <FastImage
          source={{uri: showcaseAll[3].gallery[1].photo}}
          style={styles.cover}>
          <LinearGradient
            colors={['rgba(33, 68, 87, 0.1)', 'white']}
            locations={[0.5407, 0.9282]}
            style={Layouting().linearGradient}
          />
          {token && isFinished ? (
            <View
              style={[
                Layouting().flex,
                Layouting('flex-start').flexEnd,
                styles.heading,
              ]}>
              <NotoSans
                title="Welcome Home!"
                size={Size.ms14}
                color="white"
                style={styles.textShadow}
              />
              <NotoSans
                title={
                  user.firstname[0].toUpperCase() +
                  user.firstname.substring(1) +
                  ' ' +
                  user.lastname[0]?.toUpperCase() +
                  user.lastname?.substring(1)
                }
                size={Size.ms14}
                type="Bold"
                color="white"
                style={styles.textShadow}
              />
            </View>
          ) : null}
        </FastImage>
      </TouchableOpacity>
    );
  };

  if (
    !showcase.length &&
    !showcaseAll.length &&
    !location.length &&
    !newLocation.length
  ) {
    return <SkeletonLoading />;
  } else {
    return (
      <View style={Layouting().flex}>
        <ScrollView showsVerticalScrollIndicator={false}>
          {showcaseAll.length ? <RenderCover /> : null}

          <View style={[Layouting().flex, styles.container]}>
            <HomeCard
              navProject={goToProject}
              navAppointment={goToAppointment}
              navNewAppointment={goToNewAppointment}
              navAuth={goToAuth}
            />

            <View style={styles.filterContainer}>
              <NotoSans
                title="For Your Finest Idea"
                size={Size.ms14}
                type="Bold"
              />
              <FlatList
                keyExtractor={(item) => item._id.toString()}
                horizontal={true}
                scrollsToTop={false}
                showsHorizontalScrollIndicator={false}
                data={newLocation}
                renderItem={({item}) => {
                  return (
                    <FilterCard
                      title={item.name}
                      hasBorder={false}
                      onPress={() => displayShowcase(item._id)}
                      pressed={isPressed === item._id ? true : false}
                    />
                  );
                }}
              />
            </View>

            <ScrollView horizontal={true}>
              <FlatList
                onRefresh={onRefresh}
                refreshing={refreshing}
                showsVerticalScrollIndicator={false}
                keyExtractor={(item) => item._id.toString()}
                data={showcase.length && showcase}
                renderItem={({item}) => {
                  return (
                    <DataCard
                      title={item.name}
                      location={item.address}
                      sourceImage={
                        item.gallery[0]
                          ? {uri: item.gallery[0].photo}
                          : require('../../assets/images/sweethome.png')
                      }
                      onPress={() => goToDetails(item._id)}
                    />
                  );
                }}
                ListFooterComponent={
                  activePage === totalPage || !showcase.length ? null : (
                    <NotoSans
                      title="see more"
                      color={Color.primaryBlue}
                      style={styles.textSeeMore}
                      onPress={() => {
                        showNewShowcaseState();
                      }}
                    />
                  )
                }
              />
            </ScrollView>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  token: state.AuthReducer.token,
  user: state.ProfileReducer.user,
  HomeReducer: state.HomeReducer,
  location: state.ExploreReducer.location,
  isFinished: state.ProfileReducer.isFinished,
});

const mapDispatchToProps = {
  getFormAppointment,
  getShowcaseAll,
  getShowcaseLimit,
  getLocation,
  getShowcaseHomeByLocation,
  getShowcaseDetails,
};

export default connect(mapStateToProps, mapDispatchToProps)(Home);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
  cover: {
    width: Size.wp100,
    height: heightPercentageToDP(48),
  },
  heading: {
    marginBottom: heightPercentageToDP(11),
    paddingHorizontal: Size.wp4,
  },
  textShadow: {
    textShadowColor: '#000',
    textShadowOffset: {
      width: Size.ms1,
      height: Size.ms1,
    },
    textShadowRadius: Size.ms8,
  },
  filterContainer: {
    marginTop: heightPercentageToDP(6),
    marginBottom: Size.hp1,
  },
  textSeeMore: {
    marginTop: Size.hp2,
    marginBottom: Size.hp4,
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
  loadingContainer: {
    height: Size.hp100,
    backgroundColor: 'red',
  },
});
