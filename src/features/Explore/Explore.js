import React, {useState, useEffect, useCallback} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';

// component
import SkeletonLoading from '../../shared/components/SkeletonLoading';
import NotoSans from '../../shared/components/NotoSans';
import {Search} from '../../shared/components/Search';
import {DataCard} from '../../shared/components/DataCard';
import {FilterCard} from '../../shared/components/FilterCard';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Color, Size} from '../../shared/global/config';

// icon
import Ionicons from 'react-native-vector-icons/Ionicons';

// redux
import {connect} from 'react-redux';
import {getShowcaseDetails} from '../Home/redux/action';
import {
  getShowcaseExploreAll,
  getShowcaseExplore,
  getShowcaseExploreByLocation,
  getFilter,
  getSearch,
} from './redux/action';
import {actionSaved, delSaved, getSavedAll} from '../Saved/redux/action';
import {getProfile} from '../Profile/redux/action';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function Explore(props) {
  const {
    showcase,
    showcaseAll,
    location,
    activePage,
    totalPage,
  } = props.ExploreReducer;
  const {savedAll, user} = props;
  const {filtered} = props.route.params;
  const [searchBar, setSearchBar] = useState('');
  const [pressed, setPressed] = useState('');
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    props.getProfile();
  }, []);

  useEffect(() => {
    if (searchBar.length < 2) {
      props.getShowcaseExplore();
    }
  }, [searchBar, savedAll]);

  const onRefresh = () => {
    setRefreshing(true);

    wait(1).then(() => {
      props.getSavedAll();
      props.getShowcaseExplore();
      props.getShowcaseExploreAll();
      displayShowcaseByLocation('');
      setRefreshing(false);
    });
  };

  const showNewShowcaseState = useCallback(() => {
    props.getShowcaseExplore(5);
  }, [props]);

  const displayShowcaseByLocation = (id) => {
    if (id) {
      setPressed(id);
      props.getShowcaseExploreByLocation(id);
    } else {
      setPressed('');
      props.getShowcaseExplore();
    }
  };

  const itemSaved = (id, index) => {
    const findSaved =
      showcase[index].favorites.length &&
      showcase[index].favorites.find((value) => {
        return value.user === user._id;
      });
    if (!!findSaved) {
      props.delSaved(id, 'explore');
    } else {
      props.actionSaved(id, 'explore');
    }
  };

  const updateSearch = (text) => {
    setSearchBar(text);
    props.getSearch(text);
  };

  const goToDetails = (id) => {
    props.getShowcaseDetails(id);
  };

  const goToFilter = () => {
    props.getFilter();
  };

  if (!showcase.length && !location.length && !user.length) {
    return <SkeletonLoading />;
  } else {
    return (
      <View style={Layouting().flex}>
        <Search value={searchBar} onChangeText={(text) => updateSearch(text)} />
        <View style={[Layouting().flex, styles.container]}>
          <View style={[Layouting().flexRow, styles.filterContainer]}>
            <FilterCard
              style={styles.filterIcon}
              title={
                <Ionicons
                  name="options"
                  size={Size.ms20}
                  color={filtered ? 'white' : Color.primaryBlue}
                />
              }
              onPress={goToFilter}
              pressed={filtered ? true : false}
            />
            <FlatList
              keyExtractor={(item) => item._id.toString()}
              showsHorizontalScrollIndicator={false}
              horizontal={true}
              data={location}
              renderItem={({item}) => {
                return (
                  <FilterCard
                    title={item.name}
                    hasBorder={false}
                    onPress={() => displayShowcaseByLocation(item._id)}
                    pressed={pressed === item._id ? true : false}
                  />
                );
              }}
            />
          </View>
          <View style={Layouting().flex}>
            <FlatList
              onRefresh={onRefresh}
              refreshing={refreshing}
              showsVerticalScrollIndicator={false}
              keyExtractor={(item) => item?._id.toString()}
              data={searchBar.length >= 2 ? showcaseAll : showcase}
              renderItem={({item, index}) => {
                return (
                  <DataCard
                    title={item.name}
                    location={item.address}
                    sourceImage={
                      item.gallery[0]
                        ? {uri: item.gallery[0].photo}
                        : require('../../assets/images/sweethome.png')
                    }
                    onPress={() => goToDetails(item._id)}
                    hasSaved={true}
                    onPressSaved={() => itemSaved(item._id, index)}
                    pressedSaved={
                      item.favorites.length &&
                      item.favorites.find((value) => {
                        return value.user === user._id;
                      })
                        ? true
                        : false
                    }
                  />
                );
              }}
              ListFooterComponent={
                searchBar.length >= 2 ? null : activePage === totalPage ||
                  !showcase.length ? null : (
                  <NotoSans
                    title="see more"
                    color={Color.primaryBlue}
                    style={styles.textSeeMore}
                    onPress={() => {
                      showNewShowcaseState();
                    }}
                  />
                )
              }
            />
          </View>
        </View>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  ExploreReducer: state.ExploreReducer,
  savedAll: state.SavedReducer.savedAll,
  user: state.ProfileReducer.user,
});

const mapDispatchToProps = {
  getShowcaseExploreAll,
  getShowcaseExplore,
  getShowcaseExploreByLocation,
  getShowcaseDetails,
  getFilter,
  actionSaved,
  delSaved,
  getSavedAll,
  getSearch,
  getProfile,
};

export default connect(mapStateToProps, mapDispatchToProps)(Explore);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
  filterContainer: {
    paddingVertical: Size.hp1,
  },
  filterIcon: {
    marginTop: heightPercentageToDP(1.5),
    height: Size.ms32,
    marginRight: Size.ms12,
  },
  textSeeMore: {
    marginTop: Size.hp2,
    marginBottom: Size.hp4,
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});
