export const GET_LOCATION = 'GET_LOCATION';
export const GET_STYLE = 'GET_STYLE';
export const GET_FILTER = 'GET_FILTER';
export const GET_SEARCH = 'GET_SEARCH';
export const GET_SHOWCASE_EXPLORE = 'GET_SHOWCASE_EXPLORE';
export const GET_SHOWCASE_EXPLORE_ALL = 'GET_SHOWCASE_EXPLORE_ALL';
export const GET_SHOWCASE_EXPLORE_BY_LOCATION =
  'GET_SHOWCASE_EXPLORE_BY_LOCATION';
export const GET_SHOWCASE_BY_FILTER_LOCATION =
  'GET_SHOWCASE_BY_FILTER_LOCATION';
export const GET_SHOWCASE_BY_FILTER_STYLE = 'GET_SHOWCASE_BY_FILTER_STYLE';
export const GET_SHOWCASE_BY_FILTER = 'GET_SHOWCASE_BY_FILTER';
export const SET_LOCATION = 'SET_LOCATION';
export const SET_STYLE = 'SET_STYLE';
export const SET_FILTER_LOCATION = 'SET_FILTER_LOCATION';
export const SET_FILTER_STYLE = 'SET_FILTER_STYLE';
export const SET_SHOWCASE_EXPLORE = 'SET_SHOWCASE_EXPLORE';
export const SET_SHOWCASE_EXPLORE_ALL = 'SET_SHOWCASE_EXPLORE_ALL';

export const getShowcaseExplore = (limit, from) => {
  return {
    type: GET_SHOWCASE_EXPLORE,
    limit,
    from,
  };
};

export const getShowcaseExploreAll = () => {
  return {
    type: GET_SHOWCASE_EXPLORE_ALL,
  };
};

export const getShowcaseExploreByLocation = (id, limit) => {
  return {
    type: GET_SHOWCASE_EXPLORE_BY_LOCATION,
    id,
    limit,
  };
};

export const getLocation = () => {
  return {
    type: GET_LOCATION,
  };
};

export const getStyle = () => {
  return {
    type: GET_STYLE,
  };
};

export const getFilter = () => {
  return {
    type: GET_FILTER,
  };
};

export const getSearch = (text) => {
  return {
    type: GET_SEARCH,
    text,
  };
};

export const getShowcaseByFilterLocation = (arrId, limit) => {
  return {
    type: GET_SHOWCASE_BY_FILTER_LOCATION,
    arrId,
    limit,
  };
};

export const getShowcaseByFilterStyle = (arrId, limit) => {
  return {
    type: GET_SHOWCASE_BY_FILTER_STYLE,
    arrId,
    limit,
  };
};

export const getShowcaseByFilter = (arrId, limit) => {
  return {
    type: GET_SHOWCASE_BY_FILTER,
    arrId,
    limit,
  };
};

export const setShowcaseExplore = (payload, activePage, totalPage) => {
  return {
    type: SET_SHOWCASE_EXPLORE,
    payload,
    activePage,
    totalPage,
  };
};

export const setShowcaseExploreAll = (payload) => {
  return {
    type: SET_SHOWCASE_EXPLORE_ALL,
    payload,
  };
};

export const setLocation = (payload) => {
  return {
    type: SET_LOCATION,
    payload,
  };
};

export const setStyle = (payload) => {
  return {
    type: SET_STYLE,
    payload,
  };
};

export const setFilterLocation = (payload) => {
  return {
    type: SET_FILTER_LOCATION,
    payload,
  };
};

export const setFilterStyle = (payload) => {
  return {
    type: SET_FILTER_STYLE,
    payload,
  };
};
