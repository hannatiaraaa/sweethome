import {
  SET_FILTER_LOCATION,
  SET_FILTER_STYLE,
  SET_LOCATION,
  SET_SHOWCASE_EXPLORE,
  SET_SHOWCASE_EXPLORE_ALL,
  SET_STYLE,
} from './action';

const initialState = {
  showcase: [],
  showcaseAll: [],
  location: [],
  style: [],
  filterLocation: [],
  filterStyle: [],
  activePage: 0,
  totalPage: 0,
};

export const ExploreReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SHOWCASE_EXPLORE:
      return {
        ...state,
        showcase: action.payload,
        activePage: action.activePage,
        totalPage: action.totalPage,
      };

    case SET_SHOWCASE_EXPLORE_ALL:
      return {
        ...state,
        showcaseAll: action.payload,
      };

    case SET_LOCATION:
      return {
        ...state,
        location: action.payload,
      };

    case SET_STYLE:
      return {
        ...state,
        style: action.payload,
      };

    case SET_FILTER_LOCATION:
      return {
        ...state,
        filterLocation: action.payload,
      };

    case SET_FILTER_STYLE:
      return {
        ...state,
        filterStyle: action.payload,
      };

    default:
      return state;
  }
};
