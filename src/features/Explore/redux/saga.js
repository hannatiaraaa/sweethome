import {ToastAndroid} from 'react-native';
import {all, call, put, takeLatest} from 'redux-saga/effects';

// router
import {navigate} from '../../../router/navigate';

// api
import {request} from '../../../shared/utils/api';
import {
  LOCATION,
  PROJECT,
  SEARCH,
  SHOWCASE,
  STYLE,
} from '../../../shared/utils/endPoint';

// action
import {
  GET_FILTER,
  GET_SHOWCASE_EXPLORE,
  GET_SHOWCASE_EXPLORE_BY_LOCATION,
  GET_SHOWCASE_BY_FILTER,
  GET_SHOWCASE_BY_FILTER_LOCATION,
  GET_SHOWCASE_BY_FILTER_STYLE,
  GET_LOCATION,
  GET_STYLE,
  GET_SEARCH,
  setShowcaseExplore,
  setLocation,
  setStyle,
  getLocation,
  getStyle,
  getShowcaseExplore,
  setShowcaseExploreAll,
} from './action';

function* getShowcaseExploreSaga({limit = 0, from = 'explore'}) {
  const res = yield call(
    request,
    `${SHOWCASE}${PROJECT}?limit=${5 + limit}`,
    'GET',
  );

  if (res.status) {
    yield put(
      setShowcaseExplore(
        res.data.data,
        res.data.activePage,
        res.data.totalPage,
      ),
    );
    if (from === 'filter') {
      yield navigate('MainTab', {
        screen: 'ExploreRouter',
        params: {screen: 'Explore', params: {filtered: false}},
      });
    }
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getShowcaseExploreByLocationSaga({id, limit = 0}) {
  const res = yield call(
    request,
    `${SHOWCASE}${PROJECT}${LOCATION}?query=${id}&limit=${5 + limit}`,
    'GET',
  );

  if (res.status) {
    yield put(
      setShowcaseExplore(
        res.data.data,
        res.data.activePage,
        res.data.totalPage,
      ),
    );
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getLocationSaga() {
  const res = yield call(request, `${LOCATION}`, 'GET');

  if (res.status) {
    yield put(setLocation(res.data.data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getStyleSaga() {
  const res = yield call(request, `${STYLE}`, 'GET');

  if (res.status) {
    yield put(setStyle(res.data.data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getSearchSaga({text}) {
  const res = yield call(request, `${SHOWCASE}${SEARCH}?query=${text}`, 'GET');

  if (res.status) {
    yield put(setShowcaseExploreAll(res.data.data));
  }
}

function* getFilterSaga() {
  yield put(getLocation());
  yield put(getStyle());
  yield navigate('MainTab', {
    screen: 'ExploreRouter',
    params: {screen: 'Filter'},
  });
}

function* getShowcaseByFilterSaga({arrId, limit = 0}) {
  let query = '';
  arrId.forEach((value, index) => {
    query = query.concat(`query${index}=${value}&`);
  });

  const res = yield call(
    request,
    `${SHOWCASE}${PROJECT}/location&style?${query}limit=${5 + limit}`,
    'GET',
  );

  if (res.status) {
    if (res.data.data.length) {
      yield put(
        setShowcaseExplore(
          res.data.data,
          res.data.activePage,
          res.data.totalPage,
        ),
      );
    } else {
      yield put(getShowcaseExplore());
      ToastAndroid.showWithGravity(
        res.data.message,
        ToastAndroid.LONG,
        ToastAndroid.CENTER,
      );
    }
    yield navigate('MainTab', {
      screen: 'ExploreRouter',
      params: {screen: 'Explore', params: {filtered: true}},
    });
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getShowcaseByFilterLocationSaga({arrId, limit = 0}) {
  let query = '';
  arrId.forEach((value, index) => {
    query = query.concat(`query${index}=${value}&`);
  });

  const res = yield call(
    request,
    `${SHOWCASE}${PROJECT}${LOCATION}?${query}limit=${5 + limit}`,
    'GET',
  );

  if (res.status) {
    if (res.data.data.length) {
      yield put(
        setShowcaseExplore(
          res.data.data,
          res.data.activePage,
          res.data.totalPage,
        ),
      );
    } else {
      yield put(getShowcaseExplore());
      ToastAndroid.showWithGravity(
        res.data.message,
        ToastAndroid.LONG,
        ToastAndroid.CENTER,
      );
    }
    yield navigate('MainTab', {
      screen: 'ExploreRouter',
      params: {screen: 'Explore', params: {filtered: true}},
    });
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getShowcaseByFilterStyleSaga({arrId, limit = 0}) {
  let query = '';
  arrId.forEach((value, index) => {
    query = query.concat(`query${index}=${value}&`);
  });

  const res = yield call(
    request,
    `${SHOWCASE}${PROJECT}${STYLE}?${query}limit=${5 + limit}`,
    'GET',
  );

  if (res.status) {
    if (res.data.data.length) {
      yield put(
        setShowcaseExplore(
          res.data.data,
          res.data.activePage,
          res.data.totalPage,
        ),
      );
    } else {
      yield put(getShowcaseExplore());
      ToastAndroid.showWithGravity(
        res.data.message,
        ToastAndroid.LONG,
        ToastAndroid.CENTER,
      );
    }
    yield navigate('MainTab', {
      screen: 'ExploreRouter',
      params: {screen: 'Explore', params: {filtered: true}},
    });
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

export function* ExploreSaga() {
  yield all([
    takeLatest(GET_SHOWCASE_EXPLORE, getShowcaseExploreSaga),
    takeLatest(
      GET_SHOWCASE_EXPLORE_BY_LOCATION,
      getShowcaseExploreByLocationSaga,
    ),
    takeLatest(
      GET_SHOWCASE_BY_FILTER_LOCATION,
      getShowcaseByFilterLocationSaga,
    ),
    takeLatest(GET_SHOWCASE_BY_FILTER_STYLE, getShowcaseByFilterStyleSaga),
    takeLatest(GET_SHOWCASE_BY_FILTER, getShowcaseByFilterSaga),
    takeLatest(GET_LOCATION, getLocationSaga),
    takeLatest(GET_STYLE, getStyleSaga),
    takeLatest(GET_SEARCH, getSearchSaga),
    takeLatest(GET_FILTER, getFilterSaga),
  ]);
}
