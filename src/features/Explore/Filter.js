import React, {useEffect, useState} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';

// component
import {FocusAwareStatusBar} from '../../shared/components/FocusAwareStatusBar';
import HeaderFilter from './components/HeaderFilter';
import {FilterCardContainer} from '../../shared/components/FilterCardContainer';
import {FilterCard} from '../../shared/components/FilterCard';
import {YellowButton} from '../../shared/components/YellowButton';

// global
import {Size} from '../../shared/global/config';
import {Layouting} from '../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {
  getShowcaseExplore,
  getShowcaseByFilter,
  getShowcaseByFilterLocation,
  getShowcaseByFilterStyle,
  setFilterLocation,
  setFilterStyle,
} from './redux/action';

function Filter(props) {
  const {location, style, filterLocation, filterStyle} = props;
  const [selectedLoc, setSelectedLoc] = useState([]);
  const [selectedStyle, setSelectedStyle] = useState([]);
  const [newSelectedLoc, setnewSelectedLoc] = useState(selectedLoc);
  const [newSelectedStyle, setnewSelectedStyle] = useState(selectedStyle);

  useEffect(() => {
    filterLocation.map((item) => newSelectedLoc.push(item));
    filterStyle.map((item) => newSelectedStyle.push(item));
  }, []);

  const chooseLocation = (item) => {
    let selectLoc = [...newSelectedLoc];
    if (selectLoc.find((value) => value === item)) {
      let locSelected = selectLoc.filter((e) => e !== item);
      setnewSelectedLoc(locSelected);
    } else {
      selectLoc.push(item);
      setnewSelectedLoc(selectLoc);
    }
  };

  const chooseStyle = (item) => {
    let selectStyle = [...newSelectedStyle];
    if (selectStyle.find((value) => value === item)) {
      let styleSelected = selectStyle.filter((e) => e !== item);
      setnewSelectedStyle(styleSelected);
    } else {
      selectStyle.push(item);
      setnewSelectedStyle(selectStyle);
    }
  };

  const goBack = () => {
    props.navigation.goBack();
  };

  const confirmFilter = () => {
    if (!newSelectedStyle.length && !newSelectedLoc.length) {
      props.setFilterLocation([]);
      props.setFilterStyle([]);
      props.getShowcaseExplore(0, 'filter');
    } else if (!newSelectedStyle.length && newSelectedLoc.length) {
      props.setFilterLocation(newSelectedLoc);
      props.setFilterStyle([]);
      props.getShowcaseByFilterLocation(newSelectedLoc);
    } else if (!newSelectedLoc.length && newSelectedStyle.length) {
      props.setFilterLocation([]);
      props.setFilterStyle(newSelectedStyle);
      props.getShowcaseByFilterStyle(newSelectedStyle);
    } else {
      const filterConcat = newSelectedLoc.concat(newSelectedStyle);
      props.setFilterLocation(newSelectedLoc);
      props.setFilterStyle(newSelectedStyle);
      props.getShowcaseByFilter(filterConcat);
    }
  };

  return (
    <View style={Layouting().flex}>
      <FocusAwareStatusBar />
      <HeaderFilter onPress={goBack} />

      <View style={[Layouting().flex, Layouting().flexStart, styles.container]}>
        <FilterCardContainer title="Location">
          <FlatList
            keyExtractor={(item) => item._id.toString()}
            data={location}
            numColumns={3}
            renderItem={({item}) => {
              return (
                <FilterCard
                  title={item.name}
                  id={item._id}
                  onPress={() => chooseLocation(item._id)}
                  newSelected={newSelectedLoc}
                />
              );
            }}
          />
        </FilterCardContainer>

        <FilterCardContainer title="Style">
          <FlatList
            keyExtractor={(item) => item._id.toString()}
            data={style}
            numColumns={3}
            renderItem={({item}) => {
              return (
                <FilterCard
                  title={item.name}
                  id={item._id}
                  onPress={() => chooseStyle(item._id)}
                  newSelected={newSelectedStyle}
                />
              );
            }}
          />
        </FilterCardContainer>
      </View>

      <YellowButton title="Confirm" onPress={confirmFilter} />
    </View>
  );
}

const mapStateToProps = (state) => ({
  location: state.ExploreReducer.location,
  style: state.ExploreReducer.style,
  filterLocation: state.ExploreReducer.filterLocation,
  filterStyle: state.ExploreReducer.filterStyle,
});

const mapDispatchToProps = {
  getShowcaseExplore,
  getShowcaseByFilter,
  getShowcaseByFilterLocation,
  getShowcaseByFilterStyle,
  setFilterLocation,
  setFilterStyle,
};

export default connect(mapStateToProps, mapDispatchToProps)(Filter);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
});
