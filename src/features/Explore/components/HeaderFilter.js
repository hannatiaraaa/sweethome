import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {Header} from 'react-native-elements';

// component
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

export default function HeaderFilter({onPress}) {
  return (
    <Header
      statusBarProps={{
        backgroundColor: 'transparent',
        translucent: true,
      }}
      containerStyle={[
        Layouting().spaceBetween,
        {
          backgroundColor: 'white',
          height: Size.ms84,
          paddingHorizontal: Size.wp4,
        },
      ]}
      leftComponent={
        <View style={[Layouting().flex, Layouting().centered]}>
          <NotoSans title="Filter" type="Bold" size={Size.ms18} />
        </View>
      }
      rightComponent={<NotoSans title="Cancel" onPress={onPress} />}
    />
  );
}
