import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList, ScrollView} from 'react-native';

// component
import SkeletonLoading from '../../shared/components/SkeletonLoading';
import Loading from '../../shared/components/Loading';
import HeaderOrder from './components/Header';
import NotoSans from '../../shared/components/NotoSans';
import UploadBottomSheet from '../../shared/components/UploadBottomSheet';
import {InfoCard} from './components/InfoCard';
import {
  Content,
  Package,
  PackageTotal,
} from './components/ProjectDetailsContent';
import {ProjectDetailsCard} from './components/ProjectDetailsCard';
import {CancellationBottomSheet} from './components/CancellationBottomSheet';
import {DetailsFooter} from './components/DetailsFooter';
import {YellowButton} from '../../shared/components/YellowButton';

// global
import {Color, Size} from '../../shared/global/config';
import {Layouting} from '../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {getAppointmentDetails, cancelPayment} from './redux/action';

function ProjectDetails(props) {
  const {ticket} = props.route.params;
  const {projectDetails, appointmentDetails, isLoading} = props;
  const [footer, setFooter] = useState(projectDetails.status);
  const [bottomSheetVisible, setBottomSheetVisible] = useState(false);
  const [bottomSheetUploadVisible, setBottomSheetUploadVisible] = useState(
    false,
  );
  const [reason, setReason] = useState('');

  useEffect(() => {
    props.getAppointmentDetails(projectDetails.appointment);
    if (projectDetails.status === 'Done') {
      setFooter('Completed');
    }
  }, []);

  const requestCancellation = () => {
    setBottomSheetVisible(!bottomSheetVisible);
    props.cancelPayment(projectDetails._id, reason);
  };

  const goBack = () => {
    props.navigation.goBack();
  };

  if (!projectDetails && !appointmentDetails) {
    return <SkeletonLoading />;
  } else {
    return (
      <View style={Layouting().flex}>
        {isLoading ? (
          <Loading />
        ) : (
          <>
            <HeaderOrder title={<>Project - {ticket}</>} onPress={goBack} />
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.container}>
                <ProjectDetailsCard
                  project={projectDetails}
                  appointment={appointmentDetails}
                />
                {projectDetails.status === 'Waiting Payment' && (
                  <InfoCard content="Transfer to Bank BI: 000202110 (sweethome) and upload your payment receipt below." />
                )}
                <Content
                  projectId={ticket}
                  buildingType={appointmentDetails?.buildType?.name}
                  address={appointmentDetails.address}
                />
                <NotoSans
                  title={`Packages (${projectDetails.packages.length})`}
                  size={Size.ms14}
                  type="Bold"
                  style={styles.packagesHeading}
                />
                <ScrollView horizontal={true}>
                  <FlatList
                    showsVerticalScrollIndicator={false}
                    data={projectDetails.packages}
                    keyExtractor={(item) => item._id.toString()}
                    renderItem={({item}) => {
                      return (
                        <Package
                          heading={item.location.name}
                          projectType={item.projectType.name}
                          durations={item.duration}
                          area={item.area}
                          price={item.price}
                        />
                      );
                    }}
                  />
                </ScrollView>
                <PackageTotal
                  totalDuration={projectDetails.totalDuration}
                  totalPrice={projectDetails.totalPrice}
                />

                <DetailsFooter
                  status={projectDetails.status}
                  title={footer}
                  dateCreated={projectDetails.createdAt}
                  dateStatus={projectDetails.updatedAt}
                />
              </View>
              {projectDetails.status === 'Waiting Payment' && (
                <>
                  <NotoSans
                    title="Request Cancellation"
                    color={Color.errorPink}
                    style={styles.textCancel}
                    onPress={() => setBottomSheetVisible(true)}
                  />
                  <CancellationBottomSheet
                    isVisible={bottomSheetVisible}
                    projectId={ticket}
                    value={reason}
                    onChangeText={(text) => setReason(text)}
                    onPressCancel={() =>
                      setBottomSheetVisible(!bottomSheetVisible)
                    }
                    onPress={requestCancellation}
                  />
                  <YellowButton
                    title="Upload Payment Receipt"
                    onPress={() => setBottomSheetUploadVisible(true)}
                  />
                  <UploadBottomSheet
                    route={props.route}
                    isVisible={bottomSheetUploadVisible}
                    onRequestClose={() => setBottomSheetUploadVisible(false)}
                    onClose={() => setBottomSheetUploadVisible(false)}
                  />
                </>
              )}
            </ScrollView>
          </>
        )}
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  projectDetails: state.OrderReducer.projectDetails,
  appointmentDetails: state.OrderReducer.appointmentDetails,
  isLoading: state.GlobalReducer.isLoading,
});

const mapDispatchToProps = {
  getAppointmentDetails,
  cancelPayment,
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetails);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
  packagesHeading: {
    marginTop: Size.hp1,
  },
  textCancel: {
    marginVertical: Size.hp2,
    textAlign: 'center',
  },
});
