import React, {useEffect, useState} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';

// component
import {AppointmentCard} from './components/AppointmentCard';

// global
import {Size} from '../../shared/global/config';
import {Layouting} from '../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {getAppointment, getAppointmentDetails} from './redux/action';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function Appointment(props) {
  const {appointment} = props;
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    props.getAppointment();
  }, []);

  const onRefresh = () => {
    setRefreshing(true);

    wait(1).then(() => {
      props.getAppointment();
      setRefreshing(false);
    });
  };

  const goToDetails = (id) => {
    props.getAppointmentDetails(id, 'component');
  };

  return (
    <View style={[Layouting().flex, styles.container]}>
      <FlatList
        onRefresh={onRefresh}
        refreshing={refreshing}
        showsVerticalScrollIndicator={false}
        data={appointment}
        keyExtractor={(item) => item._id.toString()}
        renderItem={({item}) => {
          return (
            <AppointmentCard
              appointment={item}
              onPress={() => goToDetails(item._id)}
            />
          );
        }}
      />
    </View>
  );
}

const mapStateToProps = (state) => ({
  appointment: state.OrderReducer.appointment,
});

const mapDispatchToProps = {
  getAppointment,
  getAppointmentDetails,
};

export default connect(mapStateToProps, mapDispatchToProps)(Appointment);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
});
