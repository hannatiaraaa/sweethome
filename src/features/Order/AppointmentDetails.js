import React, {useEffect, useState} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';

// component
import HeaderOrder from './components/Header';
import NotoSans from '../../shared/components/NotoSans';
import {AppointmentCard} from './components/AppointmentCard';
import {TextCard} from '../../shared/components/TextCard';
import {Content, Note} from './components/AppointmentDetailsContent';
import {YellowButton} from '../../shared/components/YellowButton';
import {DetailsFooter} from './components/DetailsFooter';

// global
import SkeletonLoading from '../../shared/components/SkeletonLoading';
import {Color, Size} from '../../shared/global/config';
import {Layouting} from '../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {getFormAppointment} from '../Order/redux/action';

function AppointmentDetails(props) {
  const {ticket} = props.route.params;
  const {appointmentDetails} = props;
  const [footer, setFooter] = useState(appointmentDetails.status);

  useEffect(() => {
    if (appointmentDetails.status === 'Scheduled') {
      setFooter('Approved');
    }
    if (appointmentDetails.status === 'Done') {
      setFooter('Completed');
    }
  }, [appointmentDetails]);

  const goToNewAppointment = () => {
    props.getFormAppointment();
  };

  const goBack = () => {
    props.navigation.goBack();
  };

  if (!appointmentDetails) {
    return <SkeletonLoading />;
  } else {
    return (
      <View style={Layouting().flex}>
        <HeaderOrder title={<>Appointment - {ticket}</>} onPress={goBack} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.container}>
            <AppointmentCard
              isDetails={true}
              appointment={appointmentDetails}
            />
            <TextCard
              backgroundColor={Color.wheat}
              hasShadow={false}
              containerStyle={[styles.card]}>
              <NotoSans
                title="Appointment Details"
                type="Bold"
                size={Size.ms14}
              />
              <Content
                buildingType={appointmentDetails.buildType.name}
                duration={appointmentDetails.duration}
                serviceType={appointmentDetails.serviceType.name}
                budget={appointmentDetails.budget}
                address={appointmentDetails.address}
                inspectionArea={
                  appointmentDetails.locations.length
                    ? appointmentDetails.locations.map((item, index) => {
                        if (appointmentDetails.locations.length === 1) {
                          return item.name;
                        } else {
                          if (
                            index !==
                            appointmentDetails.locations.length - 1
                          ) {
                            if (appointmentDetails.locations.length === 2) {
                              return `${item.name} `;
                            } else {
                              return `${item.name}, `;
                            }
                          } else {
                            return `and ${item.name}`;
                          }
                        }
                      })
                    : null
                }
                date={appointmentDetails.date}
                startTime={appointmentDetails.timeslot.start}
                endTime={appointmentDetails.timeslot.end}
              />
            </TextCard>
            {appointmentDetails.note === 'none' ||
            !appointmentDetails.note ? null : (
              <Note note={appointmentDetails.note} />
            )}

            <DetailsFooter
              status={appointmentDetails.status}
              title={footer}
              dateCreated={appointmentDetails.createdAt}
              dateStatus={appointmentDetails.updatedAt}
            />
          </View>
          {appointmentDetails.status === 'Declined' && (
            <YellowButton
              title="Request New Appointment"
              onPress={goToNewAppointment}
            />
          )}
        </ScrollView>
      </View>
    );
  }
}

const mapStateToProps = (state) => ({
  appointmentDetails: state.OrderReducer.appointmentDetails,
});

const mapDispatchToProps = {
  getFormAppointment,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppointmentDetails);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
  card: {
    paddingVertical: Size.hp2,
    marginVertical: Size.hp2,
  },
});
