import React from 'react';
import {StyleSheet, View} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {formatMoney} from 'accounting-js';
import moment from 'moment';

// component
import NotoSans from '../../../shared/components/NotoSans';
import {TextCard} from '../../../shared/components/TextCard';

// global
import {Superscript} from '../../../shared/components/Superscript';
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

export const Content = ({
  buildingType,
  duration,
  area,
  serviceType,
  budget,
  address,
  inspectionArea,
  date,
  startTime,
  endTime,
  isReview = false,
}) => {
  const dateAppointment = moment(date).format('DD MMMM YYYY');

  const RenderContent = ({title, content}) => {
    return (
      <View style={styles.content}>
        <NotoSans title={title} size={Size.ms14} color={Color.ashGray} />
        <NotoSans title={content} type="Bold" size={Size.ms14} />
      </View>
    );
  };

  return (
    <View
      style={[
        isReview && styles.contentContainer,
        {paddingTop: isReview ? Size.hp3 : Size.hp2},
      ]}>
      <View style={[Layouting().flexRow, Layouting().spaceBetween]}>
        <View>
          <RenderContent title="Building Type" content={buildingType} />
          <RenderContent
            title="Estimated Work Duration"
            content={
              duration > 1 ? <>{duration} Weeks</> : <>{duration} Week</>
            }
          />
        </View>
        <View>
          {isReview ? (
            <RenderContent
              title="Area Size"
              content={
                <Superscript
                  base={`${area} m`}
                  exponent="2"
                  sizeBase={Size.ms14}
                  sizeExp={Size.ms10}
                  type="Bold"
                />
              }
            />
          ) : (
            <RenderContent title="Service Type" content={serviceType} />
          )}
          <RenderContent
            title="Budget"
            content={`Rp ${formatMoney(budget, {
              symbol: '',
              thousand: ',',
              precision: 0,
            })}`}
          />
        </View>
      </View>

      <RenderContent title="Address" content={address} />
      <RenderContent title="Inspection Area" content={inspectionArea} />
      {isReview && (
        <RenderContent
          title="Date and Time"
          content={`${dateAppointment} | ${startTime} - ${endTime}`}
        />
      )}
    </View>
  );
};

export const Note = ({note}) => {
  return (
    <TextCard
      backgroundColor={Color.wheat}
      hasShadow={false}
      containerStyle={[styles.card]}>
      <NotoSans title="Note" type="Bold" size={Size.ms14} />
      <NotoSans title={note} size={Size.ms14} style={styles.note} />
    </TextCard>
  );
};

const styles = StyleSheet.create({
  content: {
    marginVertical: heightPercentageToDP(1.5),
  },
  contentContainer: {
    paddingHorizontal: Size.wp4,
    paddingBottom: Size.hp2,
  },
  card: {
    paddingTop: Size.hp2,
    paddingBottom: Size.hp4,
    marginVertical: Size.hp2,
  },
  note: {
    paddingHorizontal: Size.wp2,
    paddingVertical: heightPercentageToDP(0.5),
  },
});
