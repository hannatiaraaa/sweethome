import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import AntDesign from 'react-native-vector-icons/AntDesign';

export const PlusButton = ({onPress}) => {
  return (
    <TouchableOpacity
      activeOpacity={0.8}
      onPress={onPress}
      style={[Layouting().centered, Layouting().shadow, styles.container]}>
      <AntDesign name="plus" size={Size.ms20} color="white" />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: Size.ms40,
    height: Size.ms40,
    borderRadius: Size.ms80,
    backgroundColor: Color.primaryBlue,
    position: 'absolute',
    bottom: Size.hp2,
    right: widthPercentageToDP(2.75),
  },
});
