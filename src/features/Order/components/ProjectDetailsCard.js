import React, {useEffect, useState} from 'react';
import {Linking, StyleSheet, View} from 'react-native';
import moment from 'moment';

// component
import {OrderTopElement} from './OrderTopElement';
import {TextCard} from '../../../shared/components/TextCard';
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

export const ProjectDetailsCard = ({appointment, project, onPress}) => {
  const [statusColor, setStatusColor] = useState('');
  useEffect(() => {
    if (project.status === 'Waiting Payment') {
      setStatusColor(Color.warningYellow);
    }
    if (project.status === 'On Going') {
      setStatusColor(Color.infoBlue);
    }
    if (project.status === 'Cancellation Requested') {
      setStatusColor(Color.ashGray);
    }
    if (project.status === 'Cancelled') {
      setStatusColor(Color.errorPink);
    }
    if (project.status === 'Done') {
      setStatusColor(Color.successGreen);
    }
  }, [project]);

  const date = appointment && moment(appointment.date).format('DD MMMM YYYY');
  const RenderTextContainer = ({
    title,
    content,
    onPressReceipt,
    containerStyle,
    numberOfLines,
    dataDetectorType,
    ellipsizeMode,
    style,
  }) => {
    return (
      <View style={{...containerStyle}}>
        <NotoSans title={title} color={Color.ashGray} />
        <NotoSans
          title={content}
          size={Size.ms14}
          type="Bold"
          color={Color.primaryBlue}
          style={[onPressReceipt ? styles.underline : null, {...style}]}
          onPress={onPressReceipt}
          numberOfLines={numberOfLines}
          dataDetectorType={dataDetectorType}
          ellipsizeMode={ellipsizeMode}
        />
      </View>
    );
  };

  const RenderContent = () => {
    return (
      <View
        style={[
          Layouting(undefined, 2).flex,
          Layouting('flex-start').spaceEvenly,
          styles.content,
        ]}>
        <RenderTextContainer
          title="Related Appointment"
          content={appointment.ticket}
          style={styles.underline}
        />
        <NotoSans
          title={`${date} | ${appointment.timeslot?.start} - ${appointment.timeslot?.end}`}
          color={Color.primaryBlue}
        />
      </View>
    );
  };

  const RenderReason = () => {
    return (
      <View
        style={[
          Layouting(undefined, 5).flex,
          Layouting('flex-start').spaceEvenly,
          styles.content,
        ]}>
        <NotoSans title="Reason" color={Color.ashGray} />
        <NotoSans
          title={project.cancelPayment.reason}
          color={Color.gunGray}
          size={Size.ms14}
        />
      </View>
    );
  };

  return (
    <TextCard
      backgroundColor={Color.mint}
      onPress={onPress}
      containerStyle={[Layouting().flex, Layouting().spaceAround, styles.card]}>
      <OrderTopElement
        status={project.status}
        hasArrow={false}
        backgroundColorStatus={statusColor}
        style={styles.content}
      />
      {project.status === 'On Going' || project.status === 'Done' ? (
        <View
          style={[
            styles.content,
            project.status === 'On Going'
              ? [Layouting().flexRow, Layouting().spaceBetween]
              : Layouting('flex-start').centered,
          ]}>
          {project.status === 'On Going' && (
            <RenderTextContainer
              title="Remaining Time"
              content={
                project.totalDuration > 1 ? (
                  <>{project.totalDuration} Weeks</>
                ) : (
                  <>{project.totalDuration} Week</>
                )
              }
              containerStyle={Layouting(undefined, 3).flex}
            />
          )}
          <RenderTextContainer
            title="Receipt"
            content={
              project.status === 'On Going'
                ? project.payment?.receipt.slice(70)
                : project.payment?.receipt.slice(50)
            }
            containerStyle={Layouting(undefined, 2.5).flex}
            onPressReceipt={() => Linking.openURL(project.payment.receipt)}
            numberOfLines={1}
            dataDetectorType="link"
            ellipsizeMode="head"
            style={{flexWrap: 'nowrap'}}
          />
        </View>
      ) : null}
      {project.status === 'Cancellation Requested' ||
      project.status === 'Cancelled' ? (
        <RenderReason />
      ) : null}
      <RenderContent />
    </TextCard>
  );
};

const styles = StyleSheet.create({
  underline: {
    textDecorationLine: 'underline',
  },
  card: {
    paddingVertical: Size.hp1,
    marginVertical: Size.hp3,
    width: Size.wp92,
  },
  content: {
    width: Size.wp84,
    paddingVertical: Size.hp1,
  },
});
