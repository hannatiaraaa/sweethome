import React from 'react';
import {StyleSheet, View} from 'react-native';

// component
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Color, Size} from '../../../shared/global/config';

export const HeadingForm = ({title, children}) => {
  return (
    <View style={styles.container}>
      <NotoSans
        title={
          <>
            <NotoSans title={title} color={Color.gunGray} size={Size.ms14} />
            {title === 'Note' ? null : (
              <NotoSans title="*" color="red" size={Size.ms14} />
            )}
          </>
        }
        type="Bold"
        style={styles.title}
      />
      {children}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginVertical: Size.hp1,
  },
  title: {
    marginVertical: Size.ms8,
  },
});
