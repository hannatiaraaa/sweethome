import React from 'react';
import {StyleSheet, View} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import {formatMoney} from 'accounting-js';

// component
import NotoSans from '../../../shared/components/NotoSans';
import {TextCard} from '../../../shared/components/TextCard';
import {Superscript} from '../../../shared/components/Superscript';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

const RenderHeading = ({title}) => {
  return (
    <NotoSans
      title={title}
      size={Size.ms14}
      type="Bold"
      style={styles.heading}
    />
  );
};

export const Content = ({projectId, buildingType, address}) => {
  const RenderContent = ({title, content, style}) => {
    return (
      <View style={[styles.content, {...style}]}>
        <NotoSans title={title} size={Size.ms14} color={Color.ashGray} />
        <NotoSans title={content} type="Bold" size={Size.ms14} />
      </View>
    );
  };

  return (
    <TextCard
      backgroundColor={Color.wheat}
      hasShadow={false}
      containerStyle={[styles.card]}>
      <RenderHeading title="Project Details" />
      <View style={[Layouting().flexRow, Layouting().spaceBetween]}>
        <RenderContent
          title="Project ID"
          content={projectId}
          style={Layouting(undefined, 4).flex}
        />
        <RenderContent
          title="Building Type"
          content={buildingType}
          style={Layouting(undefined, 2.5).flex}
        />
      </View>
      <RenderContent title="Address" content={address} />
    </TextCard>
  );
};

const RenderPackageContent = ({title, content}) => {
  return (
    <View
      style={[
        Layouting().flexRow,
        Layouting().spaceBetween,
        styles.packageContent,
      ]}>
      <NotoSans title={title} size={Size.ms14} color={Color.ashGray} />
      <NotoSans
        title={content}
        type="Bold"
        size={Size.ms14}
        color={Color.gunGray}
        style={Layouting('flex-end').align}
      />
    </View>
  );
};

export const Package = ({heading, projectType, durations, area, price}) => {
  return (
    <TextCard
      backgroundColor={Color.wheat}
      hasShadow={false}
      containerStyle={[styles.card, styles.width]}>
      <RenderHeading title={heading} />
      <View>
        <RenderPackageContent title="Project Type" content={projectType} />
        <RenderPackageContent
          title="Work Duration"
          content={`${durations} Week(s)`}
        />
        <RenderPackageContent
          title="Area"
          content={
            <Superscript
              base={`${area} m`}
              exponent="2"
              sizeBase={Size.ms14}
              sizeExp={Size.ms10}
              color={Color.gunGray}
              type="Bold"
            />
          }
        />
        <RenderPackageContent
          title="Price"
          content={`Rp ${formatMoney(price, {
            symbol: '',
            thousand: ',',
            precision: 0,
          })}`}
        />
      </View>
    </TextCard>
  );
};

export const PackageTotal = ({totalDuration, totalPrice}) => {
  return (
    <TextCard
      backgroundColor={Color.cream}
      hasShadow={false}
      containerStyle={[styles.card, styles.width]}>
      <RenderPackageContent
        title="Total Duration"
        content={`${totalDuration} Week(s)`}
      />
      <RenderPackageContent
        title="Total Price"
        content={`Rp ${formatMoney(totalPrice, {
          symbol: '',
          thousand: ',',
          precision: 0,
        })}`}
      />
    </TextCard>
  );
};

const styles = StyleSheet.create({
  heading: {
    paddingBottom: Size.hp2,
  },
  card: {
    paddingVertical: Size.hp2,
    marginVertical: heightPercentageToDP(1.5),
  },
  content: {
    marginVertical: heightPercentageToDP(1.5),
  },
  width: {
    width: Size.wp92,
  },
  packageContent: {
    marginVertical: heightPercentageToDP(0.5),
  },
});
