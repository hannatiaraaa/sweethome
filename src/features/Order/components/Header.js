import React from 'react';
import {StyleSheet, TouchableHighlight, View} from 'react-native';
import {Header} from 'react-native-elements';

// component
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default function HeaderOrder({onPress, title}) {
  return (
    <Header
      statusBarProps={{
        backgroundColor: 'transparent',
        barStyle: 'light-content',
        translucent: true,
      }}
      containerStyle={[
        Layouting().spaceAround,
        {
          backgroundColor: Color.primaryBlue,
          height: Size.ms84,
          paddingHorizontal: Size.wp2,
        },
      ]}
      leftComponent={
        <TouchableHighlight
          activeOpacity={0.8}
          onPress={onPress}
          underlayColor="#2c5a73"
          style={[Layouting().centered, styles.circle]}>
          <FontAwesome5 name="arrow-left" color="white" size={Size.ms16} />
        </TouchableHighlight>
      }
      centerComponent={
        <View style={[Layouting().flex, Layouting().centered]}>
          <NotoSans title={title} size={Size.ms14} color="white" type="Bold" />
        </View>
      }
    />
  );
}

const styles = StyleSheet.create({
  circle: {
    width: Size.ms40,
    height: Size.ms40,
    borderRadius: Size.ms80,
  },
});
