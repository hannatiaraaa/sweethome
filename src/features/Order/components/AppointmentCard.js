import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import moment from 'moment';

// component
import NotoSans from '../../../shared/components/NotoSans';
import {TextCard} from '../../../shared/components/TextCard';
import {OrderTopElement} from './OrderTopElement';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

export const AppointmentCard = ({appointment, isDetails = false, onPress}) => {
  const [statusColor, setStatusColor] = useState('');
  useEffect(() => {
    if (appointment.status === 'Waiting Approval') {
      setStatusColor(Color.warningYellow);
    }
    if (appointment.status === 'Scheduled') {
      setStatusColor(Color.infoBlue);
    }
    if (appointment.status === 'Declined') {
      setStatusColor(Color.errorPink);
    }
    if (appointment.status === 'Closed') {
      setStatusColor(Color.ashGray);
    }
    if (appointment.status === 'Done') {
      setStatusColor(Color.successGreen);
    }
  }, [appointment]);

  const date = appointment && moment(appointment.date).format('DD MMMM YYYY');

  const RenderContent = () => {
    return (
      <View
        style={[
          Layouting(undefined, 2).flex,
          Layouting('flex-start').spaceEvenly,
          styles.content,
        ]}>
        {isDetails && (
          <NotoSans
            title={
              appointment.status === 'Declined'
                ? 'Requested Schedule'
                : 'Schedule'
            }
            color={Color.ashGray}
          />
        )}
        <NotoSans
          title={
            <>
              <NotoSans
                title={<>{date} |</>}
                size={Size.ms14}
                type="Bold"
                color={Color.primaryBlue}
              />{' '}
              {appointment.timeslot.start} - {appointment.timeslot.end}
            </>
          }
          color={Color.primaryBlue}
        />
        {isDetails ? null : (
          <NotoSans
            title={
              <>
                {appointment.buildType.name} - {appointment.serviceType.name}
              </>
            }
            color={Color.primaryBlue}
          />
        )}
      </View>
    );
  };

  return (
    <TextCard
      backgroundColor={Color.cream}
      onPress={onPress}
      containerStyle={[
        Layouting().flex,
        Layouting().spaceAround,
        styles.card,
        {marginBottom: isDetails ? Size.hp3 : Size.hp1},
        {height: isDetails ? null : Size.ms100},
      ]}>
      <OrderTopElement
        status={appointment.status}
        hasArrow={isDetails ? false : true}
        backgroundColorStatus={statusColor}
        style={styles.content}
      />
      <RenderContent />
    </TextCard>
  );
};

const styles = StyleSheet.create({
  card: {
    paddingVertical: Size.hp1,
    width: Size.wp92,
    marginTop: Size.hp3,
  },
  content: {
    width: Size.wp84,
    paddingVertical: Size.hp1,
  },
});
