import React from 'react';
import {StyleSheet, View} from 'react-native';
import {
  heightPercentageToDP,
  widthPercentageToDP,
} from 'react-native-responsive-screen';

// component
import {TextCard} from '../../../shared/components/TextCard';
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

export const InfoCard = ({content}) => {
  return (
    <TextCard
      hasShadow={false}
      backgroundColor={Color.cloud}
      containerStyle={[
        Layouting().flexRow,
        Layouting('flex-start').spaceEvenly,
        styles.card,
      ]}
      style={[Layouting().flex, styles.contentCard]}>
      <View style={[Layouting().flex, Layouting().centered]}>
        <View style={[Layouting().centered, styles.circleInfo]}>
          <MaterialCommunityIcons
            name="information-variant"
            color={Color.gunGray}
            size={Size.ms16}
          />
        </View>
      </View>
      <View style={Layouting(undefined, 9).flex}>
        <NotoSans title={content} color={Color.gunGray} />
      </View>
    </TextCard>
  );
};

const styles = StyleSheet.create({
  card: {
    paddingVertical: heightPercentageToDP(1.5),
    paddingHorizontal: Size.wp2,
    marginVertical: Size.hp1,
  },
  circleInfo: {
    width: Size.ms18,
    height: Size.ms18,
    borderWidth: Size.ms1,
    borderRadius: Size.ms40,
  },
});
