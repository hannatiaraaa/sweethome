import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {moderateScale} from 'react-native-size-matters';
import {heightPercentageToDP} from 'react-native-responsive-screen';

// component
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Layouting} from '../../../shared/global/Layouting';
import {Color, Size} from '../../../shared/global/config';

// icon
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export const OrderTopElement = ({
  status,
  backgroundColorStatus,
  style,
  hasArrow = false,
}) => {
  return (
    <View
      style={[
        Layouting().flex,
        hasArrow
          ? [Layouting().flexRow, Layouting().spaceBetween]
          : Layouting('flex-start').centered,
        {...style},
      ]}>
      <TouchableOpacity
        activeOpacity={1}
        style={[
          Layouting().centered,
          styles.statusContainer,
          {backgroundColor: backgroundColorStatus},
        ]}>
        <NotoSans title={status} color="white" type="Bold" />
      </TouchableOpacity>
      {hasArrow && (
        <FontAwesome5
          name="arrow-right"
          color={Color.primaryBlue}
          style={styles.arrow}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  statusContainer: {
    borderRadius: Size.ms100,
    paddingHorizontal: Size.wp4,
    paddingVertical: heightPercentageToDP(0.25),
  },
  arrow: {
    opacity: moderateScale(1.8),
  },
});
