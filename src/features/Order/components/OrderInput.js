import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import {Input} from 'react-native-elements';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import {moderateScale} from 'react-native-size-matters';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

export const OrderInput = ({
  autoCapitalize,
  keyboardType,
  placeholder,
  value,
  onChangeText,
  multiline,
  rightIcon,
  leftIcon,
  onBlur = () => {},
  onFocus = () => {},
}) => {
  const [focused, setFocused] = useState(true);

  useEffect(() => {
    handleFocus();
    handleBlur();
  }, []);

  const handleFocus = () => {
    setFocused(true);
  };

  const handleBlur = () => {
    setFocused(false);
  };

  return (
    <Input
      blurOnSubmit={false}
      onFocus={() => {
        handleFocus();
        onFocus();
      }}
      onBlur={() => {
        handleBlur();
        onBlur();
      }}
      autoCapitalize={autoCapitalize}
      keyboardType={keyboardType}
      placeholder={placeholder}
      placeholderTextColor={Color.ashGray}
      value={value}
      onChangeText={onChangeText}
      multiline={multiline}
      errorStyle={styles.textError}
      errorMessage={
        focused && !value && placeholder !== 'Tell your request here'
          ? 'This column should be field'
          : null
      }
      inputContainerStyle={[styles.box, multiline ? styles.bigInput : null]}
      rightIcon={rightIcon}
      rightIconContainerStyle={
        rightIcon && [Layouting().centered, styles.rightContainer]
      }
      leftIcon={leftIcon}
      leftIconContainerStyle={
        leftIcon && [Layouting().centered, styles.leftContainer]
      }
      inputStyle={[styles.text, multiline ? styles.textBigInput : null]}
    />
  );
};

const styles = StyleSheet.create({
  textError: {
    color: Color.errorPink,
    marginHorizontal: -moderateScale(5),
    marginBottom: -moderateScale(5),
  },
  box: {
    borderWidth: Size.ms1,
    borderRadius: Size.ms4,
    paddingHorizontal: widthPercentageToDP(3),
    width: Size.wp92,
    marginHorizontal: -Size.ms10,
  },
  boxInput: {
    borderColor: Color.primaryBlue,
  },
  boxNonInput: {
    borderColor: Color.ashGray,
  },
  bigInput: {
    height: Size.hp20,
  },
  text: {
    color: Color.gunGray,
    fontSize: Size.ms14,
    fontFamily: 'NotoSans-Regular',
  },
  textBigInput: {
    alignSelf: 'flex-start',
  },
  rightContainer: {
    borderLeftColor: Color.ashGray,
    borderLeftWidth: 1,
    paddingHorizontal: Size.wp4,
  },
  leftContainer: {
    borderRightColor: Color.ashGray,
    borderRightWidth: 1,
    paddingRight: widthPercentageToDP(3),
    marginRight: Size.wp2,
  },
});
