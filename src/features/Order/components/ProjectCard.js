import React, {useEffect, useState} from 'react';
import {StyleSheet, View} from 'react-native';
import {moderateScale} from 'react-native-size-matters';

// component
import NotoSans from '../../../shared/components/NotoSans';
import {TextCard} from '../../../shared/components/TextCard';
import {OrderTopElement} from './OrderTopElement';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// icon
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import {widthPercentageToDP} from 'react-native-responsive-screen';

export const ProjectCard = ({project, packages, onPress}) => {
  const [statusColor, setStatusColor] = useState('');
  useEffect(() => {
    if (project.status === 'Waiting Payment') {
      setStatusColor(Color.warningYellow);
    }
    if (project.status === 'On Going') {
      setStatusColor(Color.infoBlue);
    }
    if (project.status === 'Cancellation Requested') {
      setStatusColor(Color.ashGray);
    }
    if (project.status === 'Cancelled') {
      setStatusColor(Color.errorPink);
    }
    if (project.status === 'Done') {
      setStatusColor(Color.successGreen);
    }
  }, [project]);

  const RenderContent = () => {
    return (
      <View
        style={[
          Layouting(undefined, 2.5).flex,
          Layouting('flex-start').spaceEvenly,
        ]}>
        <NotoSans
          title={project.ticket}
          color={Color.primaryBlue}
          style={styles.underline}
        />
        <View
          style={[
            styles.content,
            !project.totalDuration
              ? Layouting('flex-start').centered
              : [Layouting().flexRow, Layouting().spaceBetween],
          ]}>
          <View style={Layouting('flex-start').spaceEvenly}>
            <NotoSans
              title={
                packages[0] ? (
                  <>
                    {packages[0].location.name} - {packages[0].projectType.name}
                  </>
                ) : (
                  <>No package(s)</>
                )
              }
              type="Bold"
              size={Size.ms14}
              color={Color.primaryBlue}
              style={
                !project.totalDuration || project.status === 'Cancelled'
                  ? styles.content
                  : styles.contentWrap
              }
            />
          </View>
          {!project.totalDuration || project.status === 'Cancelled' ? null : (
            <View
              style={[
                Layouting().flexRow,
                Layouting('flex-end').spaceEvenly,
                {width: Size.ms80},
              ]}>
              <MaterialCommunityIcons
                name="timer-sand"
                size={Size.ms20}
                color={Color.primaryBlue}
              />
              <NotoSans
                title={
                  project.totalDuration > 1 ? (
                    <>{project.totalDuration} Weeks</>
                  ) : (
                    <>{project.totalDuration} Week</>
                  )
                }
                color={Color.primaryBlue}
              />
            </View>
          )}
        </View>
        {packages.length > 1 ? (
          <NotoSans
            title={
              packages.length > 2 ? (
                <>+ {packages.length - 1} more packagess</>
              ) : (
                <>+ {packages.length - 1} more packages</>
              )
            }
            color={Color.ashGray}
            size={Size.ms10}
            style={styles.content}
          />
        ) : null}
      </View>
    );
  };

  return (
    <TextCard
      backgroundColor={Color.mint}
      onPress={onPress}
      containerStyle={[Layouting().flex, Layouting().spaceAround, styles.card]}
      style={{
        height: packages.length === 1 ? moderateScale(120) : moderateScale(140),
      }}>
      <OrderTopElement
        status={project.status}
        hasArrow={true}
        backgroundColorStatus={statusColor}
        style={styles.content}
      />
      <RenderContent />
    </TextCard>
  );
};

const styles = StyleSheet.create({
  card: {
    paddingVertical: Size.hp1,
    marginTop: Size.hp3,
    marginBottom: Size.hp1,
    width: Size.wp92,
  },
  content: {
    width: Size.wp84,
  },
  contentWrap: {
    width: widthPercentageToDP(60),
  },
  underline: {
    textDecorationLine: 'underline',
  },
});
