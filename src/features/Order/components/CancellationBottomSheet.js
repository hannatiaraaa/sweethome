import React from 'react';
import {StyleSheet, View} from 'react-native';
import {BottomSheet} from 'react-native-elements';

// component
import {InfoCard} from './InfoCard';
import NotoSans from '../../../shared/components/NotoSans';
import {HeadingForm} from './HeadingForm';
import {OrderInput} from './OrderInput';
import {YellowButton} from '../../../shared/components/YellowButton';

// global
import {Color, Size} from '../../../shared/global/config';

export const CancellationBottomSheet = ({
  isVisible,
  value,
  onChangeText,
  onPress,
  onPressCancel,
  projectId,
}) => {
  return (
    <BottomSheet isVisible={isVisible}>
      <View style={styles.container}>
        <InfoCard
          content={
            <>
              You want to cancel project{' '}
              <NotoSans title={projectId} type="Bold" />. We will review your
              request.
            </>
          }
        />
        <HeadingForm title="Cancellation Reason" />
        <OrderInput
          placeholder="Tell your request here"
          value={value}
          onChangeText={onChangeText}
          multiline={true}
        />
        <NotoSans
          title="Cancel"
          color={Color.errorPink}
          style={styles.textCancel}
          onPress={onPressCancel}
        />
      </View>
      <YellowButton title="Request Cancellation" onPress={onPress} />
    </BottomSheet>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    paddingHorizontal: Size.wp4,
    paddingTop: Size.hp4,
    borderTopRightRadius: Size.ms16,
    borderTopLeftRadius: Size.ms16,
  },
  textCancel: {
    marginVertical: Size.hp2,
    textAlign: 'center',
  },
});
