import React from 'react';
import {View, StyleSheet} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';
import moment from 'moment';

// component
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Size} from '../../../shared/global/config';

export const DetailsFooter = ({
  status,
  color = '#C5C7CD',
  dateCreated,
  title,
  dateStatus,
}) => {
  const created = moment(dateCreated).format('DD MMMM YYYY');
  const updated = moment(dateStatus).format('DD MMMM YYYY');

  return (
    <View style={styles.container}>
      <NotoSans
        title={`Created at ${created}`}
        color={color}
        style={styles.text}
      />
      {status === 'Waiting Approval' ||
      status === 'Waiting Payment' ||
      status === 'On Going' ? null : (
        <NotoSans
          title={`${title} at ${updated}`}
          color={color}
          style={styles.text}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginBottom: Size.hp4,
  },
  text: {
    marginVertical: heightPercentageToDP(0.5),
  },
});
