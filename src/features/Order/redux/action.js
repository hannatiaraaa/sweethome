// saga
export const GET_BUILD_TYPE = 'GET_BUILD_TYPE';
export const GET_SERVICE_TYPE = 'GET_SERVICE_TYPE';
export const GET_TIMESLOT = 'GET_TIMESLOT';
export const GET_FORM_APPOINTMENT = 'GET_FORM_APPOINTMENT';
export const GET_APPOINTMENT = 'GET_APPOINTMENT';
export const GET_APPOINTMENT_DETAILS = 'GET_APPOINTMENT_DETAILS';
export const GET_PROJECT = 'GET_PROJECT';
export const GET_PROJECT_DETAILS = 'GET_PROJECT_DETAILS';
export const CREATE_NEW_APPOINTMENT = 'CREATE_NEW_APPOINTMENT';
export const CANCEL_PAYMENT = 'CANCEL_PAYMENT';
export const UPLOAD_RECEIPT = 'UPLOAD_RECEIPT';

// reducer
export const SET_BUILD_TYPE = 'SET_BUILD_TYPE';
export const SET_SERVICE_TYPE = 'SET_SERVICE_TYPE';
export const SET_TIMESLOT = 'SET_TIMESLOT';
export const SET_NEW_APPOINTMENT = 'SET_NEW_APPOINTMENT';
export const SET_APPOINTMENT = 'SET_APPOINTMENT';
export const SET_APPOINTMENT_DETAILS = 'SET_APPOINTMENT_DETAILS';
export const SET_PROJECT = 'SET_PROJECT';
export const SET_PROJECT_DETAILS = 'SET_PROJECT_DETAILS';

export const getBuildType = () => {
  return {
    type: GET_BUILD_TYPE,
  };
};

export const getServiceType = () => {
  return {
    type: GET_SERVICE_TYPE,
  };
};

export const getTimeslot = (serviceTypeId, nav) => {
  return {
    type: GET_TIMESLOT,
    serviceTypeId,
    nav,
  };
};

export const getFormAppointment = () => {
  return {
    type: GET_FORM_APPOINTMENT,
  };
};

export const getAppointment = () => {
  return {
    type: GET_APPOINTMENT,
  };
};

export const getAppointmentDetails = (id, from) => {
  return {
    type: GET_APPOINTMENT_DETAILS,
    id,
    from,
  };
};

export const getProject = () => {
  return {
    type: GET_PROJECT,
  };
};

export const getProjectDetails = (id) => {
  return {
    type: GET_PROJECT_DETAILS,
    id,
  };
};

export const setBuildType = (payload) => {
  return {
    type: SET_BUILD_TYPE,
    payload,
  };
};

export const setServiceType = (payload) => {
  return {
    type: SET_SERVICE_TYPE,
    payload,
  };
};

export const setTimeslot = (payload) => {
  return {
    type: SET_TIMESLOT,
    payload,
  };
};

export const setNewAppointment = (payload) => {
  return {
    type: SET_NEW_APPOINTMENT,
    payload,
  };
};

export const setAppointment = (payload) => {
  return {
    type: SET_APPOINTMENT,
    payload,
  };
};

export const setAppointmentDetails = (payload) => {
  return {
    type: SET_APPOINTMENT_DETAILS,
    payload,
  };
};

export const setProject = (payload) => {
  return {
    type: SET_PROJECT,
    payload,
  };
};

export const setProjectDetails = (payload) => {
  return {
    type: SET_PROJECT_DETAILS,
    payload,
  };
};

export const createNewAppointment = (newAppointment) => {
  return {
    type: CREATE_NEW_APPOINTMENT,
    newAppointment,
  };
};

export const cancelPayment = (id, reason) => {
  return {
    type: CANCEL_PAYMENT,
    id,
    reason,
  };
};

export const uploadReceipt = (formData, id) => {
  return {
    type: UPLOAD_RECEIPT,
    formData,
    id,
  };
};
