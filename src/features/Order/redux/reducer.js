import {
  SET_APPOINTMENT,
  SET_APPOINTMENT_DETAILS,
  SET_BUILD_TYPE,
  SET_NEW_APPOINTMENT,
  SET_PROJECT,
  SET_PROJECT_DETAILS,
  SET_SERVICE_TYPE,
  SET_TIMESLOT,
} from './action';

const initialState = {
  buildType: [],
  serviceType: [],
  timeslot: [],
  newAppointment: {},
  appointment: [],
  appointmentDetails: {},
  project: [],
  projectDetails: {},
};

export const OrderReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_BUILD_TYPE:
      return {
        ...state,
        buildType: action.payload,
      };

    case SET_SERVICE_TYPE:
      return {
        ...state,
        serviceType: action.payload,
      };

    case SET_TIMESLOT:
      return {
        ...state,
        timeslot: action.payload,
      };

    case SET_NEW_APPOINTMENT:
      return {
        ...state,
        newAppointment: action.payload,
      };

    case SET_APPOINTMENT:
      return {
        ...state,
        appointment: action.payload,
      };

    case SET_APPOINTMENT_DETAILS:
      return {
        ...state,
        appointmentDetails: action.payload,
      };

    case SET_PROJECT:
      return {
        ...state,
        project: action.payload,
      };

    case SET_PROJECT_DETAILS:
      return {
        ...state,
        projectDetails: action.payload,
      };

    default:
      return state;
  }
};
