import {ToastAndroid} from 'react-native';
import {all, call, put, takeEvery, takeLatest} from 'redux-saga/effects';

// router
import {navigate} from '../../../router/navigate';

// api
import {request} from '../../../shared/utils/api';
import {
  APPOINTMENT,
  BUILD_TYPE,
  CANCEL,
  PAYMENT,
  PROJECT,
  RECEIPT,
  SERVICE_TYPE,
  TIMESLOT,
} from '../../../shared/utils/endPoint';
import {setLoading} from '../../../store/GlobalAction';
import {getLocation, getStyle} from '../../Explore/redux/action';

// action
import {
  CANCEL_PAYMENT,
  CREATE_NEW_APPOINTMENT,
  getAppointment,
  getAppointmentDetails,
  getBuildType,
  getProject,
  getServiceType,
  GET_APPOINTMENT,
  GET_APPOINTMENT_DETAILS,
  GET_BUILD_TYPE,
  GET_FORM_APPOINTMENT,
  GET_PROJECT,
  GET_PROJECT_DETAILS,
  GET_SERVICE_TYPE,
  GET_TIMESLOT,
  setAppointment,
  setAppointmentDetails,
  setBuildType,
  setProject,
  setProjectDetails,
  setServiceType,
  setTimeslot,
  UPLOAD_RECEIPT,
} from './action';

function* getBuildTypeSaga() {
  const res = yield call(request, `${BUILD_TYPE}`, 'GET');

  if (res.status) {
    yield put(setBuildType(res.data.data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getServiceTypeSaga() {
  const res = yield call(request, `${SERVICE_TYPE}`, 'GET');

  if (res.status) {
    yield put(setServiceType(res.data.data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getFormAppointmentSaga() {
  yield put(getBuildType());
  yield put(getServiceType());
  yield put(getLocation());
  yield put(getStyle());
  yield navigate('OrderScreenRouter', {screen: 'NewAppointment'});
}

function* getTimeslotSaga({serviceTypeId, nav = true}) {
  const res = yield call(
    request,
    `${SERVICE_TYPE}/${serviceTypeId}${TIMESLOT}`,
    'GET',
  );

  if (res.status) {
    yield put(setTimeslot(res.data.data));
    if (nav) {
      yield navigate('OrderScreenRouter', {
        screen: 'NewAppointment',
        params: {screen: 'SelectTime'},
      });
    }
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getAppointmentSaga() {
  const res = yield call(request, `${APPOINTMENT}`, 'GET');

  if (res.status) {
    yield put(setAppointment(res.data.data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getAppointmentDetailsSaga({id, from}) {
  const res = yield call(request, `${APPOINTMENT}/${id}`, 'GET');

  if (res.status) {
    yield put(setAppointmentDetails(res.data.data));
    if (from === 'component') {
      yield navigate('OrderScreenRouter', {
        screen: 'AppointmentDetails',
        params: {ticket: res.data.data.ticket},
      });
    }
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getProjectSaga() {
  const res = yield call(request, `${PROJECT}`, 'GET');

  if (res.status) {
    yield put(setProject(res.data.data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getProjectDetailsSaga({id}) {
  const res = yield call(request, `${PROJECT}/${id}`, 'GET');

  if (res.status) {
    yield put(getAppointmentDetails(res.data.data.appointment, 'saga'));
    yield put(setProjectDetails(res.data.data));
    yield navigate('OrderScreenRouter', {
      screen: 'ProjectDetails',
      params: {ticket: res.data.data.ticket},
    });
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* createNewAppointmentSaga({newAppointment}) {
  try {
    yield put(setLoading(true));
    const res = yield call(request, `${APPOINTMENT}`, 'POST', {
      body: newAppointment,
    });

    if (res.status) {
      yield put(getAppointment());
      yield navigate('MainTab', {
        screen: 'OrderRouter',
        params: {screen: 'Order', params: {screen: 'Appointment'}},
      });
    } else {
      ToastAndroid.showWithGravity(
        'Check your requested appointment or Create a new request again',
        ToastAndroid.LONG,
        ToastAndroid.TOP,
      );
      yield navigate('MainTab', {
        screen: 'OrderRouter',
        params: {screen: 'Order', params: {screen: 'Appointment'}},
      });
    }
  } finally {
    yield put(setLoading(false));
  }
}

function* cancelPaymentSaga({id, reason}) {
  try {
    yield put(setLoading(true));
    const res = yield call(request, `${PROJECT}/${id}${CANCEL}`, 'POST', {
      body: {
        reason,
      },
    });

    if (res.status) {
      yield put(getProject());
      yield navigate('MainTab', {
        screen: 'OrderRouter',
        params: {screen: 'Order', params: {screen: 'Project'}},
      });
    } else {
      yield put(getProject());
      ToastAndroid.showWithGravity(
        'Check your requested payment cancellation on your project list or try again',
        ToastAndroid.LONG,
        ToastAndroid.TOP,
      );
      yield navigate('MainTab', {
        screen: 'OrderRouter',
        params: {screen: 'Order', params: {screen: 'Project'}},
      });
    }
  } finally {
    yield put(setLoading(false));
  }
}

function* uploadReceiptSaga({formData, id}) {
  try {
    yield put(setLoading(true));
    const {uri, fileName, type} = formData;
    const body = new FormData();
    body.append('receipt', {uri, name: fileName, type});
    const res = yield call(
      request,
      `${PROJECT}/${id}${PAYMENT}${RECEIPT}`,
      'POST',
      {
        body: body,
      },
    );

    if (res.status) {
      yield put(getProject());
      ToastAndroid.showWithGravity(
        'Your upload payment receipt has been reviewed. Please wait and check your project list.',
        ToastAndroid.LONG,
        ToastAndroid.TOP,
      );
      yield navigate('MainTab', {
        screen: 'OrderRouter',
        params: {screen: 'Order', params: {screen: 'Project'}},
      });
    } else {
      ToastAndroid.showWithGravity(
        'Try to upload your payment receipt again',
        ToastAndroid.LONG,
        ToastAndroid.CENTER,
      );
    }
  } finally {
    yield put(setLoading(false));
  }
}

export function* OrderSaga() {
  yield all([
    takeLatest(GET_BUILD_TYPE, getBuildTypeSaga),
    takeLatest(GET_SERVICE_TYPE, getServiceTypeSaga),
    takeEvery(GET_FORM_APPOINTMENT, getFormAppointmentSaga),
    takeLatest(GET_TIMESLOT, getTimeslotSaga),
    takeLatest(GET_APPOINTMENT, getAppointmentSaga),
    takeLatest(GET_APPOINTMENT_DETAILS, getAppointmentDetailsSaga),
    takeLatest(GET_PROJECT, getProjectSaga),
    takeLatest(GET_PROJECT_DETAILS, getProjectDetailsSaga),
    takeLatest(CREATE_NEW_APPOINTMENT, createNewAppointmentSaga),
    takeLatest(CANCEL_PAYMENT, cancelPaymentSaga),
    takeLatest(UPLOAD_RECEIPT, uploadReceiptSaga),
  ]);
}
