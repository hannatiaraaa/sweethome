import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {CalendarList, LocaleConfig} from 'react-native-calendars';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import moment from 'moment';

// component
import HeaderOrder from '../components/Header';
import {YellowButton} from '../../../shared/components/YellowButton';
import NotoSans from '../../../shared/components/NotoSans';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// import
import {connect} from 'react-redux';
import {setNewAppointment, getTimeslot} from '../redux/action';

LocaleConfig.locales['en'] = {
  monthNames: [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ],
  monthNamesShort: [
    'Jan',
    'Feb',
    'Mar',
    'April',
    'May',
    'June',
    'Jul.',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
  ],
  dayNames: [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
  ],
  dayNamesShort: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],
  today: 'Today',
};
LocaleConfig.defaultLocale = 'en';

const now = moment(new Date()).format('YYYY-MM-DD');

function SelectDate(props) {
  const {newAppointment, timeslot} = props;
  const [day, setDay] = useState(now);

  const goBack = () => {
    props.navigation.goBack();
  };

  const goSelectTime = () => {
    const data = {
      ...newAppointment,
      date: day,
    };
    props.setNewAppointment(data);
    props.getTimeslot(newAppointment.serviceType);
  };

  const RenderHeader = (date) => {
    const month = moment(date.toString()).format('MMMM YYYY');
    return (
      <View style={[Layouting().flex, styles.monthContainer]}>
        <Text style={styles.monthText}>{month}</Text>
      </View>
    );
  };

  const RenderTop = () => {
    const RenderTopContent = ({backgroundColor, title}) => {
      return (
        <View
          style={[
            Layouting().flexRow,
            Layouting('flex-start').spaceEvenly,
            styles.topContentContainer,
          ]}>
          <View style={[styles.boxTop, {backgroundColor: backgroundColor}]} />
          <NotoSans title={title} size={Size.ms14} />
        </View>
      );
    };

    return (
      <View style={[Layouting().flexRow, styles.topContainer]}>
        <RenderTopContent
          backgroundColor={Color.primaryBlue}
          title="Available"
        />
        <RenderTopContent backgroundColor={Color.vogue} title="Full Booked" />
      </View>
    );
  };

  return (
    <View style={Layouting().flex}>
      <HeaderOrder title="Select Date" onPress={goBack} />
      <RenderTop />
      <View style={Layouting().flex}>
        <CalendarList
          minDate={now}
          current={now}
          renderHeader={RenderHeader}
          onDayPress={(date) => setDay(date.dateString)}
          markedDates={
            timeslot.length
              ? {
                  [day]: {
                    selected: true,
                    marked: true,
                    selectedColor: Color.warningYellow,
                    customStyles: {fontFamily: 'Inter-Bold'},
                  },
                }
              : null
          }
          disabledByDefault={timeslot.length ? false : true}
          pastScrollRange={0}
          futureScrollRange={50}
          scrollEnabled={true}
          theme={{
            textSectionTitleColor: Color.primaryBlue,
            dayTextColor: Color.primaryBlue,
            todayTextColor: Color.warningYellow,
            textDisabledColor: Color.vogue,
            selectedDayTextColor: Color.gunGray,
            textDayFontFamily: 'Inter-Regular',
            textDayHeaderFontFamily: 'Inter-SemiBold',
            textDayFontSize: Size.ms16,
            textDayHeaderFontSize: Size.ms16,
          }}
        />
      </View>
      <YellowButton
        title="Next"
        onPress={goSelectTime}
        disabled={timeslot.length ? false : true}
      />
    </View>
  );
}

const mapStateToProps = (state) => ({
  newAppointment: state.OrderReducer.newAppointment,
  timeslot: state.OrderReducer.timeslot,
});

const mapDispatchToProps = {
  setNewAppointment,
  getTimeslot,
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectDate);

const styles = StyleSheet.create({
  topContainer: {
    paddingHorizontal: Size.wp4,
  },
  topContentContainer: {
    marginVertical: Size.hp2,
    width: widthPercentageToDP(28),
    marginRight: Size.wp4,
  },
  boxTop: {
    width: Size.ms16,
    height: Size.ms16,
  },
  monthText: {
    fontFamily: 'Inter-SemiBold',
    fontSize: Size.ms16,
    color: Color.primaryBlue,
  },
  monthContainer: {
    paddingHorizontal: Size.wp2,
    paddingVertical: Size.hp1,
  },
});
