import React, {useState} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import {CheckBox} from 'react-native-elements';
import {widthPercentageToDP} from 'react-native-responsive-screen';
import moment from 'moment';

// component
import HeaderOrder from '../components/Header';
import NotoSans from '../../../shared/components/NotoSans';
import {YellowButton} from '../../../shared/components/YellowButton';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {setNewAppointment} from '../redux/action';

function SelectTime(props) {
  const {timeslot, newAppointment} = props;
  const [checkItem, setCheckItem] = useState('');

  const date = moment(newAppointment.date).format('DD MMMM YYYY');

  const doCheck = (item) => {
    setCheckItem(item);
    const data = {
      ...newAppointment,
      timeslot: item,
    };
    props.setNewAppointment(data);
  };

  const goBack = () => {
    props.navigation.goBack();
  };

  const goReview = () => {
    props.navigation.navigate('AppointmentReview');
  };

  const RenderCheckBox = ({title, checked, onPress, disabled}) => {
    return (
      <CheckBox
        title={title}
        checkedColor={Color.primaryBlue}
        uncheckedColor={Color.gunGray}
        checkedIcon="dot-circle-o"
        uncheckedIcon="circle-thin"
        size={Size.ms24}
        checked={checked}
        onPress={onPress}
        containerStyle={styles.checkBoxContainer}
        fontFamily="NotoSans-Regular"
        textStyle={[Layouting().centered, styles.text]}
        disabled={disabled}
      />
    );
  };

  return (
    <View style={Layouting().flex}>
      <HeaderOrder title={date} onPress={goBack} />
      <View style={[Layouting().flex, styles.container]}>
        <NotoSans
          title="Select appointment time"
          type="Bold"
          size={Size.ms14}
          style={styles.title}
        />
        <FlatList
          keyExtractor={(item) => item._id.toString()}
          showsVerticalScrollIndicator={false}
          data={timeslot}
          renderItem={({item}) => {
            return (
              <RenderCheckBox
                title={`${item.start} - ${item.end}`}
                checked={checkItem === item._id ? true : false}
                onPress={() => doCheck(item._id)}
              />
            );
          }}
        />
      </View>
      <YellowButton
        title="Next"
        onPress={goReview}
        disabled={checkItem === '' ? true : false}
      />
    </View>
  );
}

const mapStateToProps = (state) => ({
  timeslot: state.OrderReducer.timeslot,
  newAppointment: state.OrderReducer.newAppointment,
});

const mapDispatchToProps = {
  setNewAppointment,
};

export default connect(mapStateToProps, mapDispatchToProps)(SelectTime);

const styles = StyleSheet.create({
  container: {
    paddingVertical: Size.hp1,
  },
  title: {
    paddingHorizontal: widthPercentageToDP(5),
    paddingVertical: Size.hp2,
  },
  checkBoxContainer: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    paddingVertical: -Size.hp1,
  },
  text: {
    fontSize: Size.ms16,
    color: Color.gunGray,
    fontWeight: '400',
    fontFamily: 'NotoSans-Regular',
  },
});
