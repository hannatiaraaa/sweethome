import React, {useState} from 'react';
import {View, StyleSheet, ScrollView, FlatList} from 'react-native';
import {formatMoney} from 'accounting-js';

// component
import HeaderOrder from '../components/Header';
import NotoSans from '../../../shared/components/NotoSans';
import {InfoCard} from '../components/InfoCard';
import {HeadingForm} from '../components/HeadingForm';
import {FilterCardContainer} from '../../../shared/components/FilterCardContainer';
import {FilterCard} from '../../../shared/components/FilterCard';
import {Dropdown} from '../../../shared/components/DropDown';
import {OrderInput} from '../components/OrderInput';
import {Superscript} from '../../../shared/components/Superscript';
import {YellowButton} from '../../../shared/components/YellowButton';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {setNewAppointment, getTimeslot} from '../redux/action';

function AppointmentForm(props) {
  const {buildType, serviceType, location, style} = props;
  const [buildingType, setBuildingType] = useState('');
  const [servicingType, setServicingType] = useState('');
  const [duration, setDuration] = useState('');
  const [budget, setBudget] = useState('');
  const [address, setAddress] = useState('');
  const [area, setArea] = useState('');
  const [note, setNote] = useState('');
  const [blur, setBlur] = useState(false);
  const [selectedLoc, setSelectedLoc] = useState([]);
  const [selectedStyle, setSelectedStyle] = useState([]);
  const [newSelectedLoc, setnewSelectedLoc] = useState(selectedLoc);
  const [newSelectedStyle, setnewSelectedStyle] = useState(selectedStyle);

  const building = buildType.length
    ? buildType.map((item) => ({value: item._id, label: item.name}))
    : [];

  const service = serviceType.length
    ? serviceType.map((item) => ({value: item._id, label: item.name}))
    : [];

  const chooseLocation = (item) => {
    let selectLoc = [...newSelectedLoc];
    if (selectLoc.find((value) => value === item)) {
      let locSelected = selectLoc.filter((e) => e !== item);
      setnewSelectedLoc(locSelected);
    } else {
      selectLoc.push(item);
      setnewSelectedLoc(selectLoc);
    }
  };

  const chooseStyle = (item) => {
    let selectStyle = [...newSelectedStyle];
    if (selectStyle.find((value) => value === item)) {
      let styleSelected = selectStyle.filter((e) => e !== item);
      setnewSelectedStyle(styleSelected);
    } else {
      selectStyle.push(item);
      setnewSelectedStyle(selectStyle);
    }
  };

  const goBack = () => {
    props.navigation.goBack();
  };

  const goSelectDate = () => {
    const data = {
      duration,
      budget: budget.toString(),
      address,
      note: note === '' ? 'none' : note,
      area,
      buildType: buildingType,
      serviceType: servicingType,
      locations: newSelectedLoc,
      styles: newSelectedStyle,
    };
    props.setNewAppointment(data);
    props.getTimeslot(data.serviceType, false);
    props.navigation.navigate('SelectDate');
  };

  return (
    <View style={Layouting().flex}>
      <HeaderOrder title="New Appointment" onPress={goBack} />
      <ScrollView
        contentContainerStyle={styles.container}
        keyboardDismissMode="none"
        showsVerticalScrollIndicator={false}>
        <InfoCard content="Get free professional consultation. We help you to get best interior and architecture design." />

        <HeadingForm title="Building Type">
          <Dropdown
            placeholder="Please select"
            colorText={Color.gunGray}
            items={building}
            defaultValue={buildingType}
            onChangeItem={(item) => setBuildingType(item.value)}
          />
        </HeadingForm>

        <HeadingForm title="Service Type">
          <Dropdown
            placeholder="Please select"
            colorText={Color.gunGray}
            items={service}
            defaultValue={servicingType}
            onChangeItem={(item) => setServicingType(item.value)}
          />
        </HeadingForm>

        <HeadingForm title="Estimated Work Duration">
          <OrderInput
            placeholder="e.g 3"
            value={duration}
            onChangeText={(text) => setDuration(text)}
            keyboardType="numeric"
            rightIcon={
              <NotoSans
                title="week(s)"
                size={Size.ms14}
                color={Color.gunGray}
              />
            }
          />
        </HeadingForm>

        <HeadingForm title="Budget">
          <OrderInput
            onFocus={() => setBlur(false)}
            onBlur={() => setBlur(true)}
            placeholder="e.g 10000000"
            value={
              blur
                ? formatMoney(budget, {
                    symbol: '',
                    thousand: ',',
                    precision: 0,
                  })
                : budget.toString()
            }
            onChangeText={(text) => {
              if (text.length) {
                setBudget(parseInt(text));
              } else {
                setBudget(0);
              }
            }}
            keyboardType="numeric"
            leftIcon={
              <NotoSans title="Rp" size={Size.ms14} color={Color.gunGray} />
            }
          />
        </HeadingForm>

        <HeadingForm title="Address">
          <OrderInput
            autoCapitalize="words"
            placeholder="e.g One East Residences 7-16 Jl. Raya Kertajaya Indah No. 79 Surabaya Jawa Timur"
            value={address}
            onChangeText={(text) => setAddress(text)}
            multiline={true}
          />
        </HeadingForm>

        <HeadingForm title="Area">
          <OrderInput
            placeholder="e.g 26"
            value={area}
            onChangeText={(text) => setArea(text)}
            keyboardType="numeric"
            rightIcon={
              <NotoSans
                title={
                  <Superscript
                    base="m"
                    exponent="2"
                    sizeBase={Size.ms14}
                    sizeExp={Size.ms10}
                    color={Color.gunGray}
                  />
                }
              />
            }
          />
        </HeadingForm>

        <FilterCardContainer
          title="Inspection Area"
          hasNote={true}
          isRequired={true}>
          <ScrollView horizontal={true}>
            <FlatList
              keyExtractor={(item) => item._id.toString()}
              data={location}
              numColumns={3}
              renderItem={({item}) => {
                return (
                  <FilterCard
                    newSelected={newSelectedLoc}
                    title={item.name}
                    id={item._id}
                    onPress={() => chooseLocation(item._id)}
                  />
                );
              }}
            />
          </ScrollView>
        </FilterCardContainer>

        <FilterCardContainer title="Preferred Style" isRequired={true}>
          <ScrollView horizontal={true}>
            <FlatList
              keyExtractor={(item) => item._id.toString()}
              data={style}
              numColumns={3}
              renderItem={({item}) => {
                return (
                  <FilterCard
                    title={item.name}
                    id={item._id}
                    onPress={() => chooseStyle(item._id)}
                    newSelected={newSelectedStyle}
                  />
                );
              }}
            />
          </ScrollView>
        </FilterCardContainer>

        <HeadingForm title="Note">
          <OrderInput
            placeholder="Tell your request here"
            value={note}
            onChangeText={(text) => setNote(text)}
            multiline={true}
          />
        </HeadingForm>
      </ScrollView>
      <YellowButton
        title="Next"
        disabled={
          buildingType &&
          serviceType &&
          duration &&
          budget &&
          address &&
          area &&
          newSelectedLoc.length &&
          newSelectedStyle.length
            ? false
            : true
        }
        onPress={goSelectDate}
      />
    </View>
  );
}

const mapStateToProps = (state) => ({
  buildType: state.OrderReducer.buildType,
  serviceType: state.OrderReducer.serviceType,
  location: state.ExploreReducer.location,
  style: state.ExploreReducer.style,
});

const mapDispatchToProps = {
  setNewAppointment,
  getTimeslot,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppointmentForm);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp2,
  },
});
