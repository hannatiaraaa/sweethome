import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';

// component
import Loading from '../../../shared/components/Loading';
import HeaderOrder from '../components/Header';
import {Content, Note} from '../components/AppointmentDetailsContent';
import {YellowButton} from '../../../shared/components/YellowButton';

// global
import {Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {createNewAppointment} from '../redux/action';

function AppointmentReview(props) {
  const {newAppointment, buildType, timeslot} = props.OrderReducer;
  const {location, isLoading} = props;
  const buildingType = buildType.find(
    (item) => item._id === newAppointment.buildType,
  );

  const locations = newAppointment.locations.map((item) =>
    location.find((value) => value._id === item),
  );
  const time = timeslot.find((item) => item._id === newAppointment.timeslot);

  const goBack = () => {
    props.navigation.goBack();
  };

  const goToOrder = () => {
    props.createNewAppointment(newAppointment);
  };

  return (
    <View style={Layouting().flex}>
      {isLoading ? (
        <Loading />
      ) : (
        <>
          <HeaderOrder title="Review" onPress={goBack} />

          <ScrollView contentContainerStyle={styles.container}>
            <Content
              isReview={true}
              buildingType={buildingType.name}
              duration={newAppointment.duration}
              area={newAppointment.area}
              budget={newAppointment.budget}
              address={newAppointment.address}
              inspectionArea={locations.map((item, index) => {
                if (locations.length === 1) {
                  return item.name;
                } else {
                  if (index !== locations.length - 1) {
                    if (locations.length === 2) {
                      return `${item.name} `;
                    } else {
                      return `${item.name}, `;
                    }
                  } else {
                    return `and ${item.name}`;
                  }
                }
              })}
              date={newAppointment.date}
              startTime={time.start}
              endTime={time.end}
            />
            {newAppointment.note === 'none' || !newAppointment.note ? null : (
              <Note note={newAppointment.note} />
            )}
          </ScrollView>

          <YellowButton title="Create Appointment" onPress={goToOrder} />
        </>
      )}
    </View>
  );
}

const mapStateToProps = (state) => ({
  OrderReducer: state.OrderReducer,
  location: state.ExploreReducer.location,
  style: state.ExploreReducer.style,
  isLoading: state.GlobalReducer.isLoading,
});

const mapDispatchToProps = {
  createNewAppointment,
};

export default connect(mapStateToProps, mapDispatchToProps)(AppointmentReview);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
});
