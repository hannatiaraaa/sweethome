import React, {useState, useEffect} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';

// component
import {ProjectCard} from './components/ProjectCard';

// global
import {Size} from '../../shared/global/config';
import {Layouting} from '../../shared/global/Layouting';

// redux
import {connect} from 'react-redux';
import {getProject, getProjectDetails} from './redux/action';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function Project(props) {
  const {project} = props;
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    props.getProject();
  }, []);

  const onRefresh = () => {
    setRefreshing(true);

    wait(1).then(() => {
      props.getProject();
      setRefreshing(false);
    });
  };

  const goToDetails = (id) => {
    props.getProjectDetails(id);
  };

  return (
    <View style={[Layouting().flex, styles.container]}>
      <FlatList
        onRefresh={onRefresh}
        refreshing={refreshing}
        showsVerticalScrollIndicator={false}
        data={project}
        keyExtractor={(item) => item._id.toString()}
        renderItem={({item}) => {
          return (
            <ProjectCard
              project={item}
              packages={item.packages}
              onPress={() => goToDetails(item._id)}
            />
          );
        }}
      />
    </View>
  );
}

const mapStateToProps = (state) => ({
  project: state.OrderReducer.project,
});

const mapDispatchToProps = {
  getProject,
  getProjectDetails,
};

export default connect(mapStateToProps, mapDispatchToProps)(Project);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
  },
});
