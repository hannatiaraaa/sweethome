import {all, call, put, takeLatest} from 'redux-saga/effects';

// router
import {navigate} from '../../../router/navigate';

// api
import {request} from '../../../shared/utils/api';
import {
  FAVORITE,
  PROJECT,
  SEARCH,
  SHOWCASE,
} from '../../../shared/utils/endPoint';

// action
import {
  GET_SAVED,
  GET_SAVED_ALL,
  SAVED,
  DEL_SAVED,
  getSaved,
  setSaved,
  setSavedAll,
  setShowcaseSavedAll,
  getSavedAll,
  GET_SEARCH_SAVED,
  GET_SHOWCASE_SAVED_ALL,
} from './action';

function* getSavedSaga({limit = 0}) {
  const res = yield call(request, `${FAVORITE}?limit=${5 + limit}`, 'GET');

  if (res.status) {
    yield put(setSaved(res.data.data, res.data.activePage, res.data.totalPage));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getSavedAllSaga() {
  const res = yield call(request, `${FAVORITE}`, 'GET');

  if (res.status) {
    yield put(setSavedAll(res.data.data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* getShowcaseSavedAllSaga() {
  const res = yield call(request, `${SHOWCASE}${PROJECT}`, 'GET');

  if (res.status) {
    yield put(setShowcaseSavedAll(res.data.data));
  } else {
    throw new Error(`HTTP error! status: ${res}`);
  }
}

function* postSavedSaga({id, screen}) {
  const res = yield call(request, `${SHOWCASE}/${id}${FAVORITE}`, 'POST');
  if (res.status) {
    yield put(getSaved());
    yield put(getSavedAll());
    if (screen === 'explore') {
      yield navigate('MainTab', {
        screen: 'ExploreRouter',
        params: {screen: 'Explore'},
      });
    }
    if (screen === 'showcase') {
      yield navigate('Showcase');
    }
  }
}

function* delSavedSaga({id, screen}) {
  const res = yield call(request, `${SHOWCASE}/${id}${FAVORITE}`, 'DELETE');
  if (res.status) {
    yield put(getSaved());
    yield put(getSavedAll());
    if (screen === 'explore') {
      yield navigate('MainTab', {
        screen: 'ExploreRouter',
        params: {screen: 'Explore'},
      });
    }
    if (screen === 'saved') {
      yield navigate('MainTab', {
        screen: 'Saved',
      });
    }
    if (screen === 'showcase') {
      yield navigate('Showcase');
    }
  }
}

function* getSearchSavedSaga({text}) {
  const res = yield call(request, `${FAVORITE}${SEARCH}?query=${text}`, 'GET');

  if (res.status) {
    yield put(setShowcaseSavedAll(res.data.data));
  }
}

export function* SavedSaga() {
  yield all([
    takeLatest(GET_SAVED, getSavedSaga),
    takeLatest(GET_SAVED_ALL, getSavedAllSaga),
    takeLatest(GET_SHOWCASE_SAVED_ALL, getShowcaseSavedAllSaga),
    takeLatest(SAVED, postSavedSaga),
    takeLatest(DEL_SAVED, delSavedSaga),
    takeLatest(GET_SEARCH_SAVED, getSearchSavedSaga),
  ]);
}
