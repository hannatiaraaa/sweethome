export const GET_SAVED = 'GET_SAVED';
export const GET_SEARCH_SAVED = 'GET_SEARCH_SAVED';
export const GET_SAVED_ALL = 'GET_SAVED_ALL';
export const GET_SHOWCASE_SAVED_ALL = 'GET_SHOWCASE_SAVED_ALL';
export const SET_SAVED = 'SET_SAVED';
export const SET_SAVED_ALL = 'SET_SAVED_ALL';
export const SET_SHOWCASE_SAVED_ALL = 'SET_SHOWCASE_SAVED_ALL';
export const SAVED = 'SAVED';
export const DEL_SAVED = 'DEL_SAVED';

export const getSaved = (limit) => {
  return {
    type: GET_SAVED,
    limit,
  };
};

export const getSavedAll = () => {
  return {
    type: GET_SAVED_ALL,
  };
};

export const getShowcaseSavedAll = () => {
  return {
    type: GET_SHOWCASE_SAVED_ALL,
  };
};

export const actionSaved = (id, screen) => {
  return {
    type: SAVED,
    id,
    screen,
  };
};
export const delSaved = (id, screen) => {
  return {
    type: DEL_SAVED,
    id,
    screen,
  };
};

export const setSaved = (payload, activePage, totalPage) => {
  return {
    type: SET_SAVED,
    payload,
    activePage,
    totalPage,
  };
};

export const getSearchSaved = (text) => {
  return {
    type: GET_SEARCH_SAVED,
    text,
  };
};

export const setSavedAll = (payload) => {
  return {
    type: SET_SAVED_ALL,
    payload,
  };
};

export const setShowcaseSavedAll = (payload) => {
  return {
    type: SET_SHOWCASE_SAVED_ALL,
    payload,
  };
};
