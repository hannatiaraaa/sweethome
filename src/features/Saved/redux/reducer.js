import {SET_SAVED, SET_SAVED_ALL, SET_SHOWCASE_SAVED_ALL} from './action';

const initialState = {
  saved: [],
  savedAll: [],
  showcase: [],
  activePage: 0,
  totalPage: 0,
};

export const SavedReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SAVED:
      return {
        ...state,
        saved: action.payload,
        activePage: action.activePage,
        totalPage: action.totalPage,
      };

    case SET_SAVED_ALL:
      return {
        ...state,
        savedAll: action.payload,
      };

    case SET_SHOWCASE_SAVED_ALL:
      return {
        ...state,
        showcase: action.payload,
      };

    default:
      return state;
  }
};
