import React, {useState, useEffect, useCallback} from 'react';
import {View, FlatList, StyleSheet} from 'react-native';
import {heightPercentageToDP} from 'react-native-responsive-screen';

// component
import {Search} from '../../shared/components/Search';
import {DataCard} from '../../shared/components/DataCard';
import NotoSans from '../../shared/components/NotoSans';

// global
import {Layouting} from '../../shared/global/Layouting';
import {Color, Size} from '../../shared/global/config';

// redux
import {connect} from 'react-redux';
import {getShowcaseDetails} from '../Home/redux/action';
import {
  getSaved,
  delSaved,
  getSearchSaved,
  getShowcaseSavedAll,
} from './redux/action';

const wait = (timeout) => {
  return new Promise((resolve) => {
    setTimeout(resolve, timeout);
  });
};

function Saved(props) {
  const {saved, showcase, activePage, totalPage} = props.SavedReducer;
  const [pressedSaved, setPressedSaved] = useState('');
  const [searchBar, setSearchBar] = useState('');
  const [refreshing, setRefreshing] = useState(false);

  useEffect(() => {
    if (searchBar.length < 2) {
      props.getSaved();
    }
  }, [searchBar, showcase]);

  const onRefresh = () => {
    setRefreshing(true);

    wait(1).then(() => {
      props.getSaved();
      props.getShowcaseSavedAll();
      setRefreshing(false);
    });
  };

  const showNewShowcaseState = useCallback(() => {
    props.getSaved(5);
  }, [props]);

  const itemSaved = (id) => {
    setPressedSaved(id);
    props.delSaved(id, 'saved');
  };

  const updateSearch = (text) => {
    setSearchBar(text);
    props.getSearchSaved(text);
  };

  const goToDetails = (id) => {
    props.getShowcaseDetails(id);
  };

  return (
    <View style={Layouting().flex}>
      <Search value={searchBar} onChangeText={(text) => updateSearch(text)} />
      <View style={[Layouting().flex, styles.container]}>
        {searchBar.length >= 2 ? (
          <FlatList
            onRefresh={onRefresh}
            refreshing={refreshing}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item) => item._id.toString()}
            data={showcase.length && showcase}
            renderItem={({item}) => {
              console.log(item, 'ada apa ini');
              return (
                <DataCard
                  title={item.name}
                  location={item.address}
                  sourceImage={
                    item.gallery[0]
                      ? {uri: item.gallery[0].photo}
                      : require('../../assets/images/sweethome.png')
                  }
                  onPress={() => goToDetails(item._id)}
                  hasSaved={true}
                  onPressSaved={() => itemSaved(item._id)}
                  pressedSaved={pressedSaved === item._id ? false : true}
                />
              );
            }}
          />
        ) : (
          <FlatList
            onRefresh={onRefresh}
            refreshing={refreshing}
            showsVerticalScrollIndicator={false}
            keyExtractor={(item) => item._id.toString()}
            data={saved}
            renderItem={({item, index}) => {
              return (
                <DataCard
                  title={item.showcase.name}
                  location={item.showcase.address}
                  sourceImage={
                    item.showcase.gallery[0]
                      ? {uri: item.showcase.gallery[0].photo}
                      : require('../../assets/images/sweethome.png')
                  }
                  onPress={() => goToDetails(item.showcase._id)}
                  hasSaved={true}
                  onPressSaved={() => itemSaved(item.showcase._id)}
                  pressedSaved={
                    pressedSaved === item.showcase._id ? false : true
                  }
                />
              );
            }}
            ListFooterComponent={
              activePage === totalPage || !saved.length ? null : (
                <NotoSans
                  title="see more"
                  color={Color.primaryBlue}
                  style={styles.textSeeMore}
                  onPress={() => {
                    showNewShowcaseState();
                  }}
                />
              )
            }
          />
        )}
      </View>
    </View>
  );
}

const mapStateToProps = (state) => ({
  SavedReducer: state.SavedReducer,
  showcaseAll: state.ExploreReducer.showcaseAll,
  user: state.ProfileReducer.user,
});

const mapDispatchToProps = {
  getShowcaseDetails,
  getSaved,
  delSaved,
  getSearchSaved,
  getShowcaseSavedAll,
};

export default connect(mapStateToProps, mapDispatchToProps)(Saved);

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: Size.wp4,
    paddingVertical: Size.hp2,
  },
  filterContainer: {
    paddingVertical: Size.hp1,
  },
  filterIcon: {
    marginTop: heightPercentageToDP(1.5),
    height: Size.ms32,
    marginRight: Size.ms12,
  },
  textSeeMore: {
    marginTop: Size.hp2,
    marginBottom: Size.hp4,
    textAlign: 'center',
    textDecorationLine: 'underline',
  },
});
