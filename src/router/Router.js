import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// router
import BottomMainTab from './BottomMainTab';
import {AuthRouter} from './AuthRouter';
import {OrderScreenRouter} from './featuresRouter/OrderRouter/OrderScreenRouter';
import Showcase from '../features/Home/Showcase';

const MainStack = createStackNavigator();

export function Router() {
  return (
    <MainStack.Navigator initialRouteName="MainTab" headerMode="none">
      <MainStack.Screen name="MainTab" component={BottomMainTab} />
      <MainStack.Screen name="Auth" component={AuthRouter} />
      <MainStack.Screen
        name="OrderScreenRouter"
        component={OrderScreenRouter}
      />
      <MainStack.Screen name="Showcase" component={Showcase} />
    </MainStack.Navigator>
  );
}
