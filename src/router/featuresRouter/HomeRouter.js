import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// screen
import Home from '../../features/Home/Home';
import Showcase from '../../features/Home/Showcase';

const HomeStack = createStackNavigator();

export function HomeRouter() {
  return (
    <HomeStack.Navigator initialRouteName="Home" headerMode="none">
      <HomeStack.Screen name="Home" component={Home} />
      <HomeStack.Screen name="Showcase" component={Showcase} />
    </HomeStack.Navigator>
  );
}
