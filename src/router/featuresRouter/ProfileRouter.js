import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// screen
import ProfileLoggedIn from '../../features/Profile/ProfileLoggedIn';
import ProfileLoggedOut from '../../features/Profile/ProfileLoggedOut';
import ProfileSettings from '../../features/Profile/ProfileSettings';
import AccountSettings from '../../features/Profile/AccountSettings';

const ProfileStack = createStackNavigator();

export function ProfileRouter() {
  return (
    <ProfileStack.Navigator initialRouteName="LoggedOut">
      <ProfileStack.Screen
        name="LoggedIn"
        component={ProfileLoggedIn}
        options={{headerShown: false}}
      />
      <ProfileStack.Screen
        name="LoggedOut"
        component={ProfileLoggedOut}
        options={{headerShown: false}}
      />
      <ProfileStack.Screen
        name="ProfileSettings"
        component={ProfileSettings}
        options={{headerTitle: '', headerTransparent: true}}
      />
      <ProfileStack.Screen
        name="AccountSettings"
        component={AccountSettings}
        options={{headerTitle: '', headerTransparent: true}}
      />
    </ProfileStack.Navigator>
  );
}
