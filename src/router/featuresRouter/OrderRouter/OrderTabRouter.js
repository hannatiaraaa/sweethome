import React from 'react';
import {StyleSheet, View} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';

// screen
import Appointment from '../../../features/Order/Appointment';
import Project from '../../../features/Order/Project';

// component
import NotoSans from '../../../shared/components/NotoSans';
import {PlusButton} from '../../../features/Order/components/PlusButton';

// global
import {Color, Size} from '../../../shared/global/config';
import {Layouting} from '../../../shared/global/Layouting';

const OrderTab = createMaterialTopTabNavigator();

// redux
import {connect} from 'react-redux';
import {
  getFormAppointment,
  getAppointment,
  getProject,
} from '../../../features/Order/redux/action';

function OrderRouter(props) {
  const goToNewAppointment = () => {
    props.getFormAppointment();
    props.getAppointment();
    props.getProject();
  };

  return (
    <>
      <OrderTab.Navigator
        tabBarOptions={{
          inactiveTintColor: 'white',
          activeTintColor: Color.primaryBlue,
          indicatorStyle: {
            backgroundColor: 'transparent',
          },
          style: {
            backgroundColor: Color.primaryBlue,
            justifyContent: 'flex-end',
            alignContent: 'center',
            paddingHorizontal: Size.ms80,
            height: Size.ms84,
          },
        }}
        initialRouteName="Appointment">
        <OrderTab.Screen
          name="Appointment"
          component={Appointment}
          options={{
            tabBarLabel: ({color, focused}) => {
              return (
                <View
                  style={
                    focused ? [Layouting().centered, styles.container] : null
                  }>
                  <NotoSans title="Appointment" color={color} />
                </View>
              );
            },
          }}
        />
        <OrderTab.Screen
          name="Project"
          component={Project}
          options={{
            tabBarLabel: ({color, focused}) => {
              return (
                <View
                  style={
                    focused ? [Layouting().centered, styles.container] : null
                  }>
                  <NotoSans title="Project" color={color} />
                </View>
              );
            },
          }}
        />
      </OrderTab.Navigator>
      <PlusButton onPress={goToNewAppointment} />
    </>
  );
}

const mapStateToProps = () => ({});

const mapDispatchToProps = {
  getFormAppointment,
  getAppointment,
  getProject,
};

export default connect(mapStateToProps, mapDispatchToProps)(OrderRouter);

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    borderRadius: Size.ms7,
    marginHorizontal: -Size.ms12,
    paddingVertical: Size.ms4,
    marginVertical: -Size.ms4,
  },
});
