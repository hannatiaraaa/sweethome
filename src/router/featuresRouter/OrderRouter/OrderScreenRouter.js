import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// router
import OrderRouter from './OrderTabRouter';
import {NewAppointmentRouter} from './NewAppointmentRouter';

// screen
import AppointmentDetails from '../../../features/Order/AppointmentDetails';
import ProjectDetails from '../../../features/Order/ProjectDetails';

const OrderStack = createStackNavigator();

export function OrderScreenRouter() {
  return (
    <OrderStack.Navigator initialRouteName="Order" headerMode="none">
      <OrderStack.Screen name="Order" component={OrderRouter} />
      <OrderStack.Screen
        name="NewAppointment"
        component={NewAppointmentRouter}
      />
      <OrderStack.Screen
        name="AppointmentDetails"
        component={AppointmentDetails}
      />
      <OrderStack.Screen name="ProjectDetails" component={ProjectDetails} />
    </OrderStack.Navigator>
  );
}
