import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// screen
import AppointmentForm from '../../../features/Order/NewAppointment/Form';
import SelectDate from '../../../features/Order/NewAppointment/SelectDate';
import SelectTime from '../../../features/Order/NewAppointment/SelectTime';
import AppointmentReview from '../../../features/Order/NewAppointment/Review';

const NewAppointmentStack = createStackNavigator();

export function NewAppointmentRouter({navigation}) {
  return (
    <NewAppointmentStack.Navigator
      initialRouteName="AppointmentForm"
      headerMode="none">
      <NewAppointmentStack.Screen
        name="AppointmentForm"
        component={AppointmentForm}
      />
      <NewAppointmentStack.Screen name="SelectDate" component={SelectDate} />
      <NewAppointmentStack.Screen name="SelectTime" component={SelectTime} />
      <NewAppointmentStack.Screen
        name="AppointmentReview"
        component={AppointmentReview}
      />
    </NewAppointmentStack.Navigator>
  );
}
