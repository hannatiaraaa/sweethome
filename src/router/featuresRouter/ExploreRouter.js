import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

// screen
import Explore from '../../features/Explore/Explore';
import Filter from '../../features/Explore/Filter';

const ExploreStack = createStackNavigator();

export function ExploreRouter() {
  return (
    <ExploreStack.Navigator initialRouteName="Explore" headerMode="none">
      <ExploreStack.Screen
        name="Explore"
        component={Explore}
        initialParams={{filtered: false}}
      />
      <ExploreStack.Screen name="Filter" component={Filter} />
    </ExploreStack.Navigator>
  );
}
