import React from 'react';
import {View, StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {useNavigationState} from '@react-navigation/native';

// router
import Home from '../features/Home/Home';
import {ExploreRouter} from './featuresRouter/ExploreRouter';
import {OrderScreenRouter} from './featuresRouter/OrderRouter/OrderScreenRouter';
import {ProfileRouter} from './featuresRouter/ProfileRouter';

// screen
import Saved from '../features/Saved/Saved';

// component
import NotoSans from '../shared/components/NotoSans';

// global
import {Layouting} from '../shared/global/Layouting';
import {Color, Size} from '../shared/global/config';

// icon
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import Ionicons from 'react-native-vector-icons/Ionicons';

// redux
import {connect} from 'react-redux';
import {moderateScale} from 'react-native-size-matters';

const BottomTab = createBottomTabNavigator();

function BottomMainTab(props) {
  const data = useNavigationState((state) => state.routes);
  const index = data[0].state?.index;
  const routeNames = data[0].state?.routeNames[index];

  return (
    <BottomTab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        showLabel: false,
        allowFontScaling: true,
        inactiveTintColor: Color.primaryBlue,
        activeTintColor: 'white',
        keyboardHidesTabBar: true,
        style: [Layouting().shadow, {paddingHorizontal: Size.wp4}],
      }}>
      <BottomTab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarIcon: ({color}) => {
            return (
              <View
                style={
                  !routeNames || routeNames === 'Home'
                    ? [
                        Layouting().flexRow,
                        styles.container,
                        props.token
                          ? Layouting().spaceBetween
                          : [Layouting().centered, styles.logoutContainer],
                      ]
                    : Layouting().centered
                }>
                <MaterialCommunityIcons
                  name="home-outline"
                  size={
                    !routeNames || routeNames === 'Home' ? Size.ms18 : Size.ms22
                  }
                  color={color}
                  style={styles.icon}
                />
                {!routeNames || routeNames === 'Home' ? (
                  <NotoSans
                    title="Home"
                    size={Size.ms14}
                    type="Bold"
                    color="white"
                  />
                ) : null}
              </View>
            );
          },
        }}
      />
      {props.token ? (
        <BottomTab.Screen
          name="ExploreRouter"
          component={ExploreRouter}
          options={{
            tabBarIcon: ({color}) => {
              return (
                <View
                  style={
                    routeNames === 'ExploreRouter'
                      ? [
                          Layouting().flexRow,
                          Layouting().spaceEvenly,
                          styles.container,
                          styles.width,
                        ]
                      : Layouting().centered
                  }>
                  <Ionicons
                    name="search"
                    size={
                      !routeNames || routeNames === 'ExploreRouter'
                        ? Size.ms16
                        : Size.ms20
                    }
                    color={color}
                    style={styles.icon}
                  />
                  {routeNames === 'ExploreRouter' ? (
                    <NotoSans
                      title="Explore"
                      size={Size.ms14}
                      type="Bold"
                      color="white"
                    />
                  ) : null}
                </View>
              );
            },
          }}
        />
      ) : null}
      {props.token ? (
        <BottomTab.Screen
          name="OrderRouter"
          component={OrderScreenRouter}
          options={{
            tabBarIcon: ({color}) => {
              return (
                <View
                  style={
                    routeNames === 'OrderRouter'
                      ? [
                          Layouting().flexRow,
                          Layouting().spaceBetween,
                          styles.container,
                        ]
                      : Layouting().centered
                  }>
                  <AntDesign
                    name="shoppingcart"
                    size={
                      !routeNames || routeNames === 'OrderRouter'
                        ? Size.ms16
                        : Size.ms20
                    }
                    color={color}
                    style={styles.icon}
                  />
                  {routeNames === 'OrderRouter' ? (
                    <NotoSans
                      title="Order"
                      size={Size.ms14}
                      type="Bold"
                      color="white"
                    />
                  ) : null}
                </View>
              );
            },
          }}
        />
      ) : null}
      {props.token ? (
        <BottomTab.Screen
          name="Saved"
          component={Saved}
          options={{
            tabBarIcon: ({color}) => {
              return (
                <View
                  style={
                    routeNames === 'Saved'
                      ? [
                          Layouting().flexRow,
                          Layouting().spaceBetween,
                          styles.container,
                        ]
                      : Layouting().centered
                  }>
                  <MaterialCommunityIcons
                    name="heart-outline"
                    size={
                      !routeNames || routeNames === 'Saved'
                        ? Size.ms16
                        : Size.ms20
                    }
                    color={color}
                    style={styles.icon}
                  />
                  {routeNames === 'Saved' ? (
                    <NotoSans
                      title="Saved"
                      size={Size.ms14}
                      type="Bold"
                      color="white"
                    />
                  ) : null}
                </View>
              );
            },
          }}
        />
      ) : null}
      <BottomTab.Screen
        name="ProfileRouter"
        component={ProfileRouter}
        options={{
          tabBarIcon: ({color}) => {
            return (
              <View
                style={
                  routeNames === 'ProfileRouter'
                    ? [
                        Layouting().flexRow,
                        styles.container,
                        props.token
                          ? Layouting().spaceBetween
                          : [Layouting().centered, styles.logoutContainer],
                      ]
                    : Layouting().centered
                }>
                <Ionicons
                  name="md-person-outline"
                  size={
                    !routeNames || routeNames === 'ProfileRouter'
                      ? Size.ms16
                      : Size.ms20
                  }
                  color={color}
                  style={styles.icon}
                />
                {routeNames === 'ProfileRouter' ? (
                  <NotoSans
                    title="Profile"
                    size={Size.ms14}
                    type="Bold"
                    color="white"
                  />
                ) : null}
              </View>
            );
          },
        }}
      />
    </BottomTab.Navigator>
  );
}

const mapStateToProps = (state) => ({
  token: state.AuthReducer.token,
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(BottomMainTab);

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.primaryBlue,
    borderRadius: Size.ms8,
    padding: Size.ms7,
  },
  logoutContainer: {
    width: moderateScale(160),
  },
  width: {
    width: Size.ms100,
  },
  icon: {
    marginRight: Size.ms4,
  },
});
