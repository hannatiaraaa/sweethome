import 'react-native-gesture-handler';
import React, {useEffect} from 'react';
import {StatusBar} from 'react-native';

// splash screen
import SplashScreen from 'react-native-splash-screen';

// root
import Root from './Root';

// redux
import {Provider} from 'react-redux';
import {Persistor, Store} from './src/store/Store';
import {PersistGate} from 'redux-persist/integration/react';

export default function App() {
  useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <Provider store={Store}>
      <PersistGate persistor={Persistor}>
        <StatusBar
          backgroundColor="transparent"
          barStyle="light-content"
          translucent={true}
        />
        <Root />
      </PersistGate>
    </Provider>
  );
}
