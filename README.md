<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
    <img src="src/assets/images/loadingLogo.png" alt="Logo" width="301" height="100">
  </a>

  <h3 align="center">sweethome - mobile application</h3>

  <p align="center">
    Final Project Batch #10
    <br />
    <a href="https://gitlab.com/glints-academy-batch-10"><strong>Glints Academy with Binar</strong></a>
    <br />
</p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#showcase">Showcase</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
    <li><a href="#copyright">Copyright</a></li>
  </ol>
</details>

<!-- ABOUT THE PROJECT -->

## About The Project

One-stop service of the interior and/or architecture design to help find solutions to improve function, enrich aesthetic value, and enhance the psychological aspects of a room with booking appointment and payment experiences, all in one mobile application. This project results from the teamwork collaboration with the back-end developer team and front-end developer website team, whereby we collaborated for 4 weeks. We can finish all MVP (Minimum Viable Product) by adding some features, components, UI, and other enhancements.

### Built With

- [React Native](https://reactnative.dev)

<!-- GETTING STARTED -->

## Getting Started

After the clone process has been done, you have to install a few things before this app can run on your local machine. You can choose the following methods:

### Prerequisites

You need to have yarn or npm that has been installed in your computer. This is how to install them:

- npm

  ```sh
  npm install npm@latest -g
  ```

- yarn

  Once you have npm installed you can run the following both to install and upgrade Yarn:

  ```sh
  npm install --global yarn
  ```

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome.git
   ```
2. Install dependencies
   ```sh
   yarn install
   ```
   or
   ```sh
   npm install
   ```

<!-- SHOWCASE -->

## Showcase

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/Icon.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/SplashScreen.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/Loading.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### Authentication

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/AuthLogin.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/AuthLoginInput.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/AuthLoginFailed.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/AuthFacebook.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/AuthSignUp.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/AuthSignUpInput.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/AuthSignUpFailed.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### Screens Without Authentication

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/NoAuthHome.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/NoAuthHomeScroll.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/NoAuthShowcase.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/NoAuthProfile.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### Home

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/HomeCard1.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/HomeCard2.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/HomeCard3.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/HomeFilter.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### Showcase

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/Showcase .jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ShowcaseGallery.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ShowcaseGalleryScroll.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### Explore and Filter

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/ExploreFilterOff.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ExploreFilterOn.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/FilterOff.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/FilterOn.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### Saved

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/Saved.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### New Appointment Form

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/NewAppointmentForm1.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/NewAppointmentForm2.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/NewAppointmentForm3.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/NewAppointmentDate.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/NewAppointmentTime.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/NewAppointmentReview.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### Appointments and Details

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/Appointment.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/AppointmentDeclined1.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/AppointmentDeclinedScroll.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/AppointmentDone.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### Project and Details

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/Project.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProjectCancelled.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProjectCancelledScroll.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/ProjectWaiting.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProjectWaitingScroll.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProjectWaitingCancellation.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProjectWaitingUploadReceipt.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

### Profile

<p float="left" align="middle">
    <a href="https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome">
        <img src="src/assets/images/Screenshot/Profile.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProfileScroll.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProfileProfileSettings.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProfileProfileSettingsScroll.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProfileAccount.jpg" alt="Showcase" width="270" height="555">
        <img src="src/assets/images/Screenshot/ProfileUploadPhoto.jpg" alt="Showcase" width="270" height="555">
    </a>
</p>

<!-- CONTRIBUTING -->

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/YourFeature`)
3. Commit your Changes (`git commit -m 'Add some YourFeature'`)
4. Push to the Branch (`git push origin feature/YourFeature`)
5. Open a Pull Request

<!-- CONTACT -->

## Contact

Hanna - hannatiara@gmail.com

Project Link: [https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome](https://gitlab.com/glints-academy-batch-10/sweethome/react-native/sweethome)

<!-- ACKNOWLEDGEMENTS -->

## Acknowledgements

- [React Navigation](https://reactnavigation.org/)
- [React Native Elements](https://reactnativeelements.com/)
- [Redux Saga](https://redux-saga.js.org/)
- [Axios](https://github.com/axios/axios)
- [React Native Vector Icons](https://github.com/oblador/react-native-vector-icons)
- [React Native Fast Image](https://github.com/DylanVann/react-native-fast-image)
- [React Native Splash Screen](https://github.com/crazycodeboy/react-native-splash-screen)

<!-- COPYRIGHT -->

## Copyright

© Copyright by [Hanna Tiara Andarlia](https://gitlab.com/hannatiaraaa/ 'hannatiaraaa')
