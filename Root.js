import React, {useEffect, useState} from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';

// router
import {NavigationContainer} from '@react-navigation/native';
import {Router} from './src/router/Router';
import {ref} from './src/router/navigate';

// component
import AlertModal from './src/shared/components/AlertModal';

// global
import {Color} from './src/shared/global/config';

// redux
import {connect} from 'react-redux';
import {getAll} from './src/features/Home/redux/action';
import {setAlert} from './src/store/GlobalAction';

function Root(props) {
  const {alert} = props;
  const [alertVisible, setAlertVisible] = useState(false);
  useEffect(() => {
    props.getAll();
  }, []);
  const lightTheme = {
    dark: false,
    colors: {
      primary: 'white',
      background: 'white',
      card: 'white',
      text: 'black',
      border: 'rgba(33, 68, 87, 0.3)',
      notification: Color.infoBlue,
    },
  };

  return (
    <SafeAreaProvider>
      {alert.status ? (
        <AlertModal
          isVisible={!alertVisible}
          title={alert.title}
          onPressClose={() => {
            setAlertVisible(false);
            props.setAlert({status: false});
          }}
          numOfButtons={alert.numOfButtons}
          labelButton={alert.labelButton}
          navOnPress={alert.navOnPress}
        />
      ) : null}

      <NavigationContainer theme={lightTheme} ref={ref}>
        <Router />
      </NavigationContainer>
    </SafeAreaProvider>
  );
}

const mapStateToProps = (state) => ({
  alert: state.GlobalReducer.alert,
});

const mapDispatchToProps = {
  getAll,
  setAlert,
};

export default connect(mapStateToProps, mapDispatchToProps)(Root);
